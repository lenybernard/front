import type { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
    overwrite: true,
    schema: `http://${process.env.REACT_APP_DOCKER_API_ALIAS}/graphql`,
    documents: ['src/**'],
    generates: {
        'src/graphql/api/generated.ts': {
            config: {
                maybeValue: 'T',
            },
            plugins: [
                'typescript',
                'typescript-operations',
                'typescript-react-apollo',
            ],
        },
    },
}

export default config
