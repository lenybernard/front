module.exports = {
    root: false,
    parser: `@typescript-eslint/parser`,
    parserOptions: {
        ecmaVersion: '22',
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
        project: './tsconfig.json',
    },
    settings: {
        react: {
            version: 'detect',
        },
        'import/extensions': ['.js', '.jsx', '.ts', '.tsx', '.mjs'],
        'import/resolver': {
            typescript: true,
            node: true,
        },
    },
    env: {
        browser: true,
        es2022: true,
        amd: true,
        node: true,
    },
    extends: [
        'plugin:react/recommended',
        'eslint:recommended',
        'airbnb-base',
        'airbnb-typescript',
        'plugin:prettier/recommended',
        'plugin:import/recommended',
        'plugin:import/typescript',
    ],
    plugins: [
        'react',
        'storybook',
        'prettier',
        'react-hooks',
        'import',
        '@typescript-eslint',
    ],
    rules: {
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/no-shadow': 'error',
        '@typescript-eslint/no-explicit-any': 'warn',
        camelcase: 'off',
        '@typescript-eslint/naming-convention': [
            'error',
            {
                selector: 'typeLike',
                format: null,
                custom: {
                    regex: '^([A-Z][a-zA-Z0-9]*(__[A-Z][a-zA-Z0-9]+Fragment)?)$', // Custom Regex authorizing graphql fragments (<PascalCase>__<PascalCase>Fragment)
                    match: true,
                },
            },
        ],
        '@typescript-eslint/no-unused-vars': ['error', { args: 'none' }],
        '@typescript-eslint/explicit-function-return-type': 'off',
        'import/prefer-default-export': 'off',
        'jsx-a11y/anchor-is-valid': 'off',
        'no-shadow': 'off',
        'no-underscore-dangle': 'off',
        'no-unused-vars': 'off',
        'no-plusplus': 'off',
        'prettier/prettier': ['error', {}, { usePrettierrc: true }],
        'react/no-unescaped-entities': 'off',
        'react/display-name': 1,
        'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
        'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
        'react/function-component-definition': [
            1,
            {
                namedComponents: 'arrow-function',
            },
        ],
        'react/jsx-filename-extension': [
            1,
            { extensions: ['.ts', '.tsx', '.js', '.jsx'] },
        ],
        'react/jsx-props-no-spreading': 'off',
        'react/prop-types': 'off',
        'react/react-in-jsx-scope': 'off',
        'react/require-default-props': 'off',
    },
    globals: {
        NodeJS: 'readonly',
    },
    ignorePatterns: ['**/*generated.ts'],
    overrides: [
        {
            files: [
                'src/**/*.stories.*',
                'src/**/*Tests.ts',
                'src/test-utils.tsx',
            ],
            rules: {
                'import/no-default-export': 'off',
                'import/no-extraneous-dependencies': 'off',
            },
        },
        {
            files: ['src/graphql/api/generated.ts'],
            rules: {
                '@typescript-eslint/naming-convention': 'off',
                '@typescript-eslint/no-explicit-any': 'off',
            },
        },
    ],
}
