import { Maybe, Scalars } from '../graphql/api/generated'

export declare interface PageInfo {
    endCursor?: Maybe<Scalars['String']>
    hasNextPage: Scalars['Boolean']
    hasPreviousPage: Scalars['Boolean']
    startCursor?: Maybe<Scalars['String']>
}

export interface ButtonState {
    bgGradient: string
    color: string
    loading: boolean
    loadingText: string
}
