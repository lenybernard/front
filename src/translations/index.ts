import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import en from './en/messages.json'
import fr from './fr/messages.json'

const resources = {
    en: {
        translation: en,
    },
    fr: {
        translation: fr,
    },
}

// eslint-disable-next-line
i18n.use(LanguageDetector)
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        fallbackLng: 'en', // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
        // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage

        interpolation: {
            escapeValue: false, // react already safes from xss
        },
    })

// eslint-disable-next-line import/no-default-export
export default i18n
