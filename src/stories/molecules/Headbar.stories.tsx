import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { HeadBar } from '@components/molecules/Menu/HeadBar'
import { BiAnalyse } from 'react-icons/bi'

const meta: Meta<typeof HeadBar> = {
    title: 'molecules/Form/HeadBar',
    component: HeadBar,
    args: {},
    decorators: [
        (Story) => (
            <Box maxW="600">
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof HeadBar>

export const Default: Story = {
    args: {
        links: [
            {
                name: 'Item',
                icon: BiAnalyse,
                to: '#',
                // subItems: [
                //     {
                //         name: 'Subitem 1',
                //         to: '#',
                //         display: { base: 'none', md: 'flex' },
                //     },
                //     {
                //         name: 'Subitem 2',
                //         to: '#',
                //         display: { base: 'none', md: 'flex' },
                //     },
                // ],
            },
        ],
    },
}
