import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { BookingPriceStat } from '@components/atoms/Stat/Material/Booking/BookingPriceStat'

const meta: Meta<typeof BookingPriceStat> = {
    title: 'molecules/Form/BookingPriceStat',
    component: BookingPriceStat,
    args: {},
    decorators: [
        (Story) => (
            <Box w={400}>
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof BookingPriceStat>

export const Default: Story = {
    args: {
        price: 42,
        periods: [
            {
                id: 'someId',
                endDate: '2022/01/02',
                startDate: '2022/01/01',
            },
        ],
    },
}
