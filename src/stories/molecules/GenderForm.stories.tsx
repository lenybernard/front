import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { GenderForm } from '@components/atoms/Form/GenderForm'

const meta: Meta<typeof GenderForm> = {
    title: 'molecules/Form/GenderForm',
    component: GenderForm,
    args: {},
    decorators: [
        (Story) => (
            <Box maxW="600">
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof GenderForm>

export const Default: Story = {
    args: {},
}
