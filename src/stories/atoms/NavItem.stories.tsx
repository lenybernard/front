import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { BiAnalyse } from 'react-icons/bi'
import { NavItem } from '@components/atoms/Menu/NavItem'

const meta: Meta<typeof NavItem> = {
    title: 'atoms/NavItem',
    component: NavItem,
    args: {},
    decorators: [
        (Story) => (
            <Box w={400}>
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof NavItem>

export const Default: Story = {
    args: {
        children: 'Pooling',
        to: '#',
        icon: BiAnalyse,
    },
}
