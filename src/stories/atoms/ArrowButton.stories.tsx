import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { PrimaryButton } from '@components/atoms/Button'
import { ArrowButton } from '@components/atoms/Button/ArrowButton'

const meta: Meta<typeof ArrowButton> = {
    title: 'atoms/Button',
    component: ArrowButton,
    args: {
        button: (
            <PrimaryButton as="button" variant="outline">
                Click
            </PrimaryButton>
        ),
        incentiveLabel: "Let's Communo !",
    },
    decorators: [
        (Story) => (
            <Box p={100}>
                <Story />
            </Box>
        ),
    ],
}
export default meta
type Story = StoryObj<typeof ArrowButton>

export const ArrowButtonDefault: Story = {
    args: {},
}
