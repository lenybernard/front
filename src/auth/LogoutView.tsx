import React, { useEffect } from 'react'
import { useNavigate } from 'react-router'
import { client } from '@_/apollo/main/client'
import { useAuth } from './useAuth'

export const LogoutView = () => {
    const navigate = useNavigate()
    const auth = useAuth()
    useEffect(() => {
        auth.signout(() => {
            client.resetStore()
            navigate('/', { replace: true })
        })
    })

    return <div />
}
