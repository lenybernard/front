import React, { ReactElement } from 'react'
import { Navigate, useLocation } from 'react-router'
import { useCookies } from 'react-cookie'
import { Unauthorized403 } from '@components/molecules/Error/Unauthorized403'
import { useAuth } from './useAuth'

export const RequireAuth = ({
    children,
    permission,
}: {
    children: ReactElement
    permission?: string
}) => {
    const location = useLocation()
    const [cookies, , removeCookie] = useCookies()
    const auth = useAuth()
    if (!localStorage.getItem('token') || cookies.user === undefined) {
        removeCookie('user')
        return <Navigate to="/login" state={{ from: location }} replace />
    }
    if (auth.user && permission && !auth.user.roles?.includes(permission)) {
        return <Unauthorized403 />
    }

    return children
}
