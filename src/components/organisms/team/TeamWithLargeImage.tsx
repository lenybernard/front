import {
    Button,
    Container,
    HStack,
    Icon,
    Image,
    Link as CLink,
    SimpleGrid,
    Stack,
    Text,
} from '@chakra-ui/react'
import { FaGithub, FaLinkedin } from 'react-icons/fa'
import { BsSubstack } from 'react-icons/bs'
import { useTranslation } from 'react-i18next'
import { StaticPagesRoutes } from '@_/routes/_hooks/routes.enum'
import { Link } from 'react-router'
import { PageHeader } from './PageHeader'
import { members } from './data'

export const TeamWithLargeImage = () => {
    const { t } = useTranslation()
    return (
        <Container py={{ base: '16', md: '24' }}>
            <Stack gap={{ base: '12', md: '16' }}>
                <PageHeader
                    tagline={t('team.tagline')}
                    headline={t('team.headline')}
                    description={t('team.description')}
                >
                    <Stack
                        direction={{ base: 'column-reverse', md: 'row' }}
                        gap="3"
                    >
                        <Button
                            variant="outline"
                            size={{ base: 'lg', md: 'xl' }}
                            colorPalette="gray"
                            asChild
                        >
                            <Link to={StaticPagesRoutes.Contact}>
                                {t('team.contactUs')}
                            </Link>
                        </Button>
                        <Button size={{ base: 'lg', md: 'xl' }}>
                            <Link to={StaticPagesRoutes.Contribute}>
                                {t('team.joinUs')}
                            </Link>
                        </Button>
                    </Stack>
                </PageHeader>
                <SimpleGrid
                    columns={{ base: 1, md: 2, lg: 3 }}
                    columnGap="8"
                    rowGap={{ base: '10', lg: '16' }}
                >
                    {members.map((member) => (
                        <Stack key={member.name} gap="4">
                            <Stack gap="5">
                                <Image
                                    src={member.image}
                                    alt={member.name}
                                    h="72"
                                    objectFit="cover"
                                />
                                <Stack gap="1">
                                    <Text
                                        fontWeight="medium"
                                        textStyle={{ base: 'lg', md: 'xl' }}
                                    >
                                        {member.name}
                                    </Text>
                                    <Text
                                        color="colorPalette.fg"
                                        textStyle={{ base: 'md', md: 'lg' }}
                                    >
                                        {member.role}
                                    </Text>
                                </Stack>
                            </Stack>
                            <HStack gap="4">
                                {member.linkedin && (
                                    <CLink
                                        href={member.linkedin}
                                        colorPalette="gray"
                                    >
                                        <FaLinkedin />
                                    </CLink>
                                )}
                                {member.github && (
                                    <CLink
                                        href={member.github}
                                        colorPalette="gray"
                                    >
                                        <FaGithub />
                                    </CLink>
                                )}
                                {member.substack && (
                                    <CLink
                                        href={member.substack}
                                        colorPalette="gray"
                                    >
                                        <Icon size="md">
                                            <BsSubstack />
                                        </Icon>
                                    </CLink>
                                )}
                            </HStack>
                        </Stack>
                    ))}
                </SimpleGrid>
            </Stack>
        </Container>
    )
}
