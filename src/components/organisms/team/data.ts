export const members = [
    {
        role: 'Co-fondateur',
        image: '/images/team/leny.jpg',
        name: 'Leny Bernard',
        description:
            'Leads company strategy and operations with 15+ years of experience in driving business growth and innovation.',
        linkedin: 'https://www.linkedin.com/in/lenybernard',
        github: 'https://github.com/lenybernard',
    },
    {
        role: 'Co-fondateur',
        image: '/images/team/antoine.jpg',
        name: 'Antoine Ebel',
        description:
            'Directs technical strategy and architecture, specializing in scalable systems and cloud infrastructure.',
        linkedin: 'https://www.linkedin.com/in/antoine-ebel-2787b170',
    },
    {
        role: 'Co-fondatrice',
        image: '/images/team/sandhya.jpg',
        name: 'Sandhya Domah',
        description:
            'Leads brand strategy and digital marketing initiatives with expertise in market analysis and growth.',
        linkedin: 'https://www.linkedin.com/in/sandhyadomah',
        substack: 'https://substack.com/@sandhyadomah',
    },
]
