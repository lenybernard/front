import React from 'react'
import {
    Body,
    Cell,
    Header,
    HeaderCell,
    HeaderRow,
    Row,
    Table,
} from '@table-library/react-table-library/table'
import moment, { locale as setMomentLocale } from 'moment'
import { Flex, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { UserList__UserFragment } from '@_/graphql/api/generated'
import { useTableTheme } from '@_/hooks/useTableTheme'
import { Avatar } from '@components/ui/avatar'
import { Tooltip } from '@components/ui/tooltip'

type Props = {
    users: UserList__UserFragment[]
}

export const UserList = ({ users }: Props) => {
    const data = { nodes: users }
    const resize = { resizerHighlight: '#ffde59' }
    const { t, i18n } = useTranslation()
    setMomentLocale(i18n.language)

    return (
        <Table
            {...{
                data,
                theme: useTableTheme(),
                layout: {
                    rounded: 'xl',
                    fixedHeader: true,
                    bgColor: 'yellow',
                },
            }}
        >
            {(tableList: UserList__UserFragment[]) => {
                return (
                    <>
                        <Header>
                            <HeaderRow>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.avatar'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.fullname'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.email'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.postalCode'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.city'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.phoneNumber'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.users.table.status.label'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize} />
                            </HeaderRow>
                        </Header>

                        <Body>
                            {tableList.map((item) => {
                                const {
                                    id,
                                    avatar,
                                    firstname,
                                    lastname,
                                    gender,
                                    email,
                                    postalCode,
                                    city,
                                    phoneNumber,
                                    confirmed,
                                    updatedAt,
                                } = item
                                return (
                                    <Row key={id} item={item}>
                                        <Cell>
                                            <Avatar
                                                size="xs"
                                                src={`${avatar?.contentUrl}`}
                                                name={`${firstname} ${lastname}`}
                                                mb={4}
                                                pos="relative"
                                            />
                                        </Cell>
                                        <Cell>{`${firstname} ${lastname}`}</Cell>
                                        <Cell>{email}</Cell>
                                        <Cell>{postalCode}</Cell>
                                        <Cell>{city}</Cell>
                                        <Cell>
                                            (+{phoneNumber.countryCode}){' '}
                                            {phoneNumber.nationalNumber}
                                        </Cell>
                                        <Cell>
                                            {(confirmed && (
                                                <Tooltip
                                                    label={moment(
                                                        updatedAt
                                                    ).format('ddd ll LT')}
                                                >
                                                    {confirmed && (
                                                        <Text color="green.400">
                                                            <Flex
                                                                gap={3}
                                                                alignItems="center"
                                                            >
                                                                {t(
                                                                    'admin.accountManagement.users.table.status.confirmed',
                                                                    {
                                                                        context:
                                                                            gender,
                                                                    }
                                                                )}
                                                            </Flex>
                                                        </Text>
                                                    )}
                                                </Tooltip>
                                            )) || (
                                                <Text color="gray.400">
                                                    {t(
                                                        'admin.accountManagement.users.table.status.notConfirmed'
                                                    )}
                                                </Text>
                                            )}
                                        </Cell>
                                        <Cell />
                                    </Row>
                                )
                            })}
                        </Body>
                    </>
                )
            }}
        </Table>
    )
}
