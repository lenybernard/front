import { Flex, IconButton, Input } from '@chakra-ui/react'
import React, { Dispatch, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { FaPlay } from 'react-icons/fa'
import { UsersQueryVariables } from '@_/graphql/api/generated'
import { Field } from '@components/ui/field'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'

type UsersFilterFormData = {
    firstname?: string
    lastname?: string
    email?: string
    postalCode?: string
    confirmed?: string
}

type Props = {
    setVariables: Dispatch<SetStateAction<UsersQueryVariables>>
}

export const UsersFilterForm = ({ setVariables }: Props) => {
    const { t } = useTranslation()
    const { register, handleSubmit } = useForm<UsersFilterFormData>({
        values: {
            confirmed: '0',
            postalCode: '',
            email: '',
        },
    })

    const onSubmit = ({
        firstname,
        lastname,
        postalCode,
        email,
        confirmed,
    }: UsersFilterFormData) => {
        setVariables({
            firstname: firstname !== '' ? firstname : undefined,
            lastname: lastname !== '' ? lastname : undefined,
            postalCode: postalCode !== '' ? postalCode : undefined,
            email: email !== '' ? email : undefined,
            confirmed: confirmed === '' ? undefined : confirmed === '1',
        })
    }
    const filterFields = [
        {
            label: t('admin.accountManagement.users.table.firstname'),
            input: <Input {...register('firstname')} />,
        },
        {
            label: t('admin.accountManagement.users.table.lastname'),
            input: <Input {...register('lastname')} />,
        },
        {
            label: t('admin.accountManagement.users.table.email'),
            input: <Input {...register('email')} />,
        },
        {
            label: t('admin.accountManagement.users.table.postalCode'),
            input: <Input {...register('postalCode')} />,
        },
        {
            label: t('admin.accountManagement.users.table.status.label'),
            input: (
                <NativeSelectRoot {...register('confirmed')}>
                    <NativeSelectField>
                        {[
                            { label: '' },
                            {
                                label: t(
                                    'admin.accountManagement.users.table.status.choices.notConfirmed'
                                ),
                                value: 0,
                            },
                            {
                                label: t(
                                    'admin.accountManagement.users.table.status.choices.confirmed'
                                ),
                                value: 1,
                            },
                        ].map(({ label, value }) => (
                            <option value={value} key={label}>
                                {label}
                            </option>
                        ))}
                    </NativeSelectField>
                </NativeSelectRoot>
            ),
        },
    ]

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Flex gap={3} mb={5} alignItems="center">
                {filterFields.map(({ label, input }) => (
                    <Field key={label} label={label}>
                        {input}
                    </Field>
                ))}
                <IconButton aria-label="filter" type="submit">
                    <FaPlay />
                </IconButton>
            </Flex>
        </form>
    )
}
