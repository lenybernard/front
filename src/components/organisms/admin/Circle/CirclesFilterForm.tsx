import { Flex, IconButton, Input } from '@chakra-ui/react'
import React, { Dispatch, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { FaPlay } from 'react-icons/fa'
import { CirclesQueryVariables } from '@_/graphql/api/generated'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'
import { Field } from '@components/ui/field'

type CirclesFilterFormData = {
    name?: string
    indexable?: string
}

type Props = {
    setVariables: Dispatch<SetStateAction<CirclesQueryVariables>>
}

export const CirclesFilterForm = ({ setVariables }: Props) => {
    const { t } = useTranslation()
    const { register, handleSubmit } = useForm<CirclesFilterFormData>({
        values: {
            indexable: '0',
            name: '',
        },
    })

    const onSubmit = ({ name, indexable }: CirclesFilterFormData) => {
        setVariables({
            name: name !== '' ? name : undefined,
            indexable: indexable === '' ? undefined : indexable === '1',
        })
    }
    const filterFields = [
        {
            label: t('admin.communities.table.name'),
            input: <Input {...register('name')} />,
        },
        {
            label: t('admin.communities.table.status.label'),
            input: (
                <NativeSelectRoot {...register('indexable')}>
                    <NativeSelectField>
                        {[
                            { label: '' },
                            {
                                label: t(
                                    'admin.communities.table.status.choices.notIndexed'
                                ),
                                value: 0,
                            },
                            {
                                label: t(
                                    'admin.communities.table.status.choices.indexed'
                                ),
                                value: 1,
                            },
                        ].map(({ label, value }) => (
                            <option value={value} key={label}>
                                {label}
                            </option>
                        ))}
                    </NativeSelectField>
                </NativeSelectRoot>
            ),
        },
    ]

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Flex gap={3} mb={5} alignItems="center">
                {filterFields.map(({ label, input }) => (
                    <Field key={label} label={label}>
                        {input}
                    </Field>
                ))}
                <IconButton aria-label="filter" type="submit">
                    <FaPlay />
                </IconButton>
            </Flex>
        </form>
    )
}
