import { Box, Link, SimpleGrid, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { Heading } from '@components/atoms/Heading'
import { PricingCard } from '@components/molecules/Marketing/Pricing/PricingCard'
import { useAuth } from '@_/auth/useAuth'
import { FaConfluence } from 'react-icons/fa'
import { usePricingView_PlansQuery } from '@_/graphql/api/generated'
import { Loader } from '@components/atoms/Loader/Loader'
import { SubscriptionRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { generatePath } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { Button } from '@components/ui/button'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'

export const PricingView = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    const { data, loading } = usePricingView_PlansQuery()

    return (
        <>
            <Helmet>
                <title>{t('pricing.title')}</title>
            </Helmet>
            <Box
                as="section"
                bg={useColorModeValue('gray.50', 'gray.800')}
                py="20"
            >
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <Heading
                        as="h1"
                        size="2xl"
                        fontWeight="extrabold"
                        textAlign={{ sm: 'center' }}
                    >
                        {t('pricing.title')}
                    </Heading>
                    <Text
                        mt="4"
                        maxW="xl"
                        mx="auto"
                        fontSize="xl"
                        color={useColorModeValue('gray.600', 'gray.400')}
                        textAlign={{ sm: 'center' }}
                    >
                        {t('pricing.description')}
                    </Text>
                    {(loading && <Loader />) ||
                        (data?.plans && (
                            <SimpleGrid
                                alignItems="center"
                                mt={{ base: '10', lg: '24' }}
                                columns={{ base: 1, lg: 3 }}
                                gap={{ base: '12', lg: '8' }}
                            >
                                {data.plans.map((plan, idx) => (
                                    <PricingCard
                                        key={plan.id}
                                        cta={{
                                            label:
                                                plan.button ?? !auth.user
                                                    ? t('global.start')
                                                    : t('pricing.activated'),
                                            to: plan.button
                                                ? generatePath(
                                                      SubscriptionRoutes.Subscribe,
                                                      {
                                                          slug: plan.slug,
                                                      }
                                                  )
                                                : UserRoutes.Login,
                                            isDisabled:
                                                !plan.button &&
                                                auth.user !== undefined,
                                            detail: plan.condition,
                                        }}
                                        popular={idx === 1}
                                        name={plan.name ?? ''}
                                        description={plan.description ?? ''}
                                        price={plan.price}
                                        features={
                                            plan.features?.map(
                                                (feature) => feature
                                            ) ?? []
                                        }
                                    />
                                ))}
                            </SimpleGrid>
                        ))}
                </Box>
            </Box>
            <Box as="section">
                <Box
                    maxW="2xl"
                    mx="auto"
                    px={{ base: '6', lg: '8' }}
                    py={{ base: '16', sm: '20' }}
                    textAlign="center"
                >
                    <Heading
                        as="h2"
                        size="3xl"
                        fontWeight="extrabold"
                        letterSpacing="tight"
                    >
                        <EmphasisText color={'pink.400'}>
                            {t('pricing.selfHosted.title')}
                        </EmphasisText>
                    </Heading>
                    <Text mt="4" fontSize="lg">
                        {t('pricing.selfHosted.description')}
                    </Text>
                    <Button
                        mt="8"
                        asChild
                        size="lg"
                        colorPalette="blue"
                        fontWeight="bold"
                    >
                        <Link
                            href="https://letscommuno.atlassian.net/wiki/spaces/TECH/pages/8224797/Deploying"
                            target="_blank"
                        >
                            <FaConfluence />
                            {t('pricing.selfHosted.cta')}
                        </Link>
                    </Button>
                </Box>
            </Box>
        </>
    )
}
