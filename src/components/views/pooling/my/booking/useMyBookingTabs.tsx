import { useTranslation } from 'react-i18next'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { TabItem } from '@components/layout/TabNavigationLayout'

export const enum MyBookingTabs {
    AS_OWNER = 'AS_OWNER',
    REQUESTS = 'REQUESTS',
}

export const useMyBookingTabs = (): TabItem[] => {
    const { t } = useTranslation()

    return [
        {
            key: MyBookingTabs.AS_OWNER,
            name: t('pooling.booking.index.title', {
                context: 'as_owner',
            }),
            path: PoolingRoutes.MyBookingAsOwner,
        },
        {
            key: MyBookingTabs.REQUESTS,
            name: t('pooling.booking.index.title', {
                context: 'my_requests',
            }),
            path: PoolingRoutes.MyBookingRequests,
        },
    ]
}
