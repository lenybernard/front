import * as React from 'react'
import { Card, Container, Heading } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Loader } from '@components/atoms/Loader/Loader'
import 'moment/locale/fr'
import { BookingTable } from '@components/molecules/Table/Material/Booking/BookingTable'
import { useMaterialBookingsForApplicantQuery } from '@_/graphql/api/generated'
import {
    inUserSpace,
    WithUserProps,
} from '@components/templates/AccountManagement/inUserSpace'
import { TabNavigationLayout } from '@components/layout/TabNavigationLayout'
import {
    MyBookingTabs,
    useMyBookingTabs,
} from '@components/views/pooling/my/booking/useMyBookingTabs'
import { useColorModeValue } from '@components/ui/color-mode'

export const MyBookingsRequests = ({ user: { slug } }: WithUserProps) => {
    const { t } = useTranslation()
    const variables = {
        slug,
        statuses: ['pending', 'confirmed', 'closed', 'canceled'],
    }
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const tabs = useMyBookingTabs()
    const { loading, error, data } = useMaterialBookingsForApplicantQuery({
        variables,
    })
    const materialBookings = data?.materialBookings

    return (
        (loading && <Loader />) ||
        (error && <div>Error :(</div>) || (
            <>
                <Helmet>
                    <title>
                        {t('pooling.booking.index.title')}{' '}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Container>
                    <Heading size="3xl" py="6">
                        {t('pooling.booking.index.title')}
                    </Heading>
                    <Card.Root mb="20" rounded="2xl" bgColor={cardBgColor}>
                        <Card.Body gap={10}>
                            <TabNavigationLayout
                                {...{
                                    tabs,
                                    activeTab: MyBookingTabs.REQUESTS,
                                    actions: [],
                                }}
                            >
                                {materialBookings && (
                                    <BookingTable
                                        materialBookings={materialBookings}
                                        side={'owner'}
                                    />
                                )}
                            </TabNavigationLayout>
                        </Card.Body>
                    </Card.Root>
                </Container>
            </>
        )
    )
}

export const MyBookingsRequestsView = inUserSpace(MyBookingsRequests)
