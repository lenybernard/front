import * as React from 'react'
import { useState } from 'react'
import {
    Box,
    Card,
    Container,
    Flex,
    Heading,
    SimpleGrid,
    Spacer,
    Stack,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { PrimaryButton } from '@components/atoms/Button'
import { Button } from '@components/ui/button'
import { Loader } from '@components/atoms/Loader/Loader'
import { toast } from 'react-toastify'
import { CtaVertical } from '@components/molecules/CTA/CtaVertical'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import {
    MyListsDocument,
    useMyList_DeleteListMutation,
    useMyListsQuery,
} from '@_/graphql/api/generated'
import { useAuth } from '@_/auth/useAuth'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'
import {
    UserListRow,
    useUserListTableColumns,
} from '@components/molecules/Table/User/UserLists/useUserListTableColumns'
import { Table } from '@components/molecules/Table/Table'
import { useViewMode, ViewMode } from '@_/hooks/useViewMode'
import { UserStackedAvatars } from '@components/molecules/User/UserStackedAvatars'
import { FluidCard } from '@components/atoms/Animation/FluidCard'
import { generatePath, Link } from 'react-router'
import { ToggleViewTableAndCardMode } from '@components/atoms/Button/ToggleViewTableAndCardMode'
import { inUserSpace } from '@components/templates/AccountManagement/inUserSpace'
import { NewUserListModal } from '@components/views/pooling/my/lists/NewUserListModal'
import { TabNavigationLayout } from '@_/components/layout/TabNavigationLayout'
import { useColorModeValue } from '@components/ui/color-mode'

const MyLists = () => {
    const { t } = useTranslation()
    const [afterCursor, setAfterCursor] = useState<string>()
    const [beforeCursor, setBeforeCursor] = useState<string>()
    const [viewMode, setViewMode] = useViewMode<ViewMode.Table | ViewMode.Card>(
        ViewMode.Card
    )

    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const nbToFetch = 8 // 2 x 4 cards
    const variables = {
        afterCursor,
        beforeCursor,
        first: nbToFetch,
    }
    const [deleting, setDeleting] = useState(false)
    const { data, loading } = useMyListsQuery({
        variables,
    })
    const [deleteList] = useMyList_DeleteListMutation()

    const onDelete = async (id: string) => {
        setDeleting(true)
        deleteList({
            variables: {
                id,
            },
            refetchQueries: [
                {
                    query: MyListsDocument,
                    variables,
                },
            ],
            onCompleted: () => {
                setDeleting(false)
                toast.success(
                    t('pooling.my_lists.index.actions.delete.success')
                )
            },
        })
    }
    const auth = useAuth()
    const columns = useUserListTableColumns()
    const colorModeBG = useColorModeValue('gray.300', 'gray.800')
    const rows = data?.userLists?.edges?.map((edge) => {
        const node = edge.node!

        return {
            slug: node.slug,
            name: node.name,
            users: node.users || [],
            actions: (
                <Flex
                    alignItems="center"
                    gap={3}
                    justifyContent="flex-end"
                    flexWrap="wrap"
                >
                    <Link
                        to={generatePath(UserRoutes.EditMyList, {
                            slug: node.slug,
                        })}
                    >
                        <PrimaryButton>{t('global.edit')}</PrimaryButton>
                    </Link>
                    <Button
                        onClick={() => onDelete(node.id!)}
                        disabled={deleting}
                    >
                        {t('global.delete')}
                    </Button>
                </Flex>
            ),
        }
    }) as UserListRow[]

    if (!loading && data?.userLists?.edges) {
        const { pageInfo, totalCount } = data.userLists
        return (
            <>
                <Helmet>
                    <title>
                        {t('pooling.my_lists.meta.title')}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <Container>
                    <Heading size="3xl" py="6">
                        {t('pooling.my_lists.index.title')}
                    </Heading>

                    <Card.Root mb="20" rounded="2xl" bgColor={cardBgColor}>
                        <Card.Body gap={10}>
                            <TabNavigationLayout
                                {...{
                                    tabs: [],
                                    actions: [
                                        {
                                            key: 'toggleViewMode',
                                            component: (
                                                <ToggleViewTableAndCardMode
                                                    viewMode={viewMode}
                                                    onClick={setViewMode}
                                                />
                                            ),
                                        },
                                        {
                                            key: 'newListButton',
                                            component: (
                                                <NewUserListModal
                                                    label={t(
                                                        'pooling.my_lists.index.actions.new'
                                                    )}
                                                />
                                            ),
                                        },
                                    ],
                                }}
                            >
                                {(loading && <Loader />) ||
                                    (data.userLists?.edges?.length > 0 && (
                                        <>
                                            {viewMode === ViewMode.Table ? (
                                                <Table<UserListRow>
                                                    columns={columns}
                                                    data={rows}
                                                    enableSorting={false}
                                                ></Table>
                                            ) : (
                                                <SimpleGrid
                                                    columns={{
                                                        base: 1,
                                                        md: 3,
                                                        lg: 4,
                                                    }}
                                                    gap={10}
                                                    mt={10}
                                                >
                                                    {rows.map((row) => {
                                                        const extraCount =
                                                            row.users!
                                                                .totalCount! -
                                                            row.users!.edges!
                                                                .length
                                                        return (
                                                            <Link
                                                                key={row.name}
                                                                to={generatePath(
                                                                    UserRoutes.EditMyList,
                                                                    {
                                                                        slug: row.slug,
                                                                    }
                                                                )}
                                                            >
                                                                <FluidCard>
                                                                    <Card.Root
                                                                        maxW="sm"
                                                                        bg={
                                                                            colorModeBG
                                                                        }
                                                                        rounded={
                                                                            '2xl'
                                                                        }
                                                                    >
                                                                        <Card.Body>
                                                                            <Stack
                                                                                mt="6"
                                                                                gap="3"
                                                                            >
                                                                                <Heading size="md">
                                                                                    {
                                                                                        row.name
                                                                                    }
                                                                                </Heading>
                                                                                <Text>
                                                                                    {t(
                                                                                        'pooling.my_lists.index.users.numberInList',
                                                                                        {
                                                                                            count: row
                                                                                                .users
                                                                                                ?.totalCount,
                                                                                            context:
                                                                                                row
                                                                                                    .users
                                                                                                    ?.totalCount! >
                                                                                                1
                                                                                                    ? 'plural'
                                                                                                    : undefined,
                                                                                        }
                                                                                    )}
                                                                                </Text>
                                                                                <Flex justifyContent="center">
                                                                                    <UserStackedAvatars
                                                                                        users={row.users?.edges?.map(
                                                                                            (
                                                                                                edge
                                                                                            ) =>
                                                                                                edge.node!
                                                                                        )}
                                                                                    />
                                                                                    {extraCount >
                                                                                        0 && (
                                                                                        <Text
                                                                                            ml={
                                                                                                2
                                                                                            }
                                                                                            fontWeight="bold"
                                                                                            color="gray.500"
                                                                                        >
                                                                                            +
                                                                                            {
                                                                                                extraCount
                                                                                            }
                                                                                        </Text>
                                                                                    )}
                                                                                </Flex>
                                                                            </Stack>
                                                                        </Card.Body>
                                                                    </Card.Root>
                                                                </FluidCard>
                                                            </Link>
                                                        )
                                                    })}
                                                </SimpleGrid>
                                            )}
                                            <Flex
                                                gap={5}
                                                mt={10}
                                                justifyContent={'center'}
                                            >
                                                {pageInfo &&
                                                    nbToFetch <
                                                        (totalCount || 0) && (
                                                        <Button
                                                            disabled={
                                                                !pageInfo.hasPreviousPage
                                                            }
                                                            onClick={async () => {
                                                                setBeforeCursor(
                                                                    pageInfo?.startCursor
                                                                )
                                                                setAfterCursor(
                                                                    undefined
                                                                )
                                                            }}
                                                        >
                                                            <FaChevronLeft />
                                                            {t(
                                                                'global.previous'
                                                            )}
                                                        </Button>
                                                    )}
                                                {pageInfo && (
                                                    <Button
                                                        disabled={
                                                            !pageInfo.hasNextPage
                                                        }
                                                        onClick={async () => {
                                                            setAfterCursor(
                                                                pageInfo?.endCursor
                                                            )
                                                            setBeforeCursor(
                                                                undefined
                                                            )
                                                        }}
                                                    >
                                                        {t('global.next')}
                                                        <FaChevronRight />
                                                    </Button>
                                                )}
                                            </Flex>
                                        </>
                                    )) ||
                                    (data.userLists?.edges?.length === 0 && (
                                        <CtaVertical
                                            title={t(
                                                'pooling.my_lists.index.empty.title'
                                            )}
                                            description={t(
                                                'pooling.my_lists.index.empty.description',
                                                {
                                                    context: auth?.user?.gender,
                                                }
                                            )}
                                            emphasisColor="purple.400"
                                            buttons={[
                                                {
                                                    key: 'search_btn',
                                                    component: (
                                                        <NewUserListModal
                                                            label={t(
                                                                'pooling.my_lists.index.empty.button'
                                                            )}
                                                        />
                                                    ),
                                                },
                                            ]}
                                        />
                                    ))}
                            </TabNavigationLayout>
                        </Card.Body>
                    </Card.Root>
                </Container>
                <Box display={'none'} textAlign="center" fontSize="xl" w="100%">
                    <Heading my={4}>
                        <Flex alignItems="center" gap={3}>
                            {t('pooling.my_lists.index.title')}

                            <Spacer />
                            <ToggleViewTableAndCardMode
                                viewMode={viewMode}
                                onClick={setViewMode}
                            />
                        </Flex>
                    </Heading>
                </Box>
            </>
        )
    }
    return <Loader />
}

export const MyListsView = inUserSpace(MyLists)
