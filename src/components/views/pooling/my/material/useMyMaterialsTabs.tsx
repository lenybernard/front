import { useTranslation } from 'react-i18next'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { TabItem } from '@components/layout/TabNavigationLayout'
import { HStack } from '@chakra-ui/react'
import { Tag } from '@components/ui/tag'

export const enum MyMaterialsTabs {
    MY_PERSONAL_MATERIALS = 'MY_PERSONAL_MATERIALS',
    MY_CIRCLES_MATERIALS = 'MY_CIRCLES_MATERIALS',
}

export const useMyMaterialsTabs = (): TabItem[] => {
    const { t } = useTranslation()

    return [
        {
            key: MyMaterialsTabs.MY_PERSONAL_MATERIALS,
            name: t('pooling.my_materials.index.title'),
            path: PoolingRoutes.MyMaterials,
        },
        {
            key: MyMaterialsTabs.MY_CIRCLES_MATERIALS,
            name: (
                <HStack>
                    <Tag colorPalette={'pink'}>{t('global.soon')}</Tag>
                    {t('pooling.my_materials.index.title', {
                        context: 'communities',
                    })}
                </HStack>
            ),
            disabled: true,
        },
    ]
}
