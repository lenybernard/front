import { UnavailableModal__MaterialBookingFragment } from '@_/graphql/api/generated'

export type Props = {
    isOpen: boolean
    close: () => void
    onDelete: ({ variables }: { variables: { id: string } }) => Promise<unknown>
    booking: UnavailableModal__MaterialBookingFragment
}
