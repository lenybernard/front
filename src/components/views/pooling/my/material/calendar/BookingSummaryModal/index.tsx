import React from 'react'
import { Box, Flex } from '@chakra-ui/react'
import { Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import moment, { locale } from 'moment'
import { MaterialHorizontalCard } from '@components/molecules/Cards/MaterialHorizontalCard'
import { UserCard } from '@components/molecules/Cards/UserCard/UserCard'
import { BookingPriceStat } from '@components/atoms/Stat/Material/Booking/BookingPriceStat'
import { Button } from '@components/ui/button'
import 'moment/locale/fr'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { Props } from './types'

export const BookingSummaryModal = ({ isOpen, close, booking }: Props) => {
    const { t, i18n } = useTranslation()
    locale(i18n.language)

    return (
        <DialogRoot
            lazyMount
            isOpen={isOpen}
            onClose={close}
            placement="center"
        >
            <DialogBackdrop />
            <DialogContent>
                <DialogHeader>
                    {t('pooling.booking.calendar.modal.title', {
                        booking,
                        dateOfCreation: moment(booking.createdAt).format('L'),
                        user: booking.user?.fullname,
                    })}
                </DialogHeader>
                <DialogCloseTrigger />
                <DialogBody>
                    <Flex gap={5}>
                        <Box>
                            {booking.material && (
                                <MaterialHorizontalCard
                                    material={booking.material}
                                />
                            )}
                        </Box>
                        {booking.user && (
                            <Box>
                                <UserCard user={booking.user} />
                            </Box>
                        )}
                        <Box>
                            <Flex direction="column" gap={10}>
                                <BookingPriceStat
                                    price={booking.price || 0}
                                    periods={
                                        booking.periods?.map((period) => {
                                            const { id, startDate, endDate } =
                                                period
                                            return {
                                                id,
                                                startDate,
                                                endDate,
                                            }
                                        }) ?? []
                                    }
                                />
                            </Flex>
                        </Box>
                    </Flex>
                </DialogBody>
                <DialogFooter>
                    <Link to={`/my/bookings/${booking.slug}`}>
                        <Button variant="ghost" colorPalette="yellow">
                            {t('pooling.booking.calendar.modal.manage')}
                        </Button>
                    </Link>
                </DialogFooter>
            </DialogContent>
        </DialogRoot>
    )
}
