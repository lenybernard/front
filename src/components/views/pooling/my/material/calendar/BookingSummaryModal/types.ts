import { BookingSummaryModal__MaterialBookingFragment } from '@_/graphql/api/generated'

export type Props = {
    isOpen: boolean
    close: () => void
    booking: BookingSummaryModal__MaterialBookingFragment
}
