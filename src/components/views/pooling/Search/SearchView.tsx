import * as React from 'react'
import { useCallback, useEffect, useState } from 'react'
import { useSearchParams } from 'react-router'
import { Box, Flex, Grid, Heading, Spacer, VStack } from '@chakra-ui/react'

import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import {
    MaterialPageInfo,
    SearchAlertFragment,
    SearchMaterialFragment,
    SearchMaterialsDocument,
    SearchMaterialsQuery,
} from '@_/graphql/api/generated'
import { Blur } from '@components/layout/Blur'
import { client } from '@_/apollo/main/client'
import { Loader } from '@components/atoms/Loader/Loader'
import { useSearch } from '@_/contexts/Circles/SearchContext'
import { PaginatedMaterialList } from '@components/molecules/Material/PaginatedMaterialList'

export const POOLING_MATERIALS_SEARCH_VIEW_MODE = 'searchViewMode'
export const SearchView = () => {
    const { t } = useTranslation()
    const [materials, setMaterials] = useState<SearchMaterialFragment[]>()
    const [alert, setAlert] = useState<SearchAlertFragment>()
    const [afterCursor, setAfterCursor] = useState<string>()
    const [beforeCursor, setBeforeCursor] = useState<string>()
    const [pageInfo, setPageInfo] = useState<MaterialPageInfo>()
    const [totalCount, setTotalCount] = useState<number>(0)
    const [loading, setLoading] = useState<boolean>(false)
    const nbToFetch = 24 // 5 lines of 4 cards

    const { searchTerms, setSearchTerms, tempSearchTerms, setTempSearchTerms } =
        useSearch()
    const [searchParams] = useSearchParams()

    useEffect(() => {
        const newSearch = searchParams.get('q') || ''

        if (tempSearchTerms !== newSearch) {
            setTempSearchTerms(newSearch)
        }

        if (searchTerms !== newSearch) {
            setSearchTerms(newSearch)
        }
    }, [searchParams])

    const fetchMaterials = useCallback(() => {
        setLoading(true)
        client
            .query<SearchMaterialsQuery>({
                query: SearchMaterialsDocument,
                variables: {
                    searchTerms,
                    beforeCursor,
                    afterCursor,
                    nbToFetch,
                },
                errorPolicy: 'ignore',
                fetchPolicy: 'no-cache',
            })
            .then((res) => {
                setLoading(false)
                setAlert(res.data?.materialSearchAlerts?.edges?.[0]?.node)
                if (!res.data?.searchMaterials) {
                    return
                }
                setMaterials(
                    res.data?.searchMaterials?.edges
                        ?.map((edge) => edge.node!)
                        .filter(Boolean) || []
                )
                setPageInfo(res.data?.searchMaterials?.pageInfo || null)
                setTotalCount(res.data?.searchMaterials?.totalCount)
            })
    }, [searchTerms, beforeCursor, afterCursor, nbToFetch])

    useEffect(() => {
        console.log('SearchTerms updated:', searchTerms)
        fetchMaterials()
    }, [searchTerms, fetchMaterials])

    useEffect(() => {
        const currentSearch = searchParams.get('q') || ''
        if (searchTerms !== currentSearch) {
            setTempSearchTerms(currentSearch)
        }
    }, [searchTerms, searchParams, setTempSearchTerms])

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.index.title')} {t('meta.title.suffix')}
                </title>
            </Helmet>
            <Box textAlign="center" fontSize="xl" mt={{ base: 3, sm: 10 }}>
                <Grid p={4}>
                    <Blur
                        position="fixed"
                        bottom={10}
                        left={-10}
                        style={{ filter: 'blur(70px)' }}
                    />
                    <VStack>
                        <Heading as="h1">
                            <Flex direction={'row'} alignItems="center">
                                <Spacer />
                                {t('pooling.index.title')}
                            </Flex>
                        </Heading>

                        {(loading && <Loader />) ||
                            (materials && (
                                <PaginatedMaterialList
                                    materials={materials}
                                    pageInfo={
                                        pageInfo || ({} as MaterialPageInfo)
                                    }
                                    totalCount={totalCount}
                                    loading={loading}
                                    setBeforeCursor={setBeforeCursor}
                                    setAfterCursor={setAfterCursor}
                                    alert={alert}
                                    searchTerms={searchTerms}
                                />
                            ))}
                    </VStack>
                </Grid>
            </Box>
        </>
    )
}
