import { Box, Center, Container, VStack } from '@chakra-ui/react'
import * as React from 'react'
import { Footer } from '@components/molecules/Footer/Footer'
import { H1 } from '@components/atoms/Heading'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { Error } from '@components/atoms/Illustrations/Error'

export const ErrorPage = () => {
    return (
        <Box className={'dark'}>
            <Box minH="100vh" bg="gray.700">
                <Center>
                    <VStack gap={5} mt={100} mb={25}>
                        <H1>
                            <EmphasisText color="yellow.400">
                                Oh no...
                            </EmphasisText>
                        </H1>
                        <Container>
                            <Box color="white" textAlign="center">
                                Something bad happened
                            </Box>
                        </Container>
                        <Error />
                        <Box>
                            <a href="/">Please retry</a>
                        </Box>
                    </VStack>
                </Center>
                <Footer />
            </Box>
        </Box>
    )
}
