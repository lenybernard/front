import {
    Box,
    Container,
    Flex,
    Heading,
    HStack,
    Link as ChakraLink,
    Stack,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { ReactNode } from 'react'
import { Helmet } from 'react-helmet'
import { FaDiscord, FaEnvelope, FaHeart } from 'react-icons/fa'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { PrimaryButton } from '@components/atoms/Button'
import { PicFeature } from '@components/views/HomeView'
import { StaticPagesRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { useColorModeValue } from '@components/ui/color-mode'
import { Link } from 'react-router'

export const Page = ({ children }: { children: ReactNode }) => {
    return (
        <Box bgColor={useColorModeValue('gray.50', 'gray.800')} padding={50}>
            {children}
        </Box>
    )
}

export const ContributeView = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('contribute.title')}</title>
            </Helmet>
            <Page>
                <Container minW={'5xl'}>
                    <Stack
                        as={Box}
                        textAlign="center"
                        gap={{ base: 8, md: 14 }}
                        py={{ base: 20, md: 36 }}
                    >
                        <Heading
                            fontWeight={600}
                            fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
                            lineHeight="110%"
                        >
                            <EmphasisText color="green.400">
                                {t('contribute.how.title')}
                            </EmphasisText>
                        </Heading>
                        <Text color="gray.200" fontSize={'large'}>
                            <EmphasisText color="yellow.400">
                                {t('contribute.how.description')}
                            </EmphasisText>
                        </Text>
                        <PicFeature
                            feature={{
                                title: t(
                                    'contribute.how.financialContribution.title'
                                ),
                                description: t(
                                    'contribute.how.financialContribution.description'
                                ),
                                image: `/images/contribute/money.webp`,
                                button: (
                                    <PrimaryButton size="lg" px="8" asChild>
                                        <ChakraLink
                                            href={
                                                'https://www.helloasso.com/associations/getigne-collectif/formulaires/1'
                                            }
                                        >
                                            {t(
                                                'contribute.how.financialContribution.cta'
                                            )}
                                            <FaHeart />
                                        </ChakraLink>
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="left"
                        />
                        <PicFeature
                            feature={{
                                title: t('contribute.how.code.title'),
                                description: t(
                                    'contribute.how.code.description'
                                ),
                                image: `/images/contribute/code.webp`,
                                button: (
                                    <PrimaryButton
                                        link="https://gitlab.com/communo"
                                        size="lg"
                                        px="8"
                                    >
                                        {t('contribute.how.code.cta')}
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="right"
                        />
                        <PicFeature
                            feature={{
                                title: t('contribute.how.design.title'),
                                description: t(
                                    'contribute.how.design.description'
                                ),
                                image: `/images/contribute/design.webp`,
                                button: (
                                    <PrimaryButton
                                        link={StaticPagesRoutes.Contact}
                                        size="lg"
                                        px="8"
                                    >
                                        {t('contribute.how.design.cta')}
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="left"
                        />
                        <PicFeature
                            feature={{
                                title: t('contribute.how.writing.title'),
                                description: t(
                                    'contribute.how.writing.description'
                                ),
                                image: `/images/contribute/writing.webp`,
                                button: (
                                    <PrimaryButton
                                        link={StaticPagesRoutes.Contact}
                                        size="lg"
                                        px="8"
                                    >
                                        {t('contribute.how.writing.cta')}
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="right"
                        />
                        <PicFeature
                            feature={{
                                title: t('contribute.how.communication.title'),
                                description: t(
                                    'contribute.how.communication.description'
                                ),
                                image: `/images/contribute/communication.webp`,
                                button: (
                                    <PrimaryButton
                                        link={StaticPagesRoutes.Contact}
                                        size="lg"
                                        px="8"
                                    >
                                        {t('contribute.how.communication.cta')}
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="left"
                        />
                        <PicFeature
                            feature={{
                                title: t('contribute.how.yourTalent.title'),
                                description: t(
                                    'contribute.how.yourTalent.description'
                                ),
                                image: `/images/contribute/creativity.webp`,
                                button: (
                                    <PrimaryButton
                                        link="https://gitlab.com/communo"
                                        size="lg"
                                        px="8"
                                    >
                                        {t('contribute.how.yourTalent.cta')}
                                    </PrimaryButton>
                                ),
                            }}
                            picturePosition="right"
                        />
                    </Stack>
                </Container>
            </Page>
            <Flex
                py={12}
                textAlign="center"
                alignItems={'center'}
                gap={5}
                direction={'column'}
            >
                <Heading>{t('contribute.how.letsTalk.title')}</Heading>
                <HStack>
                    {process.env.REACT_APP_DISCORD_INVITATION_LINK && (
                        <Button
                            asChild
                            colorPalette="green"
                            bg="green.400"
                            rounded="full"
                            px={6}
                            size="lg"
                            _hover={{
                                bg: 'green.500',
                            }}
                        >
                            <ChakraLink
                                href={
                                    process.env
                                        .REACT_APP_DISCORD_INVITATION_LINK
                                }
                            >
                                <FaDiscord />

                                {t('contribute.how.letsTalk.joinDiscord')}
                            </ChakraLink>
                        </Button>
                    )}
                    <Button
                        asChild
                        colorPalette="yellow"
                        rounded="full"
                        px={6}
                        size="lg"
                        _hover={{
                            bg: 'green.500',
                        }}
                    >
                        <Link to={StaticPagesRoutes.Contact}>
                            <FaEnvelope />
                            {t('contribute.how.letsTalk.contactForm')}
                        </Link>
                    </Button>
                </HStack>
            </Flex>
        </>
    )
}
