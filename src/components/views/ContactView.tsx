import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { SubmitHandler } from 'react-hook-form'
import {
    ContactFormData,
    ContactFormWithSocialButtons,
} from '@components/molecules/Contact/contactWithSocialButtons'
import { ThankYou } from '@components/molecules/ThankYou/ThankYou'
import { useContactMutation } from '@_/graphql/api/generated'

export const ContactView = () => {
    const { t } = useTranslation()
    const [contact] = useContactMutation()
    const [hasSent, setSent] = useState(false)
    const onSubmit: SubmitHandler<ContactFormData> = (data) => {
        contact({
            variables: {
                input: data,
            },
            onCompleted: (res) => {
                if (res.createContact?.contact) {
                    setSent(true)
                }
            },
        })
    }

    return (
        <>
            <Helmet>
                <title>{t('contact.title')}</title>
            </Helmet>
            {(!hasSent && (
                <ContactFormWithSocialButtons
                    title={t('contact.title')}
                    subtitle={t('contact.subtitle')}
                    onSubmit={onSubmit}
                />
            )) || (
                <ThankYou
                    title={t('contact.thankYou.title')}
                    subtitle={t('contact.thankYou.subtitle')}
                />
            )}
        </>
    )
}
