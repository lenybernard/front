import React, { useEffect, useState } from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { useWelcomeStep } from '@components/views/accountManagement/registerWizard/steps/0welcome/useWelcomeStep'
import { usePersonalInformationsStep } from '@components/views/accountManagement/registerWizard/steps/2personalInformations/usePersonalInformationsStep'
import { useCredentialsStep } from '@components/views/accountManagement/registerWizard/steps/1credentials/useCredentialsStep'
import { createStore, GlobalState, useStateMachine } from 'little-state-machine'
import { withRouter } from '@components/views/withRouter'
import { useLocationStep } from '@components/views/accountManagement/registerWizard/steps/3location/useLocationStep'
import { useCongratulationsStep } from '@components/views/accountManagement/registerWizard/steps/congratulations/useCongratulationsStep'
import { usePreferencesStep } from '@components/views/accountManagement/registerWizard/steps/4preferences/usePreferencesStep'
import { useRegisterWizardHomeView_InvitationByTokenLazyQuery } from '@_/graphql/api/generated'
import { generatePath, useSearchParams } from 'react-router'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import {
    defaultStore,
    StepWizardProvider,
    useStepWizard,
} from './StepWizardContext'

createStore(defaultStore)

export const RegisterWizardHomeInner = ({ token }: { token?: string }) => {
    const { currentStep, setTotalSteps } = useStepWizard()
    const steps = [
        useWelcomeStep(),
        useCredentialsStep(),
        usePersonalInformationsStep(),
        useLocationStep(),
        usePreferencesStep(),
        useCongratulationsStep(),
    ]
    useEffect(() => {
        setTotalSteps(steps.length - 1)
    }, [steps.length, setTotalSteps])
    const { title, description, right, left } = steps[currentStep]

    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}

export const RegisterWizardHomeView = withRouter(() => {
    const [currentStep, setCurrentStep] = useState(0)
    const [searchParams] = useSearchParams()
    const token = searchParams.get('token') ?? undefined
    const updateState = (
        state: GlobalState,
        payload: {
            email: string
            firstname: string
            lastname: string
        }
    ) => {
        return {
            ...state,
            credentials: {
                ...state.credentials,
                email: payload.email,
            },
            personalInformations: {
                ...state.personalInformations,
                firstname: payload.firstname,
                lastname: payload.lastname,
            },
        }
    }

    const { actions } = useStateMachine({
        actions: { updateState },
    })
    const [findInvitation] =
        useRegisterWizardHomeView_InvitationByTokenLazyQuery()

    useEffect(() => {
        if (token) {
            findInvitation({
                variables: {
                    id: generatePath(UserRoutes.InvitationsIRI, {
                        id: token! ?? '',
                    }),
                },
                onCompleted: (r) => {
                    if (r?.invitation) {
                        actions.updateState({
                            email: r.invitation.email,
                            firstname: r.invitation.firstname ?? '',
                            lastname: r.invitation.lastname ?? '',
                        })
                    }
                },
            })
        }
    }, [token, findInvitation, actions])

    return (
        <StepWizardProvider
            currentStep={currentStep}
            setCurrentStep={setCurrentStep}
        >
            <RegisterWizardHomeInner />
        </StepWizardProvider>
    )
})
