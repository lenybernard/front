import 'mapbox-gl/dist/mapbox-gl.css'
import { Flex, Stack, Textarea } from '@chakra-ui/react'
import { Field } from '@components/ui/field'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'
import { RegisterWizardLocationFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { useForm } from 'react-hook-form'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { GlobalState, useStateMachine } from 'little-state-machine'
import { useCallback } from 'react'
import './map.scss'
import { SimpleMap } from '@components/views/accountManagement/registerWizard/steps/3location/SimpleMap'
import { toast } from 'react-toastify'
import { FaLock, FaMapPin } from 'react-icons/fa'
import { PrevNextNav } from '@components/molecules/Pager/PrevNextNav'

const META = () => {
    const { t } = useTranslation()
    return {
        title: t('register.wizard.location.title'),
        description: t('register.wizard.location.description'),
    }
}

export const useLocationStep = () => {
    const { t } = useTranslation()
    const { prev, next } = useStepWizard()
    const updateLocation = (
        state: GlobalState,
        payload: RegisterWizardLocationFormData
    ) => {
        return {
            ...state,
            location: {
                ...state.location,
                ...payload,
            },
        }
    }
    const { actions, state } = useStateMachine({
        actions: { updateLocation },
    })

    const { register, handleSubmit, watch, setValue } =
        useForm<RegisterWizardLocationFormData>({
            defaultValues: state.location,
        })

    const address = watch('address')

    const onGeocode = useCallback(
        (data: Partial<RegisterWizardLocationFormData>) => {
            if (data && Object.keys(data).length > 0) {
                // Mise à jour locale
                Object.entries(data).forEach(([key, value]) => {
                    setValue(
                        key as keyof RegisterWizardLocationFormData,
                        value as string
                    )
                })
            } else {
                toast.error(t('geocode.error.title'))
            }
        },
        [setValue, toast, t]
    )

    const onSubmit = useCallback(
        (data: RegisterWizardLocationFormData) => {
            actions.updateLocation(data)
            next()
        },
        [actions, next]
    )

    return {
        ...META(),
        left: (
            <form onSubmit={handleSubmit(onSubmit)}>
                <Stack
                    gap={{ base: '6', md: '8' }}
                    bg={useColorModeValue('white', 'gray.800')}
                    p="8"
                    rounded="2xl"
                    shadow="lg"
                >
                    <Stack gap={{ base: '4', md: '6' }}>
                        <Field
                            label={
                                <Flex gap="2">
                                    <FaMapPin />
                                    {t(
                                        'register.wizard.location.fields.address.label'
                                    )}
                                </Flex>
                            }
                            helperText={
                                <Flex gap="2" align={'center'}>
                                    <FaLock />
                                    {t(
                                        'register.wizard.location.fields.address.helperText'
                                    )}
                                </Flex>
                            }
                        >
                            <Textarea
                                {...register('address')}
                                size="lg"
                                rows={4}
                                resize="none"
                            />
                        </Field>
                    </Stack>
                    <Flex justify={'center'}>
                        <PrevNextNav
                            prev={{
                                onClick: prev,
                                title: t('global.previous', {
                                    context: 'step',
                                }),
                            }}
                            next={{
                                type: 'submit',
                                title: t('global.next', { context: 'step' }),
                            }}
                        />
                    </Flex>
                </Stack>
            </form>
        ),
        right: (
            <Flex align="center" h="full" w="full">
                <SimpleMap
                    address={address}
                    onGeocode={onGeocode}
                    enableMarkerDrag={true}
                    onMarkerDragEnd={(coordinates) => {
                        setValue('latitude', coordinates.lat.toString())
                        setValue('longitude', coordinates.lng.toString())
                    }}
                />
            </Flex>
        ),
    }
}
