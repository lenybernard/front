import { useColorModeValue } from '@components/ui/color-mode'
import { Heading } from '@components/atoms/Heading'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { ThemeModeForm } from '@components/molecules/Form/AccountManagement/User/Preferences/Appearence/ThemeModeForm'
import { ToggleForm } from '@components/atoms/Form/ToggleForm'
import { Field } from '@components/ui/field'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'
import { CommunicationMode } from '@_/graphql/api/generated'
import { PrevNextNav } from '@components/molecules/Pager/PrevNextNav'
import { Controller, useFormContext } from 'react-hook-form'
import { RegisterWizardPreferencesFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { Flex, Stack, Text, VStack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'

export const PreferencesForm = ({
    onSubmit,
}: {
    onSubmit: (data: RegisterWizardPreferencesFormData) => void
}) => {
    const { handleSubmit, register, control, watch } =
        useFormContext<RegisterWizardPreferencesFormData>()
    const { prev } = useStepWizard()

    const watchedData = watch()
    const { t } = useTranslation()

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack
                gap={{ base: '6', md: '8' }}
                bg={useColorModeValue('white', 'gray.800')}
                p="8"
                rounded="2xl"
                shadow="lg"
            >
                <Flex mt="8" gap={5} direction="column">
                    <VStack gap="3" textAlign="center">
                        <Heading fontWeight="medium">
                            {t('register.wizard.preferences.themeMode.title')}
                        </Heading>
                        <EmphasisText color="pink.400">
                            {t(
                                'register.wizard.preferences.themeMode.description'
                            )}
                        </EmphasisText>
                        <ThemeModeForm />
                    </VStack>
                    <Stack gap="6">
                        <Stack gap="1">
                            <Text fontWeight="semibold" textStyle="lg">
                                {t(
                                    'register.wizard.preferences.dataPrivacy.title'
                                )}
                            </Text>
                            <EmphasisText color="blue.400">
                                {t(
                                    'register.wizard.preferences.dataPrivacy.description'
                                )}
                            </EmphasisText>
                        </Stack>
                        <Stack gap="6">
                            <Controller
                                name="abbreviatedName"
                                control={control}
                                render={({ field }) => (
                                    <ToggleForm
                                        title={t(
                                            'register.wizard.preferences.dataPrivacy.abbreviatedName.title'
                                        )}
                                        description={t(
                                            'register.wizard.preferences.dataPrivacy.abbreviatedName.description'
                                        )}
                                        checked={watchedData.abbreviatedName}
                                        onCheckedChange={(checked: boolean) =>
                                            field.onChange(checked)
                                        }
                                    />
                                )}
                            />
                        </Stack>
                        <Flex>
                            <Field
                                label={t(
                                    'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.title'
                                )}
                                helperText={t(
                                    'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.help'
                                )}
                            >
                                <NativeSelectRoot size="sm">
                                    <NativeSelectField
                                        {...register(
                                            'preferedCommunicationMode'
                                        )}
                                        items={[
                                            {
                                                value: CommunicationMode.Email,
                                                label: t(
                                                    'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.email'
                                                ),
                                            },
                                            {
                                                value: CommunicationMode.Phone,
                                                label: t(
                                                    'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.phone'
                                                ),
                                            },
                                        ]}
                                    />
                                </NativeSelectRoot>
                            </Field>
                        </Flex>
                    </Stack>

                    <Flex justify={'center'}>
                        <PrevNextNav
                            prev={{
                                onClick: prev,
                                title: t('global.previous', {
                                    context: 'step',
                                }),
                            }}
                            next={{
                                type: 'submit',
                                title: t('global.next', {
                                    context: 'step',
                                }),
                            }}
                        />
                    </Flex>
                </Flex>
            </Stack>
        </form>
    )
}
