import React, { createContext, ReactNode, useContext, useState } from 'react'
import { CommunicationMode, ThemeMode } from '@_/graphql/api/generated'

interface StepWizardContextProps {
    currentStep: number
    totalSteps: number
    setTotalSteps: (step: number) => void
    next: () => void
    prev: () => void
}

const StepWizardContext = createContext<StepWizardContextProps | undefined>(
    undefined
)

interface StepWizardProviderProps {
    children: ReactNode
    currentStep: number
    setCurrentStep: (step: number) => void
    totalSteps?: number
}

export const StepWizardProvider = ({
    children,
    currentStep,
    totalSteps: totalStepsInitialValue = 0,
    setCurrentStep,
}: StepWizardProviderProps) => {
    const [totalSteps, setTotalSteps] = useState(totalStepsInitialValue)
    const next = () => {
        setCurrentStep(Math.min(currentStep + 1, totalSteps))
    }
    const prev = () => setCurrentStep(Math.max(currentStep - 1, 1))

    return (
        <StepWizardContext.Provider
            value={{ currentStep, totalSteps, setTotalSteps, next, prev }}
        >
            {children}
        </StepWizardContext.Provider>
    )
}

export const useStepWizard = (): StepWizardContextProps => {
    const context = useContext(StepWizardContext)
    if (!context) {
        throw new Error(
            'useStepWizard must be used within a StepWizardProvider'
        )
    }
    return context
}

export const defaultStore = {
    credentials: {
        email: '',
        password: '',
    },
    personalInformations: {
        avatar: undefined,
        firstname: '',
        lastname: '',
        countryCode: 33,
        nationalNumber: '',
        birthDate: undefined,
        bio: '',
    },
    location: {
        address: '',
        postalCode: '',
        city: '',
        country: '',
        latitude: '',
        longitude: '',
    },
    preferences: {
        themeMode: ThemeMode.System,
        abbreviatedName: false,
        preferedCommunicationMode: CommunicationMode.Email,
    },
}
