import { Card, Container, Flex } from '@chakra-ui/react'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { FieldValues } from 'react-hook-form'
import { useApolloClient } from '@apollo/client'
import { toast } from 'react-toastify'
import { generatePath, Link, useNavigate } from 'react-router'
import { useAuth } from '@_/auth/useAuth'
import { UserForm } from '@components/molecules/Form/AccountManagement/User/UserForm'
import { Loader } from '@components/atoms/Loader/Loader'
import { ChangePasswordForm } from '@components/molecules/Form/AccountManagement/User/ChangePasswordForm'
import { DeleteUserForm } from '@components/molecules/Form/AccountManagement/User/DeleteUserForm'
import {
    inUserSpace,
    WithUserProps,
} from '@components/templates/AccountManagement/inUserSpace'
import {
    useChangePasswordUserMutation,
    useDeleteUserMutation,
    UserProfile__UserFragment as User,
    useUpdateUserMutation,
    useUserProfileQuery,
} from '@_/graphql/api/generated'
import { FormSection } from '@components/ui/form-section'
import { useColorModeValue } from '@components/ui/color-mode'
import { UserProfileFormValues } from '@components/molecules/Form/AccountManagement/User/UserForm.types'
import { base64ToFile } from '@utils/image'
import { LocationForm } from '@components/molecules/Form/AccountManagement/User/LocationForm/LocationForm'
import { StaticPagesRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { Heading } from '@components/atoms/Heading'

const MyProfile = ({ user: { id } }: WithUserProps) => {
    const { t } = useTranslation()
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const client = useApolloClient()
    const navigate = useNavigate()
    const { data, error } = useUserProfileQuery({
        variables: {
            id,
        },
    })
    const auth = useAuth()
    const user = data?.user
    const [changePassword] = useChangePasswordUserMutation()
    const onChangePasswordSubmit = async (values: FieldValues) => {
        if (!user) {
            return
        }
        await changePassword({
            variables: {
                id,
                currentPassword: values.currentPass,
                newPassword: values.newPass,
            },
        }).then((r) => {
            if (r.data?.changePasswordUser?.user) {
                toast.success(t('global.ok'))
                return
            }
            toast.error(t('global.oups'))
        })
    }
    const [updateUser] = useUpdateUserMutation({
        update() {
            client.resetStore()
        },
    })
    const [deleteUser] = useDeleteUserMutation()
    const onUpdateSubmit = ({ avatar, ...values }: UserProfileFormValues) => {
        if (user) {
            updateUser({
                variables: {
                    input: {
                        id: user.id,
                        avatar: avatar ? base64ToFile(avatar) : null,
                        ...values,
                    },
                },
            }).then((r) => {
                if (r.data?.updateUser?.user) {
                    const returnedUser = r.data?.updateUser?.user
                    toast.success(
                        t('accountManagement.my.profile.update.toast_success')
                    )
                    auth.signin(returnedUser)
                    return
                }
                toast.error(
                    t('accountManagement.my.profile.updateStatus.toast_error')
                )
            })
        }
    }
    const onDelete = async (u: User) => {
        await deleteUser({
            variables: {
                id: u.id,
            },
        }).then((r) => {
            if (r.data?.deleteUser === null) {
                toast.error('Oups')
                return
            }
            toast.success(t('accountManagement.deleteAccount.toast_success'), {
                icon: '😿',
            })
            document.body.scrollTop = 0
            document.documentElement.scrollTop = 0
            auth.signout(() => {
                navigate(StaticPagesRoutes.Home, { replace: true })
            })
        })
    }

    return (
        (error && <p>Error </p>) ||
        (user && (
            <>
                <Helmet>
                    <title>
                        {t(`accountManagement.my.profile.title`)} {'< Communo'}
                    </title>
                </Helmet>
                <Container>
                    <Flex justifyContent="space-between" align={'end'} py="6">
                        <Heading size="3xl" as={'h1'}>
                            {t('accountManagement.my.profile.title')}
                        </Heading>
                        <Button colorPalette={'yellow'} asChild>
                            <Link
                                to={generatePath(UserRoutes.Profile, {
                                    slug: user.slug,
                                })}
                            >
                                {t(
                                    'accountManagement.my.profile.showPublicPage'
                                )}
                            </Link>
                        </Button>
                    </Flex>
                    <Card.Root
                        mb="20"
                        rounded="2xl"
                        p={'10'}
                        bgColor={cardBgColor}
                    >
                        <Card.Body gap={10}>
                            <FormSection
                                title={t(
                                    'accountManagement.my.profile.userInformations.title'
                                )}
                                description={t(
                                    'accountManagement.my.profile.userInformations.subTitle'
                                )}
                            >
                                <UserForm
                                    user={user}
                                    onSubmit={onUpdateSubmit}
                                />
                            </FormSection>
                            <FormSection
                                title={t(
                                    'accountManagement.my.profile.location.title'
                                )}
                                description={t(
                                    'accountManagement.my.profile.location.subTitle'
                                )}
                            >
                                <LocationForm user={user} />
                            </FormSection>
                            <FormSection
                                title={t(
                                    'accountManagement.my.profile.changePassword.title'
                                )}
                                description={t(
                                    'accountManagement.my.profile.changePassword.subTitle'
                                )}
                                emphasisColor={'pink.400'}
                            >
                                <ChangePasswordForm
                                    onSubmit={(values: FieldValues) =>
                                        onChangePasswordSubmit(values)
                                    }
                                />
                            </FormSection>
                            <FormSection
                                title={t(
                                    'accountManagement.my.profile.dangerZone.title'
                                )}
                                description={t(
                                    'accountManagement.my.profile.dangerZone.subTitle'
                                )}
                            >
                                <DeleteUserForm
                                    onSubmit={() => onDelete(user as User)}
                                />
                            </FormSection>
                        </Card.Body>
                    </Card.Root>
                </Container>
            </>
        )) || <Loader />
    )
}

export const MyProfileView = inUserSpace(MyProfile)
