import React from 'react'
import { Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import {
    Box,
    Card,
    Container,
    Flex,
    Link as ChakraLink,
    Stack,
    Text,
} from '@chakra-ui/react'
import { LoginForm } from '@components/molecules/Form/AccountManagement/Login/loginForm'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Heading } from '@components/atoms/Heading'
import { useColorModeValue } from '@components/ui/color-mode'

export const LoginView = () => {
    const { t } = useTranslation()

    return (
        <Flex height="full" flex="1">
            <Box
                flex="1"
                hideBelow="lg"
                bgImage={'url(/photos/pooling/5.jpg)'}
                bgPos={'center'}
                bgSize={'cover'}
            />
            <Box flex="1.5" py={{ base: '24', md: '32' }}>
                <Container maxW="md">
                    <Stack gap="8">
                        <Stack gap={5}>
                            <Stack
                                gap={{ base: '2', md: '3' }}
                                textAlign="center"
                            >
                                <Heading size={{ base: '2xl', md: '3xl' }}>
                                    {t('login.title')}
                                </Heading>
                                <Text color="fg.muted">
                                    {t('login.subtitle')}
                                </Text>
                            </Stack>
                            <Card.Root
                                rounded={'2xl'}
                                bg={useColorModeValue('white', 'gray.800')}
                            >
                                <Card.Body>
                                    <LoginForm />
                                </Card.Body>
                            </Card.Root>

                            <Flex
                                textStyle="sm"
                                color="fg.muted"
                                justifyContent="center"
                                gap={2}
                            >
                                {t('login.form.noAccount')}
                                <ChakraLink variant="underline" asChild>
                                    <Link to={UserRoutes.Register}>
                                        {t('login.form.register')}
                                    </Link>
                                </ChakraLink>
                            </Flex>
                        </Stack>
                    </Stack>
                </Container>
            </Box>
        </Flex>
    )
}
