import React, { useState } from 'react'
import { Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Box, Flex, Heading, Stack } from '@chakra-ui/react'
import { FaTicketAlt } from 'react-icons/fa'
import { toast } from 'react-toastify'
import {
    InvitationForm,
    InvitationFormData,
} from '@components/molecules/Form/AccountManagement/Beta/InvitationForm'
import { ThankYou } from '@components/molecules/ThankYou/ThankYou'
import { useRequestInvitationMutation } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { Button } from '@components/ui/button'

export const RequestInviteView = () => {
    const { t } = useTranslation()
    const [hasSent, setSent] = useState(false)
    const [requestInvitation] = useRequestInvitationMutation()
    const onSubmit = (data: InvitationFormData) => {
        requestInvitation({
            variables: {
                input: data,
            },
            onCompleted: () => {
                setSent(true)
            },
            onError: (error) => {
                if (error.message === 'email: alreadyRequested') {
                    toast.error(
                        t(
                            'accountManagement.invitationRequest.form.email.error.alreadyRequested'
                        )
                    )
                }
            },
        })
    }

    return (
        <Box as="section" py={{ lg: '12' }}>
            <Stack align="center">
                <Heading fontSize="4xl" textAlign="center" my={5}>
                    <Flex gap={5} alignItems="center">
                        <FaTicketAlt /> {t(`signin.requestInvite`)}
                    </Flex>
                </Heading>
            </Stack>
            <Box
                bg={useColorModeValue('white', 'gray.800')}
                rounded={{ lg: '2xl' }}
                maxW="5xl"
                mx="auto"
                px={{ base: '4', sm: '6', lg: '8' }}
                py={{ base: '12', sm: '16' }}
            >
                <Box className={'light'}>
                    <Box maxW="xl" mx="auto" textAlign="center">
                        {(!hasSent && (
                            <Stack gap={2}>
                                <InvitationForm {...{ onSubmit }} />
                                <Button w="full" asChild>
                                    <Link to="/login">
                                        {t('register.form.login')}
                                    </Link>
                                </Button>
                            </Stack>
                        )) || (
                            <ThankYou
                                title={t(
                                    'accountManagement.invitationRequest.thankYou.title'
                                )}
                                subtitle={t(
                                    'accountManagement.invitationRequest.thankYou.subtitle'
                                )}
                            />
                        )}
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}
