import { CommunicationMode, ThemeMode } from '@_/graphql/api/generated'

export type UserPreferencesFormValues = {
    settings: {
        id: string
        useAbbreviatedName?: boolean
        preferedCommunicationMode?: CommunicationMode
    }
}

export type UserAppearanceFormValues = {
    settings: {
        id: string
        themeMode?: ThemeMode
        language?: string
    }
    gender?: string
}
