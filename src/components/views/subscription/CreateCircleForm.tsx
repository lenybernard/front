import React, { useState } from 'react'
import { Flex, Input, Text, useBreakpointValue, VStack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Button } from '@components/ui/button'

interface CreateCircleFormProps {
    onCreate: (name: string) => void
    loading: boolean
}

export const CreateCircleForm: React.FC<CreateCircleFormProps> = ({
    onCreate,
    loading,
}) => {
    const { t } = useTranslation()
    const [circleName, setCircleName] = useState('')
    const isMobile = useBreakpointValue({ base: true, md: false })

    const handleSubmit = () => {
        if (circleName.trim() !== '') {
            onCreate(circleName)
            setCircleName('')
        }
    }

    return (
        <VStack gap={4} align="flex-start" width="100%" maxWidth="md">
            <Text>{t('subscription.subscribe.communityNameLabel')}</Text>
            <Flex
                direction={isMobile ? 'column' : 'row'}
                align={isMobile ? 'flex-start' : 'center'}
                gap={4}
                width="100%"
            >
                <Input
                    placeholder={t(
                        'subscription.subscribe.communityNamePlaceholder'
                    )}
                    value={circleName}
                    onChange={(e) => setCircleName(e.target.value)}
                    maxWidth="300px"
                />
                <Button
                    colorPalette="yellow"
                    onClick={handleSubmit}
                    loading={loading}
                >
                    {t('subscription.subscribe.create')}
                </Button>
            </Flex>
        </VStack>
    )
}
