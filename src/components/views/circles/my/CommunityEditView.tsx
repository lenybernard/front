import * as React from 'react'
import { Button } from '@components/ui/button'
import { generatePath, Link, useParams } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Loader } from '@components/atoms/Loader/Loader'
import { useEditFormCircleQuery } from '@_/graphql/api/generated'
import { withCommunityProvider } from '@components/templates/Circles/withCommunityProvider'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { inUserSpace } from '@components/templates/AccountManagement/inUserSpace'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { BreadcrumbLink, Card, Container, Flex } from '@chakra-ui/react'
import { H1 } from '@components/atoms/Heading'
import { FiHome } from 'react-icons/fi'
import {
    BreadcrumbCurrentLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'
import { useColorModeValue } from '@components/ui/color-mode'
import { FormSection } from '@components/ui/form-section'
import { InformationFormBlock } from '@components/molecules/Form/Circles/InformationForm/InformationFormBlock'
import { MediaFormBlock } from '@components/molecules/Form/Circles/MediaForm/MediaFormBlock'
import { DeleteCircleForm } from '@components/molecules/Form/Circles/DeleteCircleForm'
import { LocationFormBlock } from '@components/molecules/Form/Circles/LocationForm/LocationFormBlock'

export const CommunityEdit = () => {
    const { t } = useTranslation()
    const { slug } = useParams<{
        slug: string
    }>()
    const { community, setCommunity } = useCircle()

    const cardBgColor = useColorModeValue('gray.50', 'gray.800')

    if (!slug) {
        throw new Error('slug is required')
    }
    const { error, data } = useEditFormCircleQuery({
        variables: {
            id: generatePath(CommunitiesRoutes.IRI, { slug }),
        },
    })
    setCommunity(data?.circle)

    return (
        (error && <p>Error </p>) ||
        (community && (
            <>
                <Helmet>
                    <title>
                        {t('communities.show.meta.title', {
                            community,
                        })}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Container>
                    <BreadcrumbRoot py={2} color="yellow.600">
                        <BreadcrumbLink asChild>
                            <Link to="/">
                                <FiHome />
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbLink asChild>
                            <Link
                                to={generatePath(
                                    CommunitiesRoutes.MyCommunities
                                )}
                            >
                                {t('menu.my.communities')}
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbCurrentLink>
                            <Link
                                to={generatePath(CommunitiesRoutes.Show, {
                                    slug: community.slug,
                                })}
                            >
                                {t('menu.community.show', {
                                    community,
                                })}
                            </Link>
                        </BreadcrumbCurrentLink>
                        <BreadcrumbCurrentLink>
                            {t('menu.community.edit')}
                        </BreadcrumbCurrentLink>
                    </BreadcrumbRoot>
                    <Flex flexDir={'column'} gap="5">
                        <Flex justifyContent="space-between" align={'end'}>
                            <H1>{community.name}</H1>
                            <Button colorPalette={'yellow'} asChild>
                                <Link
                                    to={generatePath(CommunitiesRoutes.Show, {
                                        slug: community.slug,
                                    })}
                                >
                                    {t(
                                        'communities.my_communities.show.actions.see'
                                    )}
                                </Link>
                            </Button>
                        </Flex>
                        <Card.Root
                            mb="20"
                            rounded="2xl"
                            p={'10'}
                            bgColor={cardBgColor}
                        >
                            <Card.Body gap={10}>
                                <FormSection
                                    title={t(
                                        'communities.form.informations.title'
                                    )}
                                    description={t(
                                        'communities.form.informations.subTitle'
                                    )}
                                >
                                    <InformationFormBlock />
                                </FormSection>
                                <FormSection
                                    title={t('communities.form.media.title')}
                                    description={t(
                                        'communities.form.media.subTitle'
                                    )}
                                    emphasisColor={'pink.400'}
                                >
                                    <MediaFormBlock />
                                </FormSection>
                                <FormSection
                                    title={t('communities.form.location.title')}
                                    description={t(
                                        'communities.form.location.help'
                                    )}
                                    emphasisColor={'pink.400'}
                                >
                                    <LocationFormBlock />
                                </FormSection>
                                <FormSection
                                    title={t(
                                        'communities.form.dangerZone.title'
                                    )}
                                    description={t(
                                        'communities.form.dangerZone.subTitle'
                                    )}
                                >
                                    <DeleteCircleForm
                                        id={community.id}
                                        name={community.name}
                                    />
                                </FormSection>
                            </Card.Body>
                        </Card.Root>
                    </Flex>
                </Container>
            </>
        )) || <Loader />
    )
}

export const CommunityEditView = withCommunityProvider(
    // @ts-ignore
    inUserSpace(CommunityEdit)
)
