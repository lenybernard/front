import React, { useEffect, useState } from 'react'
import {
    Box,
    Card,
    Container,
    Heading,
    HStack,
    SimpleGrid,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import 'moment/locale/fr'
import { CircleMembershipsTable } from '@components/molecules/Table/Circles/CircleMembershipsTable'
import {
    CircleMembership,
    GetUserCirclesQueryVariables,
    useGetUserCirclesQuery,
} from '@_/graphql/api/generated'
import { usePaginatedQuery } from '@_/hooks/usePaginatedQuery'
import {
    inUserSpace,
    WithUserProps,
} from '@components/templates/AccountManagement/inUserSpace'
import { CommunityCard } from '@components/molecules/Cards/CommunityCard/CommunityCard'
import { useViewMode, ViewMode } from '@_/hooks/useViewMode'
import { FluidCard } from '@components/atoms/Animation/FluidCard'
import { ToggleViewTableAndCardMode } from '@components/atoms/Button/ToggleViewTableAndCardMode'
import { TabNavigationLayout } from '@components/layout/TabNavigationLayout'
import { TableSkeleton } from '@components/ui/skeleton'
import { Pager } from '@components/molecules/Pager/Pager'
import { NewCircleDialog } from '@components/molecules/Form/Circles/CircleForm/NewCircleDialog'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { Link, useSearchParams } from 'react-router'
import { FaMapMarkerAlt } from 'react-icons/fa'
import { InfoButton } from '@components/atoms/Button'
import { CtaVertical } from '@components/molecules/CTA/CtaVertical'
import { useAuth } from '@_/auth/useAuth'
import { useColorModeValue } from '@components/ui/color-mode'
import { Loader } from '@components/atoms/Loader/Loader'
import { JoinCommunityInvitationDialog } from '../JoinCommunityInvitationDialog'

const MyCircles = ({ user }: WithUserProps) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const [afterCursor, setAfterCursor] = useState<string | undefined>()
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>()
    const [viewMode, setViewMode] = useViewMode<ViewMode.Table | ViewMode.Card>(
        ViewMode.Card
    )
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const nbToFetch = 12
    const [variables, setVariables] = useState<GetUserCirclesQueryVariables>({
        afterCursor,
        beforeCursor,
        nbToFetch,
        statusList: ['ok', 'pending', 'invited'],
        id: `/users/${user.slug}`,
    })

    useEffect(() => {
        setVariables({
            afterCursor,
            beforeCursor,
            nbToFetch,
            statusList: ['ok', 'pending', 'invited'],
            id: `/users/${user.slug}`,
        })
    }, [user, afterCursor, beforeCursor, nbToFetch])
    const [searchParams] = useSearchParams()
    const invitationId = searchParams.get('invitation') ?? undefined

    const {
        loading,
        nodes: circleMemberships,
        pageInfo,
        totalCount,
    } = usePaginatedQuery<CircleMembership, GetUserCirclesQueryVariables>(
        // @ts-ignore
        useGetUserCirclesQuery,
        { path: ['user', 'circleMemberships'] },
        variables
    )

    return (
        <>
            <Helmet>
                <title>
                    {t('communities.my_communities.index.title')}{' '}
                    {t('meta.title.suffix')}
                </title>
            </Helmet>

            <Container>
                <Heading size="3xl" py="6">
                    {t('communities.my_communities.index.title')}
                </Heading>
                {(loading && <Loader />) ||
                    (totalCount > 0 && (
                        <Card.Root mb="20" rounded="2xl" bgColor={cardBgColor}>
                            <Card.Body gap={10}>
                                <TabNavigationLayout
                                    {...{
                                        tabs: [],
                                        actions: [
                                            {
                                                key: 'toggleViewMode',
                                                component: (
                                                    <ToggleViewTableAndCardMode
                                                        viewMode={viewMode}
                                                        onClick={setViewMode}
                                                    />
                                                ),
                                            },
                                            {
                                                key: 'newCircleButton',
                                                component: <NewCircleDialog />,
                                            },
                                            {
                                                key: 'circleMapButton',
                                                component: (
                                                    <Button
                                                        variant="solid"
                                                        colorPalette={'gray'}
                                                        asChild
                                                    >
                                                        <Link
                                                            to={
                                                                CommunitiesRoutes.Map
                                                            }
                                                        >
                                                            <FaMapMarkerAlt />
                                                            {t(
                                                                'communities.my_communities.index.actions.find'
                                                            )}
                                                        </Link>
                                                    </Button>
                                                ),
                                            },
                                        ],
                                    }}
                                >
                                    {(loading && (
                                        <Box mt={15}>
                                            <TableSkeleton
                                                columns={3}
                                                rows={5}
                                            />
                                        </Box>
                                    )) || (
                                        <Box>
                                            {viewMode === ViewMode.Table ? (
                                                <CircleMembershipsTable
                                                    circleMemberships={
                                                        circleMemberships
                                                    }
                                                    variables={variables}
                                                />
                                            ) : (
                                                <SimpleGrid
                                                    columns={{
                                                        base: 1,
                                                        md: 3,
                                                        lg: 4,
                                                    }}
                                                    gap={10}
                                                    pt={10}
                                                >
                                                    {circleMemberships.map(
                                                        (node) => (
                                                            <FluidCard
                                                                key={node.id}
                                                            >
                                                                <CommunityCard
                                                                    circle={
                                                                        node.circle
                                                                    }
                                                                />
                                                            </FluidCard>
                                                        )
                                                    )}
                                                </SimpleGrid>
                                            )}
                                            {pageInfo && totalCount > 0 && (
                                                <Pager
                                                    {...{
                                                        pageInfo,
                                                        loading,
                                                        setBeforeCursor,
                                                        setAfterCursor,
                                                    }}
                                                />
                                            )}
                                        </Box>
                                    )}
                                </TabNavigationLayout>
                            </Card.Body>
                        </Card.Root>
                    )) || (
                        <CtaVertical
                            tagline={t(
                                'communities.my_communities.index.empty.tagLine'
                            )}
                            title={t(
                                'communities.my_communities.index.empty.title',
                                {
                                    context: auth.user?.gender,
                                }
                            )}
                            description={t(
                                'communities.my_communities.index.empty.description'
                            )}
                            buttons={[
                                {
                                    key: 'btn-map',
                                    component: (
                                        <Link to={CommunitiesRoutes.Map}>
                                            <InfoButton asChild>
                                                <HStack>
                                                    <FaMapMarkerAlt />
                                                    {t(
                                                        'communities.my_communities.index.empty.button'
                                                    )}
                                                </HStack>
                                            </InfoButton>
                                        </Link>
                                    ),
                                },
                                {
                                    key: 'btn-new',
                                    component: (
                                        <NewCircleDialog
                                            title={t(
                                                'communities.my_communities.index.actions.new'
                                            )}
                                        />
                                    ),
                                },
                            ]}
                        />
                    )}
            </Container>
            {invitationId && (
                <JoinCommunityInvitationDialog
                    invitationId={invitationId}
                    variables={variables}
                />
            )}
        </>
    )
}

export const MyCommunitiesView = inUserSpace(MyCircles)
