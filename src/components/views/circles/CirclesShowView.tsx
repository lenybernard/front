import * as React from 'react'
import { useState } from 'react'
import {
    Badge,
    Box,
    ButtonGroup,
    Center,
    Container,
    Flex,
    Heading,
    Link as ChakraLink,
    Separator,
    Text,
} from '@chakra-ui/react'
import { generatePath, Link, useNavigate, useParams } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { FaChevronDown, FaEnvelope, FaExternalLinkAlt } from 'react-icons/fa'
import { Loader } from '@components/atoms/Loader/Loader'
import { CircleLogo } from '@components/molecules/User/Circle/CircleLogo'
import { useAuth } from '@_/auth/useAuth'
import { JoinCircleButton } from '@components/molecules/Circles/JoinCircleButton/JoinCircleButton'
import { MembershipList } from '@components/molecules/Circles/members/MembershipList'
import { useCircleShowView_CircleQuery } from '@_/graphql/api/generated'
import { CommunitiesRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { toast } from 'react-toastify'
import {
    MenuContent,
    MenuItem,
    MenuRoot,
    MenuTrigger,
} from '@components/ui/menu'
import { InvitationFormDialog } from '@components/molecules/Form/AccountManagement/User/InvitationForm/InvitationFormDialog'

export const CirclesShowView = () => {
    const { t } = useTranslation()
    const params = useParams()
    if (!params.slug) {
        throw new Error('circle slug is required')
    }

    const [inviteModalIsOpen, setInviteModalOpen] = useState(false)
    const { error, data } = useCircleShowView_CircleQuery({
        variables: {
            id: generatePath(CommunitiesRoutes.IRI, { slug: params.slug }),
        },
    })
    const auth = useAuth()
    const navigate = useNavigate()
    const circle = data?.circle
    const currentUserMembership = circle?.memberships?.edges?.find(
        (edge) => edge.node!.user.id === auth.user?.id
    )

    return (
        (circle && (
            <>
                <Helmet>
                    <title>
                        {t('communities.show.meta.title', {
                            community: circle,
                        })}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <Box
                    position="relative"
                    bg={`url("${(circle.cover && circle.cover.contentUrl) || '../photos/community-blured.webp'}")`}
                    bgPos="center"
                    bgSize="cover"
                    _after={{
                        content: '""',
                        display: 'block',
                        w: 'full',
                        h: 'full',
                        bg: 'blackAlpha.700',
                        position: 'absolute',
                        inset: 0,
                        zIndex: 0,
                    }}
                >
                    <Box
                        mx="auto"
                        p={{ base: 6, md: 8 }}
                        h="full"
                        zIndex={1}
                        position="relative"
                        backdropFilter="auto"
                        backdropBlur="4px"
                    >
                        <Container>
                            <Center
                                flexDirection="column"
                                textAlign="center"
                                color="white"
                                h="full"
                                my="40"
                                gap="5"
                            >
                                <CircleLogo circle={circle} size="xl" mb={10} />
                                {circle.subscription?.plan && (
                                    <Badge
                                        colorPalette={'yellow'}
                                        fontSize={'larger'}
                                    >
                                        {circle.subscription.plan.name}
                                    </Badge>
                                )}
                                <Heading
                                    as="h1"
                                    size="2xl"
                                    fontWeight="extrabold"
                                >
                                    {circle.name}{' '}
                                    <small>
                                        {circle.explicitLocationLabel ??
                                            circle.city}
                                    </small>
                                </Heading>
                                <Text fontSize="lg" fontWeight="medium" mt="3">
                                    {circle.description}
                                </Text>
                                {circle.website && (
                                    <ChakraLink href={circle.website}>
                                        <FaExternalLinkAlt />
                                        {circle.website}
                                    </ChakraLink>
                                )}
                                <Box my={10}>
                                    {(circle.activeUserActions.length > 0 &&
                                        circle.activeUserActions.map(
                                            (action: string) =>
                                                (action === 'edit' &&
                                                    circle && (
                                                        <ButtonGroup
                                                            size="sm"
                                                            attached
                                                            key={`edit-button-${circle.id}`}
                                                        >
                                                            <Button asChild>
                                                                <Link
                                                                    to={generatePath(
                                                                        CommunitiesRoutes.Edit,
                                                                        {
                                                                            slug: circle.slug,
                                                                        }
                                                                    )}
                                                                    title={t(
                                                                        'communities.my_communities.index.table.manage'
                                                                    )}
                                                                >
                                                                    {t(
                                                                        'communities.my_communities.index.table.manage'
                                                                    )}
                                                                </Link>
                                                            </Button>
                                                            <MenuRoot
                                                                positioning={{
                                                                    placement:
                                                                        'bottom-end',
                                                                }}
                                                            >
                                                                <MenuTrigger
                                                                    asChild
                                                                >
                                                                    <Button roundedLeft="none">
                                                                        <FaChevronDown />
                                                                    </Button>
                                                                </MenuTrigger>
                                                                <MenuContent>
                                                                    <MenuItem
                                                                        value="cut"
                                                                        valueText="cut"
                                                                    >
                                                                        <Button
                                                                            variant="plain"
                                                                            onClick={() =>
                                                                                setInviteModalOpen(
                                                                                    true
                                                                                )
                                                                            }
                                                                            asChild
                                                                        >
                                                                            <Flex
                                                                                gap={
                                                                                    2
                                                                                }
                                                                                alignItems="center"
                                                                            >
                                                                                <FaEnvelope />
                                                                                {t(
                                                                                    'communities.my_communities.show.actions.inviteUser'
                                                                                )}
                                                                            </Flex>
                                                                        </Button>
                                                                    </MenuItem>
                                                                </MenuContent>
                                                            </MenuRoot>
                                                        </ButtonGroup>
                                                    )) ||
                                                (action === 'quit' &&
                                                    currentUserMembership &&
                                                    currentUserMembership.node
                                                        ?.status ===
                                                        'pending' && (
                                                        <JoinCircleButton
                                                            key={`already-join-button-${circle.id}`}
                                                            circle={circle}
                                                            disabled={true}
                                                            title={
                                                                (currentUserMembership
                                                                    ?.node
                                                                    ?.status &&
                                                                    t(
                                                                        `communities.my_communities.index.table.status.${currentUserMembership.node?.status}_tooltip`
                                                                    )) ||
                                                                undefined
                                                            }
                                                        />
                                                    ))
                                        )) ||
                                        (!auth.user && (
                                            <Button
                                                onClick={() => {
                                                    toast.info(
                                                        t('global.please.login')
                                                    )
                                                    navigate(UserRoutes.Login, {
                                                        state: {
                                                            from: generatePath(
                                                                CommunitiesRoutes.Show,
                                                                {
                                                                    slug: circle.slug,
                                                                }
                                                            ),
                                                        },
                                                    })
                                                }}
                                            >
                                                {t(
                                                    'communities.show.actions.askToJoin.button'
                                                )}
                                            </Button>
                                        )) || (
                                            <JoinCircleButton circle={circle} />
                                        )}
                                </Box>
                            </Center>
                        </Container>
                    </Box>
                </Box>
                {params.slug && (
                    <Container>
                        <Box p={{ base: 6, md: 8 }}>
                            <MembershipList
                                title="communities.show.users.ok.title"
                                circleSlug={params.slug}
                                statuses={['ok']}
                                order={[
                                    {
                                        permission: 'ASC',
                                        user_firstname: 'ASC',
                                        user_lastname: 'ASC',
                                    },
                                ]}
                            />
                        </Box>

                        {circle.activeUserActions.includes('acceptUser') && (
                            <>
                                <Box p={{ base: 6, md: 8 }}>
                                    <MembershipList
                                        title="communities.show.users.waiting.title"
                                        circleSlug={params.slug}
                                        statuses={['pending', 'invited']}
                                        order={[
                                            {
                                                createdAt: 'ASC',
                                            },
                                        ]}
                                    />
                                </Box>
                                <Separator />
                            </>
                        )}
                    </Container>
                )}
                {inviteModalIsOpen && (
                    <InvitationFormDialog
                        {...{
                            inviteModalIsOpen,
                            setInviteModalOpen,
                            community: circle,
                        }}
                    />
                )}
            </>
        )) ||
        (error && <p>Error </p>) || <Loader />
    )
}
