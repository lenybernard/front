import { Container, Heading, Stack, Text, VStack } from '@chakra-ui/react'
import { ProfilePoolingHeroFragment } from '@_/graphql/api/generated'
import { useTranslation } from 'react-i18next'

export const ProfilePoolingHero = ({
    user,
}: {
    user: ProfilePoolingHeroFragment
}) => {
    const { t } = useTranslation()

    return (
        <Container py={{ base: '16', md: '24' }}>
            <VStack gap="10" textAlign="center">
                <Stack gap="4">
                    <Heading
                        as="h1"
                        textStyle={{ base: '4xl', md: '6xl' }}
                        maxW={{ md: '4xl' }}
                        mx="auto"
                        lineHeight="tighter"
                    >
                        {user.firstname} partage son{' '}
                        <Text color="yellow.400" asChild>
                            <em>super</em>
                        </Text>{' '}
                        matériel !
                    </Heading>
                    <Text
                        color="fg.muted"
                        textStyle={{ base: 'lg', md: 'xl' }}
                        maxW={{ md: '4xl' }}
                        mx="auto"
                    >
                        {t('accountManagement.profile.pooling.description', {
                            context: user.gender,
                        })}
                    </Text>
                </Stack>
            </VStack>
        </Container>
    )
}
