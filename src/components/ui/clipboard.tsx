import type { ButtonProps, InputProps } from '@chakra-ui/react'
import {
    Button,
    Clipboard as ChakraClipboard,
    IconButton,
    Input,
} from '@chakra-ui/react'
import * as React from 'react'
import { LuCheck, LuClipboard, LuLink } from 'react-icons/lu'

import { useTranslation } from 'react-i18next'

const ClipboardIcon = React.forwardRef<
    HTMLDivElement,
    ChakraClipboard.IndicatorProps
>(function ClipboardIcon(props, ref) {
    return (
        <ChakraClipboard.Indicator copied={<LuCheck />} {...props} ref={ref}>
            <LuClipboard />
        </ChakraClipboard.Indicator>
    )
})

const ClipboardCopyText = React.forwardRef<
    HTMLDivElement,
    ChakraClipboard.IndicatorProps & { label?: string }
>(function ClipboardCopyText({ label, ...props }, ref) {
    const { t } = useTranslation()
    return (
        <ChakraClipboard.Indicator
            copied={t('global.clipboard.copied')}
            {...props}
            ref={ref}
        >
            {label || t('global.clipboard.copy')}
        </ChakraClipboard.Indicator>
    )
})

export const ClipboardLabel = React.forwardRef<
    HTMLLabelElement,
    ChakraClipboard.LabelProps
>(function ClipboardLabel(props, ref) {
    return (
        <ChakraClipboard.Label
            textStyle="sm"
            fontWeight="medium"
            display="inline-block"
            mb="1"
            {...props}
            ref={ref}
        />
    )
})

export const ClipboardButton = React.forwardRef<
    HTMLButtonElement,
    ButtonProps & { label?: string }
>(function ClipboardButton({ label, ...props }, ref) {
    return (
        <ChakraClipboard.Trigger asChild>
            <Button ref={ref} size="sm" variant="surface" {...props}>
                <ClipboardIcon />
                <ClipboardCopyText label={label} />
            </Button>
        </ChakraClipboard.Trigger>
    )
})

export const ClipboardLink = React.forwardRef<HTMLButtonElement, ButtonProps>(
    function ClipboardLink(props, ref) {
        return (
            <ChakraClipboard.Trigger asChild>
                <Button
                    unstyled
                    variant="plain"
                    size="xs"
                    display="inline-flex"
                    alignItems="center"
                    gap="2"
                    ref={ref}
                    {...props}
                >
                    <LuLink />
                    <ClipboardCopyText />
                </Button>
            </ChakraClipboard.Trigger>
        )
    }
)

export const ClipboardIconButton = React.forwardRef<
    HTMLButtonElement,
    ButtonProps & { label?: string }
>(function ClipboardIconButton({ label, ...props }, ref) {
    return (
        <ChakraClipboard.Trigger asChild>
            <IconButton ref={ref} size="xs" variant="subtle" {...props}>
                <ClipboardIcon />
                <ClipboardCopyText label={label} />
            </IconButton>
        </ChakraClipboard.Trigger>
    )
})

export const ClipboardInput = React.forwardRef<HTMLInputElement, InputProps>(
    function ClipboardInputElement(props, ref) {
        return (
            <ChakraClipboard.Input asChild>
                <Input ref={ref} {...props} />
            </ChakraClipboard.Input>
        )
    }
)

export const ClipboardRoot = ChakraClipboard.Root
