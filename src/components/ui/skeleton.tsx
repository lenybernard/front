import type {
    CircleProps,
    SkeletonProps as ChakraSkeletonProps,
} from '@chakra-ui/react'
import {
    Box,
    Circle,
    Flex,
    Skeleton as ChakraSkeleton,
    Stack,
} from '@chakra-ui/react'
import * as React from 'react'

export interface SkeletonCircleProps extends ChakraSkeletonProps {
    size?: CircleProps['size']
}

export const Skeleton = ChakraSkeleton
export const SkeletonCircle = React.forwardRef<
    HTMLDivElement,
    SkeletonCircleProps
>(function SkeletonCircle(props, ref) {
    const { size, ...rest } = props
    return (
        <Circle size={size} asChild ref={ref}>
            <ChakraSkeleton {...rest} />
        </Circle>
    )
})

export interface SkeletonTextProps extends ChakraSkeletonProps {
    noOfLines?: number
}

export const SkeletonText = React.forwardRef<HTMLDivElement, SkeletonTextProps>(
    function SkeletonText(props, ref) {
        const { noOfLines = 3, gap, ...rest } = props
        return (
            <Stack gap={gap} width="full" ref={ref}>
                {Array.from({ length: noOfLines }).map((_, index) => (
                    <ChakraSkeleton
                        height="4"
                        key={index}
                        {...props}
                        _last={{ maxW: '80%' }}
                        {...rest}
                    />
                ))}
            </Stack>
        )
    }
)

export const HorizontalCardSkeleton = () => (
    <Box p={4} borderWidth="1px" borderRadius="lg">
        <Flex gap={4} alignItems="center">
            <Skeleton borderRadius="full" boxSize="50px" />
            <Box flex="1">
                <Skeleton height="20px" width="40%" mb={2} />
                <Skeleton height="16px" width="60%" />
            </Box>
            <Skeleton height="32px" width="80px" borderRadius="md" />
        </Flex>
    </Box>
)

export const VerticalCardSkeleton = () => (
    <Box p={4} borderWidth="1px" borderRadius="lg">
        <Flex gap={4} direction="column" alignItems="center">
            <Skeleton borderRadius="full" boxSize="60px" />
            <Skeleton height="20px" width="40%" />
            <Skeleton height="20px" width="67%" />
            <Skeleton height="16px" width="55%" />
            <Skeleton height="32px" width="80px" borderRadius="md" />
        </Flex>
    </Box>
)

type TableSkeletonProps = {
    columns: number
    rows: number
}

export const TableSkeleton = ({
    columns = 5,
    rows = 10,
}: TableSkeletonProps) => {
    return (
        <Box asChild width="full">
            <table>
                <thead>
                    <tr>
                        {Array.from({ length: columns }).map((_, index) => (
                            <th
                                key={`header-${index}`}
                                style={{ padding: '0.5rem' }}
                            >
                                <ChakraSkeleton h={6} />
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {Array.from({ length: rows }).map((_, rowIndex) => (
                        <tr key={`row-${rowIndex}`}>
                            {Array.from({ length: columns }).map(
                                (_2, colIndex) => (
                                    <td
                                        key={`cell-${rowIndex}-${colIndex}`}
                                        style={{
                                            padding: '0.5rem',
                                        }}
                                    >
                                        <ChakraSkeleton h={6} w={'95%'} />
                                    </td>
                                )
                            )}
                        </tr>
                    ))}
                </tbody>
            </table>
        </Box>
    )
}
