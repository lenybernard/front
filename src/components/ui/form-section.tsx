import { Fieldset, Stack } from '@chakra-ui/react'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { ReactNode } from 'react'

interface FormSectionProps {
    title: string
    description: ReactNode
    children: ReactNode
    emphasisColor?: string
}

export const FormSection = (props: FormSectionProps) => {
    return (
        <Fieldset.Root size="lg" flexDir={{ base: 'column', md: 'row' }}>
            <Stack flex="1" maxW="2xl">
                <Fieldset.Legend textStyle="xl">
                    <EmphasisText color={props.emphasisColor ?? 'yellow.400'}>
                        {props.title}
                    </EmphasisText>
                </Fieldset.Legend>
                <Fieldset.HelperText>{props.description}</Fieldset.HelperText>
            </Stack>
            <Fieldset.Content flex="1" alignItems="flex-start" mt={{ md: '0' }}>
                {props.children}
            </Fieldset.Content>
        </Fieldset.Root>
    )
}
