'use client'

import type { ButtonProps, RecipeProps } from '@chakra-ui/react'
import {
    Button,
    FileUpload as ChakraFileUpload,
    Float,
    Icon,
    IconButton,
    Span,
    Text,
    useFileUploadContext,
    useRecipe,
} from '@chakra-ui/react'
import * as React from 'react'
import { LuFile, LuUpload } from 'react-icons/lu'
import { Avatar } from '@components/ui/avatar'
import { FaTrashAlt } from 'react-icons/fa'

export interface FileUploadRootProps extends ChakraFileUpload.RootProps {
    inputProps?: React.InputHTMLAttributes<HTMLInputElement>
}

export const FileUploadRoot = React.forwardRef<
    HTMLInputElement,
    FileUploadRootProps
>(function FileUploadRoot(props, ref) {
    const { children, inputProps, ...rest } = props
    return (
        <ChakraFileUpload.Root {...rest}>
            <ChakraFileUpload.HiddenInput ref={ref} {...inputProps} />
            {children}
        </ChakraFileUpload.Root>
    )
})

export interface FileUploadDropzoneProps
    extends ChakraFileUpload.DropzoneProps {
    label: React.ReactNode
    description?: React.ReactNode
}

export const FileUploadDropzone = React.forwardRef<
    HTMLInputElement,
    FileUploadDropzoneProps
>(function FileUploadDropzone(props, ref) {
    const { children, label, description, ...rest } = props
    return (
        <ChakraFileUpload.Dropzone ref={ref} {...rest}>
            <Icon fontSize="xl" color="fg.muted">
                <LuUpload />
            </Icon>
            <ChakraFileUpload.DropzoneContent>
                <div>{label}</div>
                {description && <Text color="fg.muted">{description}</Text>}
            </ChakraFileUpload.DropzoneContent>
            {children}
        </ChakraFileUpload.Dropzone>
    )
})

interface VisibilityProps {
    showSize?: boolean
    clearable?: boolean
}

interface FileUploadItemProps extends VisibilityProps {
    file: File
}

const FileUploadItem = React.forwardRef<HTMLLIElement, FileUploadItemProps>(
    function FileUploadItem(props, ref) {
        const { file, showSize, clearable } = props
        const [imagePreview, setImagePreview] = React.useState<string | null>(
            null
        )

        React.useEffect(() => {
            if (file.type.startsWith('image/')) {
                const reader = new FileReader()
                reader.onloadend = () => {
                    setImagePreview(reader.result as string)
                }
                reader.readAsDataURL(file)
            }
        }, [file])

        return (
            <ChakraFileUpload.Item
                file={file}
                ref={ref}
                bg={'none'}
                border={'none'}
            >
                <ChakraFileUpload.ItemPreview asChild>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {imagePreview ? (
                            <Avatar
                                src={imagePreview}
                                size="2xl"
                                shape="rounded"
                                alt={file.name}
                            />
                        ) : (
                            <Icon fontSize="lg" color="fg.muted">
                                <LuFile />
                            </Icon>
                        )}
                        {clearable && file.type.startsWith('image/') && (
                            <ChakraFileUpload.ItemDeleteTrigger asChild>
                                <IconButton
                                    marginTop="-20px"
                                    marginLeft="-20px"
                                    boxShadow="2xl"
                                    size="sm"
                                    rounded="full"
                                    colorPalette="red"
                                    color="white"
                                    aria-label="remove image"
                                    onClick={(e: React.MouseEvent) => {
                                        // onDelete()
                                        e.stopPropagation()
                                    }}
                                >
                                    <FaTrashAlt />
                                </IconButton>
                            </ChakraFileUpload.ItemDeleteTrigger>
                        )}
                    </div>
                </ChakraFileUpload.ItemPreview>

                {showSize && (
                    <ChakraFileUpload.ItemContent>
                        <ChakraFileUpload.ItemSizeText />
                    </ChakraFileUpload.ItemContent>
                )}

                {clearable && !file.type.startsWith('image/') && (
                    <ChakraFileUpload.ItemDeleteTrigger asChild>
                        <Float placement="top-end">
                            <IconButton
                                size="sm"
                                rounded="full"
                                colorPalette="red"
                                color="white"
                                aria-label="remove image"
                                onClick={(e: React.MouseEvent) => {
                                    // onDelete()
                                    e.stopPropagation()
                                }}
                            >
                                <FaTrashAlt />
                            </IconButton>
                        </Float>
                    </ChakraFileUpload.ItemDeleteTrigger>
                )}
            </ChakraFileUpload.Item>
        )
    }
)

interface FileUploadListProps
    extends VisibilityProps,
        ChakraFileUpload.ItemGroupProps {
    files?: File[]
}

export const FileUploadList = React.forwardRef<
    HTMLUListElement,
    FileUploadListProps
>(function FileUploadList(props, ref) {
    const { showSize, clearable, files, ...rest } = props

    const fileUpload = useFileUploadContext()
    const acceptedFiles = files ?? fileUpload.acceptedFiles

    if (acceptedFiles.length === 0) return null

    return (
        <ChakraFileUpload.ItemGroup ref={ref} {...rest}>
            {acceptedFiles.map((file: File) => (
                <FileUploadItem
                    key={file.name}
                    file={file}
                    showSize={showSize}
                    clearable={clearable}
                />
            ))}
        </ChakraFileUpload.ItemGroup>
    )
})

type Assign<T, U> = Omit<T, keyof U> & U

interface FileInputProps extends Assign<ButtonProps, RecipeProps<'input'>> {
    placeholder?: React.ReactNode
}

export const FileInput = React.forwardRef<HTMLButtonElement, FileInputProps>(
    function FileInput(props, ref) {
        const inputRecipe = useRecipe({ key: 'input' })
        const [recipeProps, restProps] = inputRecipe.splitVariantProps(props)
        const { placeholder = 'Select file(s)', ...rest } = restProps
        return (
            <ChakraFileUpload.Trigger asChild>
                <Button
                    unstyled
                    py="0"
                    ref={ref}
                    {...rest}
                    css={[inputRecipe(recipeProps), props.css]}
                >
                    <ChakraFileUpload.Context>
                        {({ acceptedFiles }: { acceptedFiles: File[] }) => {
                            if (acceptedFiles.length === 1) {
                                return <span>{acceptedFiles[0].name}</span>
                            }
                            if (acceptedFiles.length > 1) {
                                return <span>{acceptedFiles.length} files</span>
                            }
                            return <Span color="fg.subtle">{placeholder}</Span>
                        }}
                    </ChakraFileUpload.Context>
                </Button>
            </ChakraFileUpload.Trigger>
        )
    }
)

export const FileUploadLabel = ChakraFileUpload.Label
export const FileUploadClearTrigger = ChakraFileUpload.ClearTrigger
export const FileUploadTrigger = ChakraFileUpload.Trigger
