'use client'

import {
    DrawerContext,
    Flex,
    IconButton,
    Stack,
    StackSeparator,
} from '@chakra-ui/react'
import { type PropsWithChildren, useState } from 'react'
import { LuAlignRight, LuX } from 'react-icons/lu'
import {
    DrawerActionTrigger,
    DrawerCloseTrigger,
    DrawerContent,
    DrawerRoot,
    DrawerTrigger,
} from '@components/ui/drawer'
import { useColorModeValue } from '@components/ui/color-mode'
import { SearchBar } from '@components/molecules/Menu/SearchBar'
import { Button } from '@components/ui/button'
import { useAuth } from '@_/auth/useAuth'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { RainbowButton } from '@components/atoms/Button'
import { LoginForm } from '@components/molecules/Form/AccountManagement/Login/loginForm'

enum MinW {
    Collapsed = '300px',
    Expanded = '500px',
}
export const DesktopNavDrawer = (props: PropsWithChildren) => {
    const [minW, setMinW] = useState(MinW.Collapsed)
    const { t } = useTranslation()
    const navigate = useNavigate()
    const auth = useAuth()
    return (
        <DrawerRoot placement="start" autoFocus={false}>
            <DrawerContext>
                {(context: any) => (
                    <DrawerTrigger asChild>
                        <IconButton
                            aria-label="Open Menu"
                            variant="ghost"
                            size="sm"
                            colorPalette="gray"
                            hideBelow="md"
                        >
                            {context.open ? <LuX /> : <LuAlignRight />}
                        </IconButton>
                    </DrawerTrigger>
                )}
            </DrawerContext>
            <DrawerContent
                bgColor={useColorModeValue('gray.100', 'gray.800')}
                textStyle="md"
                boxShadow="none"
                borderRadius="none"
                maxW="unset"
                minW={minW}
                px="4"
                py="6"
                width="var(--available-width)"
                height="var(--available-height)"
            >
                <DrawerActionTrigger>{props.children}</DrawerActionTrigger>
                <DrawerContext>
                    {(store: any) => {
                        return (
                            <>
                                <Stack
                                    as="nav"
                                    mt={10}
                                    separator={<StackSeparator />}
                                    gap={5}
                                >
                                    <SearchBar
                                        onFocus={() => setMinW(MinW.Expanded)}
                                        onBlur={() => setMinW(MinW.Collapsed)}
                                    />
                                    {(auth.user && (
                                        <Button
                                            onClick={() => {
                                                navigate(UserRoutes.MyProfile)
                                                store.setOpen(false)
                                            }}
                                        >
                                            {t('menu.profile')}
                                        </Button>
                                    )) || (
                                        <Flex direction="column" gap={5}>
                                            <LoginForm />
                                            <RainbowButton
                                                onClick={() => {
                                                    navigate(
                                                        UserRoutes.Register
                                                    )
                                                    store.setOpen(false)
                                                }}
                                            >
                                                {t('menu.register')}
                                            </RainbowButton>
                                        </Flex>
                                    )}
                                </Stack>
                            </>
                        )
                    }}
                </DrawerContext>
                <DrawerCloseTrigger />
            </DrawerContent>
        </DrawerRoot>
    )
}
