import { FC, useState } from 'react'
import { useForm } from 'react-hook-form'
import { Flex, IconButton, Input } from '@chakra-ui/react'
import { useSearch } from '@_/contexts/Circles/SearchContext'
import { useTranslation } from 'react-i18next'
import { motion } from 'framer-motion'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { useNavigate } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { InputGroup } from '@components/ui/input-group'
import { FaSearch, FaTimes } from 'react-icons/fa'

type SearchBarProps = {
    onFocus?: () => void
    onBlur?: () => void
}

export type SearchInputFormValues = {
    searchTerms: string
}

export const SearchBar: FC<SearchBarProps> = ({ onFocus, onBlur }) => {
    const {
        tempSearchTerms,
        setTempSearchTerms,
        handleSubmit: onSubmit,
    } = useSearch()
    const { t } = useTranslation()

    const { register, handleSubmit } = useForm<SearchInputFormValues>({
        defaultValues: {
            searchTerms: tempSearchTerms,
        },
    })
    const navigate = useNavigate()

    const [isFocused, setIsFocused] = useState(false)

    const handleSearch = (data: SearchInputFormValues) => {
        setTempSearchTerms(data.searchTerms)
        onSubmit()
    }

    const handleReset = () => {
        setTempSearchTerms('')
        navigate(PoolingRoutes.Search)
    }

    return (
        <Flex width="100%" px={3}>
            <motion.div
                initial={{ width: '60px' }}
                animate={{ width: isFocused ? '100%' : '300px' }}
                transition={{ duration: 0.5, delay: 0.2, ease: 'backInOut' }}
                style={{ overflow: 'hidden' }}
            >
                <form onSubmit={handleSubmit(handleSearch)}>
                    <InputGroup
                        w="99%"
                        endElement={
                            <Flex height="100%" alignItems="center">
                                {tempSearchTerms && (
                                    <IconButton
                                        aria-label="Reset"
                                        variant="ghost"
                                        size="sm"
                                        onClick={handleReset}
                                    >
                                        <FaTimes />
                                    </IconButton>
                                )}
                                <IconButton
                                    aria-label="Search"
                                    type="submit"
                                    size="sm"
                                    rounded="full"
                                >
                                    <FaSearch />
                                </IconButton>
                            </Flex>
                        }
                    >
                        <Input
                            aria-label={t('pooling.index.search.placeholder')}
                            placeholder={t('pooling.index.search.placeholder')}
                            size="xl"
                            w="100%"
                            bg={useColorModeValue('white', 'gray.700')}
                            rounded="full"
                            color={useColorModeValue('gray.700', 'white')}
                            py={2}
                            {...register('searchTerms')}
                            value={tempSearchTerms}
                            onChange={(e) => setTempSearchTerms(e.target.value)}
                            onFocus={() => {
                                if (onFocus) {
                                    onFocus()
                                }
                                setIsFocused(true)
                            }}
                            onBlur={() => {
                                if (onBlur) {
                                    onBlur()
                                }
                                setIsFocused(false)
                            }}
                        />
                    </InputGroup>
                </form>
            </motion.div>
        </Flex>
    )
}
