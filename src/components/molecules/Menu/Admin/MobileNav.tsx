import React from 'react'
import { Stack } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { AdminNav } from './DesktopNav'
import { MobileNavItem } from './MobileNavItem'

export const MobileNav = ({ menuItems }: AdminNav) => {
    return (
        <Stack
            bg={useColorModeValue('white', 'gray.800')}
            p={4}
            display={{ md: 'none' }}
        >
            {menuItems.map((navItem) => (
                <MobileNavItem key={navItem.label} {...navItem} />
            ))}
        </Stack>
    )
}
