'use client'

import React from 'react'
import {
    Box,
    Collapsible,
    Flex,
    IconButton,
    Stack,
    Text,
    useBreakpointValue,
    useDisclosure,
} from '@chakra-ui/react'
import { TFunction } from 'i18next'
import { useTranslation } from 'react-i18next'
import { useColorModeValue } from '@components/ui/color-mode'
import { FaHamburger, FaTimes } from 'react-icons/fa'
import { Link } from 'react-router'
import { DesktopNav } from './DesktopNav'
import { MobileNav } from './MobileNav'
import { AdminNavItem } from './types'

const AdminMenu = (t: TFunction): Array<AdminNavItem> => {
    return [
        {
            label: t('admin.menu.configuration.label'),
            disabled: true,
            children: [
                {
                    label: t('admin.menu.configuration.instance.label'),
                    disabled: true,
                    href: '/admin/users',
                },
            ],
        },
        {
            label: t('admin.menu.accountManagement.label'),
            children: [
                {
                    label: t('admin.menu.accountManagement.users.label'),
                    href: '/admin/users',
                },
                {
                    label: t('admin.menu.communities.label'),
                    href: '/admin/communities',
                },
                {
                    label: t('admin.menu.accountManagement.invitations.label'),
                    href: '/admin/invitations',
                },
            ],
        },
        {
            label: t('admin.menu.pooling.label'),
            disabled: true,
            children: [
                {
                    label: t('admin.menu.pooling.material.label'),
                    disabled: true,
                    href: '/admin/pooling/material',
                },
                {
                    label: t('admin.menu.pooling.material.booking.label'),
                    disabled: true,
                    href: '/admin/pooling/material/booking',
                },
            ],
        },
    ]
}

export const AdminNavBar = () => {
    const { open, onToggle } = useDisclosure()
    const { t } = useTranslation()

    return (
        <Box>
            <Flex
                bg="yellow.300"
                color="black"
                minH="60px"
                py={{ base: 2 }}
                px={{ base: 4 }}
                borderBottom={1}
                borderStyle="solid"
                borderColor={useColorModeValue('gray.200', 'gray.900')}
                align="center"
            >
                <Flex
                    flex={{ base: 1, md: 'auto' }}
                    ml={{ base: -2 }}
                    display={{ base: 'flex', md: 'none' }}
                >
                    <IconButton
                        onClick={onToggle}
                        variant="ghost"
                        aria-label="Toggle Navigation"
                    >
                        {open ? <FaTimes /> : <FaHamburger />}
                    </IconButton>
                </Flex>
                <Flex
                    flex={{ base: 1 }}
                    justify={{ base: 'center', md: 'start' }}
                >
                    <Link to="/admin">
                        <Text
                            textAlign={useBreakpointValue({
                                base: 'center',
                                md: 'left',
                            })}
                            fontFamily="heading"
                            fontWeight="bold"
                            color={useColorModeValue('gray.800', 'black')}
                        >
                            {t('admin.title')}
                        </Text>
                    </Link>

                    <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
                        <DesktopNav menuItems={AdminMenu(t)} />
                    </Flex>
                </Flex>

                <Stack
                    flex={{ base: 1, md: 0 }}
                    justify="flex-end"
                    direction="row"
                    gap={6}
                />
            </Flex>

            <Collapsible.Root in={open} animateOpacity>
                <MobileNav menuItems={AdminMenu(t)} />
            </Collapsible.Root>
        </Box>
    )
}
