import React from 'react'
import { useColorModeValue } from '@components/ui/color-mode'
import {
    Box,
    Flex,
    Icon,
    Link as ChakraLink,
    Stack,
    Text,
} from '@chakra-ui/react'
import { FaChevronRight } from 'react-icons/fa'
import { LinkButton } from '@components/ui/link-button'
import { AdminNavItem } from './types'

export const DesktopSubNav = ({
    label,
    href,
    subLabel,
    disabled,
}: AdminNavItem) => {
    const linkColorDisabled = useColorModeValue('gray.300', 'gray.500')
    const bgHover = useColorModeValue('yellow.100', 'yellow.500')
    return (
        <LinkButton
            asChild
            role="group"
            display="block"
            p={2}
            color={disabled ? linkColorDisabled : 'gray.800'}
            rounded="md"
            _hover={{
                bg: disabled ? undefined : bgHover,
            }}
        >
            <ChakraLink href={!disabled && href ? href : '#'}>
                <Stack direction="row" align="center">
                    <Box>
                        <Text
                            _groupHover={{
                                color: disabled ? undefined : 'yellow.800',
                                cursor: disabled ? 'not-allowed' : 'pointer',
                            }}
                            fontWeight={500}
                        >
                            {label}
                        </Text>
                        <Text fontSize="sm">{subLabel}</Text>
                    </Box>
                    {!disabled && (
                        <Flex
                            transform="translateX(-10px)"
                            opacity={0}
                            _groupHover={{
                                opacity: '100%',
                                transform: 'translateX(0)',
                            }}
                            justify="flex-end"
                            align="center"
                            flex={1}
                        >
                            <Icon color="yellow.400" w={5} h={5}>
                                <FaChevronRight />
                            </Icon>
                        </Flex>
                    )}
                </Stack>
            </ChakraLink>
        </LinkButton>
    )
}
