import React from 'react'
import { Box, Link as ChakraLink, Stack } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { LinkButton } from '@components/ui/link-button'
import {
    PopoverContent,
    PopoverRoot,
    PopoverTrigger,
} from '@components/ui/popover'
import { DesktopSubNav } from './DesktopSubNav'
import { AdminNavItem } from './types'

export type AdminNav = {
    menuItems: AdminNavItem[]
}

export const DesktopNav = ({ menuItems }: AdminNav) => {
    const linkColor = useColorModeValue('gray.600', 'gray.800')
    const linkColorDisabled = useColorModeValue('gray.500', 'gray.500')
    const linkHoverColor = useColorModeValue('gray.800', 'black')
    const linkHoverColorDisabled = useColorModeValue('gray.700', 'gray.700')
    const popoverContentBgColor = useColorModeValue('white', 'yellow.400')

    return (
        <Stack direction="row" gap={4}>
            {menuItems.map((navItem) => (
                <Box key={navItem.label}>
                    <PopoverRoot trigger="hover" placement="bottom-start">
                        <PopoverTrigger>
                            <LinkButton
                                asChild
                                p={2}
                                fontSize="sm"
                                fontWeight={500}
                                color={
                                    navItem.disabled
                                        ? linkColorDisabled
                                        : linkColor
                                }
                                _hover={{
                                    textDecoration: 'none',
                                    color: navItem.disabled
                                        ? linkHoverColorDisabled
                                        : linkHoverColor,
                                }}
                            >
                                <ChakraLink
                                    href={
                                        !navItem.disabled && navItem.href
                                            ? navItem.href
                                            : '#'
                                    }
                                >
                                    {navItem.label}
                                </ChakraLink>
                            </LinkButton>
                        </PopoverTrigger>

                        {navItem.children && (
                            <PopoverContent
                                border={0}
                                boxShadow="xl"
                                bg={popoverContentBgColor}
                                p={4}
                                roundedBottom="xl"
                                minW="sm"
                            >
                                <Stack>
                                    {navItem.children.map((child) => (
                                        <DesktopSubNav
                                            key={child.label}
                                            {...child}
                                        />
                                    ))}
                                </Stack>
                            </PopoverContent>
                        )}
                    </PopoverRoot>
                </Box>
            ))}
        </Stack>
    )
}
