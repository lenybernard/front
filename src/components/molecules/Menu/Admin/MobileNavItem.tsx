import React from 'react'
import {
    Box,
    Collapsible,
    Icon,
    Stack,
    Text,
    useDisclosure,
} from '@chakra-ui/react'
import { FaChevronDown } from 'react-icons/fa'
import { Link } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { AdminNavItem } from './types'

export const MobileNavItem = ({ label, children, href }: AdminNavItem) => {
    const { open: isOpen, onToggle } = useDisclosure()

    return (
        <Stack gap={4} onClick={children && onToggle}>
            <Box
                py={2}
                asChild
                justifyContent="space-between"
                alignItems="center"
                _hover={{
                    textDecoration: 'none',
                }}
            >
                <Link to={href ?? '#'}>
                    <>
                        <Text
                            fontWeight={600}
                            color={useColorModeValue('gray.600', 'gray.200')}
                        >
                            {label}
                        </Text>
                        {children && (
                            <Icon
                                transition="all .25s ease-in-out"
                                transform={isOpen ? 'rotate(180deg)' : ''}
                                w={6}
                                h={6}
                            >
                                <FaChevronDown />
                            </Icon>
                        )}
                    </>
                </Link>
            </Box>

            <Collapsible.Root
                in={isOpen}
                animateOpacity
                style={{ marginTop: '0!important' }}
            >
                <Stack
                    mt={2}
                    pl={4}
                    borderLeft={1}
                    borderStyle="solid"
                    borderColor={useColorModeValue('gray.200', 'gray.700')}
                    align="start"
                >
                    {children &&
                        children.map((child) => (
                            <Box asChild key={child.label} py={2}>
                                <Link to={child.href ?? '#'}>
                                    {child.label}
                                </Link>
                            </Box>
                        ))}
                </Stack>
            </Collapsible.Root>
        </Stack>
    )
}
