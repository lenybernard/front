import { Button } from '@components/ui/button'
import { useTranslation } from 'react-i18next'
import {
    DialogBackdrop,
    DialogBody,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'

type ModalProps = {
    isOpen: boolean
    onClose: () => void
}

export const SubscriptionCongratulationModal = ({
    isOpen,
    onClose,
}: ModalProps) => {
    const { t } = useTranslation()

    return (
        <DialogRoot
            isOpen={isOpen}
            onClose={onClose}
            placement={'center'}
            closeOnEsc={false}
            closeOnOverlayClick={false}
        >
            <DialogBackdrop />
            <DialogContent>
                <DialogHeader>
                    {t('subscription.subscribe.congratualtionModal.title')}
                </DialogHeader>
                <DialogBody>
                    {t('subscription.subscribe.congratualtionModal.content')}
                </DialogBody>
                <DialogFooter>
                    <Button colorPalette="yellow" mr={3} onClick={onClose}>
                        {t('global.continue')}
                    </Button>
                </DialogFooter>
            </DialogContent>
        </DialogRoot>
    )
}
