import React, { ReactNode } from 'react'
import {
    Alert,
    Box,
    Button,
    ClipboardRoot,
    DialogTrigger,
    Flex,
    Input,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import {
    CommunicationMode,
    ContactUserModal__UserFragment,
} from '@_/graphql/api/generated'
import {
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
    DialogTitle,
} from '@components/ui/dialog'
import { ClipboardButton } from '@_/components/ui/clipboard'
import { useColorModeValue } from '@components/ui/color-mode'
import { LuMessageCircle } from 'react-icons/lu'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { withMask } from 'use-mask-input'

type Props = {
    title?: ReactNode
    user: ContactUserModal__UserFragment
}
export const ContactUserModal = ({ title, user }: Props) => {
    const { t } = useTranslation()
    const dialogBgColor = useColorModeValue('blue.50', 'blue.900')
    const contactValue =
        (user.settings?.preferedCommunicationMode &&
            (user.settings?.preferedCommunicationMode ===
            CommunicationMode.Email
                ? user.email
                : `+${user.phoneNumber.countryCode}${user.phoneNumber.nationalNumber.replace(/^0+/, '')}`)) ||
        undefined

    return (
        contactValue && (
            <DialogRoot placement={'center'} motionPreset="slide-in-bottom">
                <DialogTrigger asChild>
                    <Button variant="subtle" colorPalette={'gray'}>
                        <LuMessageCircle />{' '}
                        {title || t('social.message.button.label')}
                    </Button>
                </DialogTrigger>
                <DialogContent bgColor={dialogBgColor}>
                    <DialogHeader>
                        <DialogTitle>
                            {t('social.message.contact.modal.title', {
                                userName: user.fullname,
                            })}
                        </DialogTitle>
                    </DialogHeader>
                    <DialogBody>
                        <Flex direction="column" gap={5}>
                            <Box>
                                {user.settings!.preferedCommunicationMode ===
                                CommunicationMode.Email ? (
                                    <Flex direction="column" gap={5}>
                                        {t(
                                            'social.message.contact.modal.descriptionEmail',
                                            {
                                                userName: user.fullname,
                                            }
                                        )}
                                        <Flex gap={2} alignItems="center">
                                            <Text
                                                as="em"
                                                color={'yellow.400'}
                                                fontSize="xl"
                                            >
                                                {contactValue}
                                            </Text>
                                            <ClipboardRoot
                                                value={contactValue}
                                                cursor={'copy'}
                                            >
                                                <ClipboardButton />
                                            </ClipboardRoot>
                                        </Flex>
                                    </Flex>
                                ) : (
                                    <Flex direction="column" gap={5}>
                                        <EmphasisText color={'yellow.400'}>
                                            {t(
                                                'social.message.contact.modal.descriptionPhone',
                                                {
                                                    userName: user.fullname,
                                                }
                                            )}
                                        </EmphasisText>
                                        <Flex gap={2}>
                                            <a
                                                href={`tel:${contactValue}`}
                                                style={{
                                                    fontWeight: 'bold',
                                                }}
                                            >
                                                <Input
                                                    cursor="alias"
                                                    value={contactValue}
                                                    size={'xl'}
                                                    ref={withMask(
                                                        '(99) 9 99 99 99 99'
                                                    )}
                                                />
                                            </a>
                                            <ClipboardRoot
                                                value={contactValue}
                                                cursor={'copy'}
                                            >
                                                <ClipboardButton />
                                            </ClipboardRoot>
                                        </Flex>
                                    </Flex>
                                )}
                            </Box>
                            <Alert.Root status="warning">
                                <Alert.Indicator />
                                <Alert.Title>
                                    {t(
                                        'social.message.contact.modal.privacyWarning'
                                    )}
                                </Alert.Title>
                            </Alert.Root>
                        </Flex>
                    </DialogBody>
                    <DialogCloseTrigger />
                </DialogContent>
            </DialogRoot>
        )
    )
}
