import React from 'react'
import { motion } from 'framer-motion'
import { AvatarFan__UserFragment } from '@_/graphql/api/generated'
import { Avatar } from '@components/ui/avatar'

type Props = {
    owners: AvatarFan__UserFragment[]
}

export const AvatarFan = ({ owners }: Props) => {
    const fanVariants = {
        hidden: {
            opacity: 0,
            scale: 0.5,
        },
        visible: (i: number) => ({
            opacity: 1,
            scale: 1,
            transition: {
                delay: i * 0.1,
            },
        }),
    }

    return (
        <>
            {owners.map(({ id, firstname, lastname, avatar }, index) => (
                <motion.div
                    key={id}
                    custom={index}
                    variants={fanVariants}
                    initial="hidden"
                    animate="visible"
                    style={{
                        position: 'absolute',
                        top: 0,
                        left: `${index * 15}px`,
                    }}
                >
                    <Avatar
                        src={avatar?.contentUrl}
                        name={`${firstname} ${lastname}`}
                        size="sm"
                    />
                </motion.div>
            ))}
        </>
    )
}
