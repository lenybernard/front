import React, { ReactNode } from 'react'
import { Box, Container, Heading, SimpleGrid, Text } from '@chakra-ui/react'
import { StatCard } from './StatCard'
import { EmphasisText } from '../../../atoms/Text/EmphasisText'

type Props = {
    heading: string
    subHeading?: string
    stats: {
        description: string
        icon?: ReactNode
        source: {
            name: string
            link: string
        }
        title: string
        value: string
    }[]
}

export const StatsWithCard = ({ heading, subHeading, stats }: Props) => {
    return (
        <Box as="section" pb="20">
            <Box bg="blue.600" pt="20" pb="60">
                <Container textAlign="center" px={{ base: '6', md: '8' }}>
                    <Heading
                        size="2xl"
                        letterSpacing="tight"
                        fontWeight="extrabold"
                        lineHeight="normal"
                    >
                        <EmphasisText color="yellow.400">
                            {heading}
                        </EmphasisText>
                    </Heading>
                    {subHeading && (
                        <Text fontSize="lg" mt="4" fontWeight="medium">
                            {subHeading}
                        </Text>
                    )}
                </Container>
            </Box>
            <Box mt="-20">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <SimpleGrid
                        columns={{ base: 1, md: 2, lg: 4 }}
                        columnGap={{ base: 10, lg: 6 }}
                        rowGap={{ base: '120px', lg: 6 }}
                    >
                        {stats &&
                            stats.map(({ title, value, description, icon }) => (
                                <StatCard
                                    key={title}
                                    title={title}
                                    value={value}
                                    icon={icon}
                                >
                                    <EmphasisText color="yellow.400">
                                        {description}
                                    </EmphasisText>
                                </StatCard>
                            ))}
                    </SimpleGrid>
                </Box>
            </Box>
        </Box>
    )
}
