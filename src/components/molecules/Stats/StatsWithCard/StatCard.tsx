import React, { ReactNode } from 'react'
import { Box, BoxProps, Text } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { EmphasisText } from '../../../atoms/Text/EmphasisText'

interface StatCardProps extends BoxProps {
    title: string
    value: string
    icon?: ReactNode
    children: ReactNode
}
export const StatCard = (props: StatCardProps) => {
    const { title, value, icon, children } = props
    return (
        <Box
            as="dl"
            bg={useColorModeValue('white', 'gray.900')}
            p="6"
            rounded="lg"
            shadow="md"
        >
            {icon && (
                <Box pos="absolute" m="-90px 90px;">
                    {icon}
                </Box>
            )}
            <Text
                as="dt"
                color={useColorModeValue('blue.500', 'blue.300')}
                fontSize="sm"
                fontWeight="bold"
            >
                {title}
            </Text>
            <Text
                as="dd"
                fontSize="5xl"
                fontWeight="extrabold"
                lineHeight="1"
                my="4"
            >
                <EmphasisText color="yellow.400">{value}</EmphasisText>
            </Text>
            <Text as="dd" color={useColorModeValue('gray.600', 'white')}>
                {children}
            </Text>
        </Box>
    )
}
