import { UserListFormProps } from '@components/molecules/Form/Pooling/Material/ConditionsMaterialForm/UserListForm.types'
import React from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { Controller } from 'react-hook-form'
import { Select } from 'chakra-react-select'
import { Field } from '@components/ui/field'

export const UserListForm = ({
    control,
    options,
    ...otherSelectProps
}: UserListFormProps) => {
    const { t } = useTranslation()

    return (
        <Controller
            control={control}
            name={'userLists'}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { error },
            }) => (
                <Field
                    label={t(`pooling.form.userList.label`)}
                    errorText={error?.message}
                >
                    {/* @ts-ignore-next-line */}
                    <Select
                        name={name}
                        isDisabled={!options}
                        ref={ref}
                        onChange={onChange}
                        onBlur={onBlur}
                        value={value}
                        placeholder={t(`pooling.form.userList.placeholder`)}
                        options={options}
                        className="chakra-react-select"
                        classNamePrefix="chakra-react-select"
                        selectedOptionStyle="check"
                        {...otherSelectProps}
                    />
                </Field>
            )}
        />
    )
}
