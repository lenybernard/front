import { Dispatch, SetStateAction } from 'react'
import {
    BookingSummary__MaterialBookingFragment,
    User,
} from '@_/graphql/api/generated'
import { ButtonState } from '@_/types'

export type Props = {
    booking: BookingSummary__MaterialBookingFragment
    material: {
        mainOwner?: Pick<User, 'firstname'>
    }
    setButtonState: Dispatch<SetStateAction<ButtonState | null>>
    buttonState: ButtonState | null
    setStep: Dispatch<SetStateAction<'initial' | 'choosePeriod' | 'pending'>>
}
