import { Box, Text, Textarea } from '@chakra-ui/react'
import * as React from 'react'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import moment, { locale } from 'moment'
import 'moment/locale/fr'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { useApolloClient } from '@apollo/client'
import { FieldValues, useForm } from 'react-hook-form'
import { BookingContext } from '@_/contexts/BookingContext'
import { useAuth } from '@_/auth/useAuth'
import { moneyFormat } from '@utils/money'
import { randomTrans } from '@utils/random'
import { RainbowButton } from '@components/atoms/Button'
import { useBookingSummaryApplyBookingMutation } from '@_/graphql/api/generated'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { Field } from '@components/ui/field'
import { Props } from './types'

export const BookingSummary = ({
    booking,
    material,
    buttonState,
    setButtonState,
    setStep,
}: Props) => {
    const { t, i18n } = useTranslation()
    const client = useApolloClient()
    locale(i18n.language)
    const auth = useAuth()

    const [apply] = useBookingSummaryApplyBookingMutation({
        update() {
            client.resetStore()
        },
    })
    const bookingContext = React.useContext(BookingContext)
    const { register, handleSubmit } = useForm<FieldValues>()
    const loadingButtonState = {
        loadingText: t('pooling.show.booking.summary.button.pending'),
        bgGradient: 'linear(to-l, purple.400, pink.400)',
        color: 'white',
        loading: true,
    }

    const onSubmit = (data: FieldValues) => {
        setButtonState(loadingButtonState)

        apply({
            variables: {
                id: data.id,
                message: data.message,
            },
        }).then((r) => {
            if (r.data?.changeStatusMaterialBooking?.materialBooking === null) {
                toast.error(
                    randomTrans(t, 'pooling.show.booking.validate.error', 5)
                )
                setButtonState(null)

                return
            }
            setStep('pending')
            bookingContext?.setBooking(
                r.data?.changeStatusMaterialBooking?.materialBooking
            )
        })
    }

    return (
        booking?.periods && (
            <Box color="gray.300" textAlign="center">
                <Text m="1rem" fontSize="lg">
                    <EmphasisText color={'yellow.400'}>
                        {t('pooling.show.booking.summary.introduction', {
                            mainOwner: material.mainOwner,
                        })}
                    </EmphasisText>
                </Text>
                {booking.periods.map((period) => {
                    return (
                        <div key={`period-${period.id}`}>
                            {(moment(period.startDate).format('L') !==
                                moment(period.endDate).format('L') &&
                                t(
                                    'pooling.show.booking.summary.periodItem.range.label',
                                    {
                                        startDate: moment(
                                            period.startDate
                                        ).format('LL'),
                                        endDate: moment(period.endDate).format(
                                            'LL'
                                        ),
                                        price: period.price,
                                    }
                                )) ||
                                t(
                                    'pooling.show.booking.summary.periodItem.singleDay.label',
                                    {
                                        day: moment(period.startDate).format(
                                            'LL'
                                        ),
                                    }
                                )}
                        </div>
                    )
                })}
                <Text>
                    {t('pooling.show.booking.summary.price', {
                        context:
                            booking.price && booking.price > 0
                                ? 'cost'
                                : 'free',
                        price: moneyFormat(booking.price ?? 0, i18n.language),
                    })}
                </Text>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input
                        type="hidden"
                        {...register('id', { value: booking.id })}
                    />
                    <Field
                        id="message"
                        my="5"
                        label={t('pooling.show.booking.summary.message.label')}
                    >
                        <Textarea
                            borderColor="gray.300"
                            _hover={{
                                borderRadius: 'gray.300',
                            }}
                            rows={10}
                            placeholder={t(
                                'pooling.show.booking.summary.message.placeholder',
                                {
                                    user: material.mainOwner,
                                    me: auth?.user?.firstname,
                                }
                            )}
                            {...register('message', {
                                value: t(
                                    'pooling.show.booking.summary.message.defaultMessage',
                                    {
                                        user: material.mainOwner,
                                        me: auth?.user?.firstname,
                                    }
                                ),
                            })}
                        />
                    </Field>
                    <RainbowButton type="submit" mt="1rem" {...buttonState}>
                        {t('pooling.show.booking.summary.button.apply')}
                    </RainbowButton>
                </form>
            </Box>
        )
    )
}
