import React, { Dispatch, SetStateAction, useEffect, useState } from 'react'
import { Box, Heading, Spacer, VStack } from '@chakra-ui/react'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import { DateRangePicker, FocusedInputShape, isSameDay } from 'react-dates'
import moment, { locale } from 'moment'
import 'moment/locale/fr'
import { FaArrowLeft } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { useApolloClient } from '@apollo/client'
import { toast } from 'react-toastify'
import { ButtonState } from '@_/types'
import { BookingContext } from '@_/contexts/BookingContext'
import {
    AvailabilityPlanning__MaterialFragment,
    useAvailabilityPlanningEstimateMutation,
} from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { Button } from '@components/ui/button'
import { BookingSummary } from '../BookingSummary/BookingSummary'

export const AvailabilityPlanning = ({
    material,
    onBack,
    setStep,
}: {
    material: AvailabilityPlanning__MaterialFragment
    onBack: () => void
    setStep: Dispatch<SetStateAction<'initial' | 'choosePeriod' | 'pending'>>
}) => {
    const { t, i18n } = useTranslation()
    const [fromDate, setFromDate] = useState<moment.Moment | null>(null)
    const [toDate, setToDate] = useState<moment.Moment | null>(null)
    const [focus, setFocus] = useState<FocusedInputShape | null>('startDate')
    const [buttonState, setButtonState] = useState<null | ButtonState>(null)
    const [summary, setSummary] = useState<Element>()
    const bookingContext = React.useContext(BookingContext)
    const client = useApolloClient()
    locale(i18n.language)

    const [estimate] = useAvailabilityPlanningEstimateMutation({
        update() {
            client.resetStore()
        },
    })

    const bookedDays: moment.Moment[] = []
    material.bookings?.edges
        ?.filter(({ node }) => ['pending', 'confirmed'].includes(node!.status))
        .flatMap((edge) =>
            edge.node?.periods?.map((period) => {
                const { startDate, endDate } = period
                bookedDays.push(moment(startDate))
                let start = moment(startDate)
                const end = moment(endDate)
                if (start < end) {
                    while (start < end) {
                        const newDate = start.add(1, 'day')
                        start = moment(newDate)
                        bookedDays.push(newDate)
                    }
                }
                bookedDays.push(moment(endDate))

                return true
            })
        )

    useEffect(() => {
        if (bookingContext?.booking) {
            setSummary(
                // @ts-ignore
                <BookingSummary
                    booking={bookingContext?.booking}
                    material={material}
                    setButtonState={setButtonState}
                    buttonState={buttonState}
                    setStep={setStep}
                />
            )
        }
    }, [bookingContext, buttonState, material, setStep])

    return (
        <Box
            w="full"
            bg={useColorModeValue('white', 'gray.900')}
            boxShadow="2xl"
            rounded="lg"
            p={6}
        >
            {onBack && (
                <Button onClick={onBack}>
                    <FaArrowLeft />
                    {t('global.back')}
                </Button>
            )}
            <VStack>
                <>
                    <Heading as="h2" size="lg">
                        {t('pooling.show.booking.choosePeriod.title')}
                    </Heading>
                    <Spacer />
                    <DateRangePicker
                        startDatePlaceholderText={t(
                            'pooling.show.booking.choosePeriod.startDate'
                        )}
                        endDatePlaceholderText={t(
                            'pooling.show.booking.choosePeriod.endDate'
                        )}
                        showClearDates
                        hideKeyboardShortcutsPanel
                        startDate={fromDate} // momentPropTypes.momentObj or null,
                        startDateId="availabilityBookingInput_StartDate" // PropTypes.string.required,
                        endDate={toDate} // momentPropTypes.momentObj or null,
                        endDateId="availabilityBookingInput_EndDate" // PropTypes.string.required,
                        onDatesChange={({ startDate, endDate }) => {
                            setFromDate(startDate)
                            setToDate(endDate)
                            if (startDate && endDate) {
                                estimate({
                                    variables: {
                                        startDate: startDate.format(),
                                        endDate: endDate.format(),
                                        materialId: material.id,
                                    },
                                }).then((r) => {
                                    if (
                                        r.data?.estimateMaterialBooking
                                            ?.materialBooking === null
                                    ) {
                                        toast.error(
                                            t(
                                                'pooling.booking.estimate.toast',
                                                {
                                                    context: 'error',
                                                }
                                            )
                                        )
                                        return
                                    }
                                    bookingContext?.setBooking(
                                        r.data?.estimateMaterialBooking
                                            ?.materialBooking
                                    )
                                })
                            }
                        }} // PropTypes.func.required,
                        focusedInput={focus} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                        onFocusChange={(focusedInput) => setFocus(focusedInput)} // PropTypes.func.required,
                        minimumNights={0}
                        numberOfMonths={1}
                        firstDayOfWeek={1}
                        displayFormat="DD/MM/Y"
                        isDayBlocked={(momentDate) =>
                            isSameDay(moment(), momentDate) ||
                            bookedDays.some((bookedDay) =>
                                isSameDay(bookedDay, momentDate)
                            )
                        }
                    />
                    <Spacer />

                    {bookingContext?.booking && summary}
                </>
            </VStack>
        </Box>
    )
}
