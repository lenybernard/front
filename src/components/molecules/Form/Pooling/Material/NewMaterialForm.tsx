import React from 'react'
import 'react-phone-number-input/style.css'
import { Box, Center, Spinner, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { generatePath, useNavigate } from 'react-router'
import { FormProvider, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { FaSave } from 'react-icons/fa'
import {
    CreateMaterialInput,
    useCreateMaterialMutation,
} from '@_/graphql/api/generated'
import { useAuth } from '@_/auth/useAuth'
import { useColorModeValue } from '@components/ui/color-mode'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { H2, Heading } from '../../../../atoms/Heading'
import { SubmitButton } from '../../../../atoms/Button'
import { InformationFormValues, MaterialFormSteps } from './types'
import { InformationMaterialForm } from './InformationMaterialForm/InformationMaterialForm'
import { prepareFormData } from './prepareFormData'

export const NewMaterialForm = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const auth = useAuth()

    const [createMaterial] = useCreateMaterialMutation()
    const onSubmit = ({
        mainOwnership,
        ownerships,
        ...otherValues
    }: InformationFormValues) => {
        createMaterial({
            variables: {
                input: prepareFormData<CreateMaterialInput>(
                    {
                        ...otherValues,
                        price: 0,
                    },
                    mainOwnership
                ),
            },
        }).then((r) => {
            if (r.data?.createMaterial?.material) {
                toast.success(t('pooling.new.toast.success'))
                navigate(
                    generatePath(PoolingRoutes.EditMyMaterialStep, {
                        slug: r.data.createMaterial.material.slug,
                        step: MaterialFormSteps.Medias,
                    })
                )
            }
        })
    }
    const currentUserIRI = `/users/${auth?.user?.slug}` as const
    const methods = useForm<InformationFormValues>({
        defaultValues: {
            mainOwnership: auth?.user
                ? {
                      value: currentUserIRI,
                      label: auth.user.fullname,
                  }
                : undefined,
        },
    })
    const {
        handleSubmit,
        formState: { isSubmitting },
    } = methods

    return (
        <Box py={15} my={50}>
            <Box textAlign="center">
                <Heading as={'h1'} fontSize={'3xl'}>
                    {t('pooling.new.title')}
                </Heading>
                <H2>{t('pooling.new.tellUsMore')}</H2>
            </Box>
            <Stack
                bg={useColorModeValue('white', 'gray.800')}
                rounded="3xl"
                p={{ base: 4, sm: 6, md: 8 }}
                my={{ base: 4, sm: 6, md: 8 }}
                w="100%"
            >
                <FormProvider {...methods}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <InformationMaterialForm />
                        <Center mt={10}>
                            <SubmitButton
                                disabled={isSubmitting}
                                loading={isSubmitting}
                            >
                                {t('global.save')}
                                {(isSubmitting && <Spinner />) || <FaSave />}
                            </SubmitButton>
                        </Center>
                    </form>
                </FormProvider>
            </Stack>
        </Box>
    )
}
