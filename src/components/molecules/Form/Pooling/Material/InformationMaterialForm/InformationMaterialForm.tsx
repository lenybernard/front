import { Flex, Input, SimpleGrid, Textarea, VStack } from '@chakra-ui/react'
import React, { ReactNode, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { AnimatePresence, motion } from 'framer-motion'
import { useFormContext, useWatch } from 'react-hook-form'
import { RequiredAsterisk } from '@components/atoms/Form/RequiredAsterisk'
import { useAuth } from '@_/auth/useAuth'
import { useInformationMaterialForm_CirclesOptionsQuery } from '@_/graphql/api/generated'
import { OwnershipMaterialForm } from '@components/atoms/Form/Material/Ownership/OwnershipMaterialForm'
import { Field } from '@components/ui/field'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { generatePath } from 'react-router'
import { CirclePlanIcon } from '@components/molecules/Form/Pooling/Material/InformationMaterialForm/CirclePlanIcon'
import {
    InformationFormValues,
    InformationMaterialFormProps,
    IRI,
} from '../types'

export const InformationMaterialForm = ({
    material,
}: InformationMaterialFormProps) => {
    const { control, register, setValue, watch } =
        useFormContext<InformationFormValues>()
    const { t } = useTranslation()

    const auth = useAuth()

    const [circleOptions, setCircleOptions] = useState<
        {
            value: IRI<'circles', string>
            label: ReactNode
            plan?: string
            group?: string
        }[]
    >([])

    const mainOwnership = useWatch({ control, name: 'mainOwnership' })
    const filteredCircleOptions = circleOptions.filter(
        (option) => option.value !== mainOwnership?.value
    )
    const [isScopeEligible, setIsScopeEligible] = useState(false)

    const ownerships = watch('ownerships')
    const currentUserIRI = generatePath(UserRoutes.IRI, {
        slug: auth?.user?.slug!,
    })

    useEffect(() => {
        if (
            mainOwnership?.value &&
            ownerships?.some(({ value }) => value === mainOwnership.value)
        ) {
            const updatedOwnerships = ownerships.filter(
                ({ value }) => value !== mainOwnership.value
            )
            setValue('ownerships', updatedOwnerships)
        }
    }, [mainOwnership, ownerships, setValue])

    useInformationMaterialForm_CirclesOptionsQuery({
        onCompleted: (data) => {
            if (data.myCircles?.edges) {
                const options = data.myCircles.edges.map((edge) => {
                    const { id, name, subscription } = edge.node!
                    return {
                        value: id as IRI<'circles', string>,
                        label: (
                            <>
                                {name}
                                {subscription?.plan && (
                                    <CirclePlanIcon plan={subscription?.plan} />
                                )}
                            </>
                        ),
                        plan: subscription?.plan?.slug,
                        group: t('communities.my_communities.index.title'),
                    }
                })
                setCircleOptions(options)
            }
        },
    })
    useEffect(() => {
        const circle = circleOptions.find(
            (option) => option.value === mainOwnership?.value
        )
        setIsScopeEligible(['community', 'pro'].includes(circle?.plan ?? ''))
    }, [mainOwnership, circleOptions])

    return (
        <VStack>
            <Flex direction={{ base: 'column', md: 'row' }} w="100%">
                <Flex direction="column" w="100%" gap={5}>
                    <Field
                        w="100%"
                        label={
                            <>
                                {t('pooling.form.name.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        }
                    >
                        <Input
                            type="text"
                            placeholder={t('pooling.form.name.placeholder')}
                            required
                            {...register('name', {
                                value: material?.name,
                                required: t('form.errors.required').toString(),
                                minLength: {
                                    value: 4,
                                    message: t('form.errors.tooShort', {
                                        count: 4,
                                    }),
                                },
                            })}
                        />
                    </Field>
                    <SimpleGrid columns={{ base: 1, md: 2 }} gap={5}>
                        <Field
                            w={{ base: '100%', md: 'auto' }}
                            label={t('pooling.form.brand.label')}
                        >
                            <Input
                                type="text"
                                required={false}
                                {...register('brand', {
                                    required: false,
                                    value: material?.brand,
                                })}
                            />
                        </Field>
                        <Field label={t('pooling.form.model.label')}>
                            <Input
                                type="text"
                                placeholder={t(
                                    'pooling.form.model.placeholder'
                                )}
                                required={false}
                                {...register('model', {
                                    required: false,
                                    value: material?.model,
                                })}
                            />
                        </Field>
                    </SimpleGrid>
                    <Field w="100%" label={t('pooling.form.description.label')}>
                        <Textarea
                            rows={10}
                            placeholder={t(
                                'pooling.form.description.placeholder'
                            )}
                            {...register('description', {
                                value: material?.description,
                            })}
                        />
                    </Field>
                    {circleOptions.length > 0 && (
                        <SimpleGrid columns={{ base: 1, md: 2 }} gap={5}>
                            <OwnershipMaterialForm
                                inputName="mainOwnership"
                                options={[
                                    {
                                        value: currentUserIRI,
                                        label: auth?.user?.fullname ?? 'me',
                                    },
                                    ...circleOptions,
                                ]}
                                closeMenuOnSelect
                            />
                            <AnimatePresence>
                                {isScopeEligible && (
                                    <motion.div
                                        initial={{ opacity: 0, height: 0 }}
                                        animate={{ opacity: 1, height: 'auto' }}
                                        exit={{ opacity: 0, height: 0 }}
                                        transition={{ duration: 0.5 }}
                                    >
                                        <OwnershipMaterialForm
                                            inputName="ownerships"
                                            control={control}
                                            isMulti
                                            options={[
                                                {
                                                    value: currentUserIRI,
                                                    label:
                                                        auth?.user?.fullname ??
                                                        'me',
                                                },
                                                ...filteredCircleOptions,
                                            ]}
                                        />
                                    </motion.div>
                                )}
                            </AnimatePresence>
                        </SimpleGrid>
                    )}
                </Flex>
            </Flex>
        </VStack>
    )
}
