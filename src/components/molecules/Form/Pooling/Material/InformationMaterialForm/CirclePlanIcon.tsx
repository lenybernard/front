import { HStack } from '@chakra-ui/react'
import { Tag } from '@components/ui/tag'
import { FaAward } from 'react-icons/fa'
import { LuBadgeCheck } from 'react-icons/lu'

export const CirclePlanIcon = ({
    plan: { slug, name },
}: {
    plan: { slug: string; name?: string }
}) => {
    switch (slug) {
        case 'community':
            return (
                <Tag colorPalette="yellow">
                    <HStack>
                        <LuBadgeCheck /> {name || 'Commu'}
                    </HStack>
                </Tag>
            )
        case 'pro':
            return (
                <Tag colorPalette="red">
                    <HStack>
                        <FaAward /> {name || 'PRO'}
                    </HStack>
                </Tag>
            )
        default:
            return ''
    }
}
