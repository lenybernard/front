import {
    CreateMaterialInput,
    UpdateMaterialInput,
} from '@_/graphql/api/generated'

export const prepareFormData = <
    T extends UpdateMaterialInput | CreateMaterialInput,
>(
    otherValues: T,
    mainOwnership?: { label: string; value?: string },
    ownerships?: Array<{ value?: string }>,
    userLists?: Array<{ value?: string }>
) => {
    const ownershipsParams = ownerships?.map((o) => ({ owner: o.value }))

    return {
        ...otherValues,
        ...((ownershipsParams &&
            ownershipsParams.length > 0 && {
                ownerships: ownershipsParams,
            }) || { ownerships: [] }),
        ...(mainOwnership && {
            mainOwnership: { owner: mainOwnership.value },
        }),
        ...((userLists && {
            userLists: userLists.map((u) => u.value),
        }) ||
            {}),
    }
}
