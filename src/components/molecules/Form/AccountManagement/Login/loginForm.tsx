import { Input, Stack, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import React from 'react'
import { Link, useLocation, useNavigate } from 'react-router'
import { useAuth } from '@_/auth/useAuth'
import { randomTrans } from '@utils/random'
import {
    AuthUser__UserFragment,
    ThemeMode,
    useLoginMutation,
} from '@_/graphql/api/generated'
import { client } from '@_/apollo/main/client'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Field } from '@components/ui/field'
import { Button } from '@components/ui/button'
import { PasswordInput } from '@components/ui/password-input'
import { useColorMode } from '@components/ui/color-mode'

type FormData = {
    username: string
    password: string
}

interface LocationState {
    from: {
        pathname: string
    }
}

export const LoginForm = () => {
    const { t, i18n } = useTranslation()
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
    } = useForm<FormData>()

    const navigate = useNavigate()
    const location = useLocation()
    const auth = useAuth()
    const [login] = useLoginMutation()
    const { setColorMode } = useColorMode()

    const onSubmit = (values: FormData) => {
        auth.signout()
        login({
            variables: values,
            onCompleted: (data) => {
                if (data.loginUser?.user) {
                    const { user } = data.loginUser

                    if (user) {
                        auth.signin(user as AuthUser__UserFragment, () => {
                            setColorMode(
                                (
                                    user.settings?.themeMode ?? ThemeMode.System
                                ).toLowerCase()
                            )
                            if (user.settings?.language) {
                                i18n.changeLanguage(user.settings?.language)
                            }
                            toast.success(
                                randomTrans(t, 'login.toast.success', 4, {
                                    user,
                                })
                            )
                            client.resetStore()
                            const from =
                                (location.state as LocationState)?.from ||
                                PoolingRoutes.Search
                            document.body.scrollTop = 0
                            document.documentElement.scrollTop = 0
                            navigate(from, { replace: true })
                        })
                    }
                }
            },
            onError: () => {
                toast.error(randomTrans(t, 'login.toast.error', 5))
            },
        })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack gap={5}>
                <Field
                    id="username"
                    invalid={errors.username !== undefined}
                    label={t('login.form.email.label')}
                >
                    <Input
                        type="email"
                        required
                        autoComplete="email"
                        {...register('username', {
                            required: 'This is required',
                            minLength: {
                                value: 4,
                                message: t('form.errors.tooShort', {
                                    count: 4,
                                }),
                            },
                            pattern: /^\S+@\S+$/i,
                        })}
                    />
                </Field>
                <Field
                    id="password"
                    invalid={errors.password !== undefined}
                    label={t('login.form.password.label')}
                >
                    <PasswordInput
                        {...register('password', {
                            required: t('form.required').toString(),
                            minLength: {
                                value: 4,
                                message: 'Minimum length should be 4',
                            },
                        })}
                    />
                    <Link to={UserRoutes.ForgotPassword}>
                        <Text color="yellow.400">
                            {t('login.forgotPassword')}
                        </Text>
                    </Link>
                </Field>
                <Button
                    color="white"
                    bgGradient="to-r"
                    gradientFrom="red.400"
                    gradientTo="pink.400"
                    _hover={{
                        boxShadow: 'sm',
                    }}
                    loading={isSubmitting}
                    type="submit"
                >
                    {t('login.form.button')}
                </Button>
            </Stack>
        </form>
    )
}
