import React from 'react'
import { Box, Center, Flex, Stack } from '@chakra-ui/react'
import { Skeleton, SkeletonCircle, SkeletonText } from '@components/ui/skeleton'

export const RegistrationFormSkeleton = () => {
    return (
        <Center>
            <Flex flexDirection="column" alignItems="center" w="100%">
                {/* Avatar */}
                <Box alignSelf="flex-start" my="4">
                    <SkeletonCircle size="100px" />
                </Box>

                <Flex width="100%" justifyContent="space-between">
                    <Stack gap={4}>
                        <SkeletonText noOfLines={1} width="200px" />
                        <Skeleton height="40px" width="300px" />
                        <SkeletonText noOfLines={1} width="200px" />
                        <Skeleton height="40px" width="300px" />
                    </Stack>

                    <Stack gap={4}>
                        <SkeletonText noOfLines={1} width="200px" />
                        <Skeleton height="40px" width="300px" />
                        <SkeletonText noOfLines={1} width="200px" />
                        <Skeleton height="40px" width="300px" />
                    </Stack>
                </Flex>

                <Box marginTop="4" alignSelf="center">
                    <Skeleton height="50px" width="200px" />
                </Box>
            </Flex>
        </Center>
    )
}
