import React from 'react'
import { useFormContext, UseFormRegisterReturn } from 'react-hook-form'
import { UserForm__UserAvatarFragment } from '@_/graphql/api/generated'
import { Field } from '@components/ui/field'
import { useTranslation } from 'react-i18next'
import { fileToBase64 } from '@utils/image'
import { UserProfileFormValues } from '@components/molecules/Form/AccountManagement/User/UserForm.types'
import Compressor from 'compressorjs'
import { ImageForm } from '../../../../atoms/Form/ImageForm'

type UserAvatarFormProps = {
    avatar?: UserForm__UserAvatarFragment
    register?: UseFormRegisterReturn
}

export const UserAvatarForm = ({ avatar }: UserAvatarFormProps) => {
    const { t } = useTranslation()
    const {
        setValue,
        formState: { errors },
    } = useFormContext<UserProfileFormValues>()
    const [avatarPreview, setAvatarPreview] = React.useState(avatar?.contentUrl)

    const handleAvatarChange = (file: File) => {
        /* eslint-disable no-new */
        new Compressor(file, {
            quality: 0.8,
            maxWidth: 500,
            maxHeight: 500,
            success(compressedFile: File) {
                fileToBase64(compressedFile).then((avatarFile) => {
                    setValue('avatar', avatarFile)
                    setValue('removeAvatar', false)
                    setAvatarPreview(avatarFile.base64)
                })
            },
            error(err) {
                console.error(
                    'Erreur lors de la compression de l’avatar :',
                    err.message
                )
            },
        })
    }

    const handleAvatarDelete = () => {
        setValue('avatar', null)
        setValue('removeAvatar', true)
        setAvatarPreview(undefined)
    }

    return (
        <Field
            label={
                <>{t('register.wizard.personalInformations.fields.avatar')}</>
            }
            errorText={errors.avatar?.message}
        >
            <ImageForm
                preview={avatarPreview}
                avatarProps={{}}
                onFileChange={handleAvatarChange}
                onDelete={handleAvatarDelete}
            />
        </Field>
    )
}
