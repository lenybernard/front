import React from 'react'
import { Box, HStack, Input, SimpleGrid, VStack } from '@chakra-ui/react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { UserForm__UserFragment as User } from '@_/graphql/api/generated'
import { Field } from '@components/ui/field'
import { UserProfileFormValues } from '@components/molecules/Form/AccountManagement/User/UserForm.types'
import { Button } from '@components/ui/button'
import { UserAvatarForm } from './UserAvatarForm'
import 'react-phone-number-input/style.css'
import { RequiredAsterisk } from '../../../../atoms/Form/RequiredAsterisk'
import { PhoneNumberInput } from '../../../../atoms/Form/PhoneNumberInput'
import 'yup-phone'

export const UserForm = ({
    onSubmit,
    user,
}: {
    onSubmit: (values: UserProfileFormValues) => void
    user?: User
}) => {
    const { t, i18n } = useTranslation()
    const schema = yup.object().shape({
        email: yup.string().email().required(),
        firstname: yup.string().required(),
        lastname: yup.string().required(),
    })
    const defaultCountryCode = i18n.language === 'en' ? 1 : 33
    const methods = useForm<UserProfileFormValues>({
        defaultValues: {
            settings: {
                language: user ? user.settings?.language : i18n.language,
            },
            countryCode: user
                ? user.phoneNumber.countryCode
                : defaultCountryCode,
            removeAvatar: false,
        },
        resolver: yupResolver(schema),
    })
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
    } = methods

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <input type="hidden" {...register('settings.language')} />
                <Box>
                    <VStack pl={0} gap={3} alignItems="flex-start">
                        <SimpleGrid
                            columns={{ base: 1, sm: 3 }}
                            columnGap={10}
                            rowGap={3}
                        >
                            <UserAvatarForm
                                avatar={user?.avatar}
                                register={register('avatar')}
                            />
                            <Field
                                id="firstname"
                                errorText={errors.firstname?.message}
                                label={
                                    <>
                                        {t(
                                            'accountManagement.my.profile.form.firstname.label'
                                        )}{' '}
                                        <RequiredAsterisk />
                                    </>
                                }
                            >
                                <Input
                                    type="text"
                                    placeholder={t(
                                        'accountManagement.my.profile.form.firstname.placeholder'
                                    )}
                                    required
                                    {...register('firstname', {
                                        value: user?.firstname ?? '',
                                        required: t('form.required').toString(),
                                    })}
                                />
                            </Field>
                            <Field
                                id="lastname"
                                label={
                                    <>
                                        {t(
                                            'accountManagement.my.profile.form.lastname.label'
                                        )}{' '}
                                        <RequiredAsterisk />
                                    </>
                                }
                                errorText={errors.lastname?.message}
                            >
                                <Input
                                    type="text"
                                    placeholder={t(
                                        'accountManagement.my.profile.form.lastname.placeholder'
                                    )}
                                    required
                                    {...register('lastname', {
                                        value: user?.lastname,
                                        required: t('form.required').toString(),
                                    })}
                                />
                            </Field>
                        </SimpleGrid>
                        <SimpleGrid
                            columns={{ base: 1, sm: 2 }}
                            columnGap={10}
                            rowGap={3}
                        >
                            <Field
                                id="email"
                                label={
                                    <>
                                        {t(
                                            'accountManagement.my.profile.form.email.label'
                                        )}{' '}
                                        <RequiredAsterisk />
                                    </>
                                }
                                errorText={errors.email?.message}
                            >
                                <Input
                                    type="email"
                                    required
                                    {...register('email', {
                                        value: user?.email,
                                        required: true,
                                        pattern: /^\S+@\S+$/i,
                                    })}
                                />
                            </Field>

                            <Field
                                id="email"
                                label={
                                    <>
                                        {t(
                                            'accountManagement.my.profile.form.phoneNumber.label'
                                        )}
                                    </>
                                }
                                errorText={errors.email?.message}
                            >
                                <PhoneNumberInput
                                    name="phoneNumberObject"
                                    countryCodeRegister={register(
                                        'countryCode',
                                        {
                                            value: user?.phoneNumber
                                                .countryCode,
                                            valueAsNumber: true,
                                        }
                                    )}
                                    nationalNumberRegister={register(
                                        'nationalNumber',
                                        {
                                            value: user?.phoneNumber
                                                .nationalNumber,
                                        }
                                    )}
                                    required
                                    errors={errors}
                                />
                            </Field>
                        </SimpleGrid>
                    </VStack>
                </Box>
                <HStack mt={10}>
                    <Button
                        fontFamily="heading"
                        mt={8}
                        loading={isSubmitting}
                        type="submit"
                    >
                        {(user && t('global.save')) ||
                            t('accountManagement.my.profile.form.button')}
                    </Button>
                </HStack>
            </form>
        </FormProvider>
    )
}
