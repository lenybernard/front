import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { FormProvider, useForm } from 'react-hook-form'
import {
    AddressChangedEvent,
    AddressSearch,
} from '@components/atoms/Form/AddressSearch'
import {
    MyProfileLocationForm_UserFragment,
    useUpdateUserLocationMutation,
} from '@_/graphql/api/generated'
import { UserProfileLocationFormValues } from '@components/molecules/Form/AccountManagement/User/LocationForm/LocationForm.types'
import { Button } from '@components/ui/button'
import { Box } from '@chakra-ui/react'

export const LocationForm = ({
    user,
}: {
    user: MyProfileLocationForm_UserFragment
}) => {
    const { t } = useTranslation()
    const methods = useForm<UserProfileLocationFormValues>()
    const { setValue } = methods
    const [updateUser] = useUpdateUserLocationMutation()
    const [showMap, setShowMap] = useState(false)

    const handleAddressChanged = useCallback(
        ({ coordinates, address }: AddressChangedEvent) => {
            if (address && address !== '') {
                setValue('address', address)
            }
            if (coordinates?.lat && coordinates?.lng) {
                setValue('latitude', coordinates.lat.toString())
                setValue('longitude', coordinates.lng.toString())
            }
        },
        [setValue]
    )

    const onSubmit = (data: UserProfileLocationFormValues) => {
        updateUser({
            variables: {
                input: {
                    id: user.id,
                    streetAddress: data.address,
                    latitude: data.latitude,
                    longitude: data.longitude,
                },
            },
        }).then((response) => {
            if (response.data?.updateUser?.user) {
                const { latitude, longitude, streetAddress } =
                    response.data.updateUser.user
                if (latitude && longitude) {
                    handleAddressChanged({
                        coordinates: { lat: latitude, lng: longitude },
                        address: streetAddress,
                    })
                }
            }
        })
    }

    useEffect(() => {
        if (user?.latitude && user?.longitude) {
            handleAddressChanged({
                coordinates: {
                    lat: user.latitude,
                    lng: user.longitude,
                },
                address: user.streetAddress || '',
            })
        }
    }, [user, handleAddressChanged])

    return (
        <FormProvider {...methods}>
            <form
                style={{ width: '100%' }}
                onSubmit={methods.handleSubmit(onSubmit)}
            >
                {(showMap && (
                    <>
                        <AddressSearch
                            onAddressChanged={handleAddressChanged}
                            defaultValue={{
                                address: user?.streetAddress,
                                latitude: user?.latitude?.toString(),
                                longitude: user?.longitude?.toString(),
                            }}
                        />
                        <Button type="submit" mt={5}>
                            {t('global.save')}
                        </Button>
                    </>
                )) || (
                    <>
                        <Box fontWeight={'bold'}>{user?.streetAddress}</Box>
                        <Button
                            type={'button'}
                            mt={5}
                            variant="outline"
                            onClick={() => setShowMap(true)}
                        >
                            {t('accountManagement.my.profile.location.edit')}
                        </Button>
                    </>
                )}
            </form>
        </FormProvider>
    )
}
