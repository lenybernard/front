export type UserProfileLocationFormValues = {
    latitude: string
    longitude: string
    address: string
}
