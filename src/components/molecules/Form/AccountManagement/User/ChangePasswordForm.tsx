import React from 'react'
import { FieldValues, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import * as yup from 'yup'
import { Field } from '@components/ui/field'
import { PasswordInput } from '@components/ui/password-input'
import { RequiredAsterisk } from '../../../../atoms/Form/RequiredAsterisk'
import { PrimaryButton } from '../../../../atoms/Button'

export const ChangePasswordForm = ({
    onSubmit,
}: {
    onSubmit: (values: FieldValues) => void
}) => {
    const { t } = useTranslation()
    const schema = yup.object().shape({
        currentPass: yup.string().required(),
        newPass: yup.string().min(8).max(32).required(),
    })
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
    } = useForm<FieldValues>({
        resolver: yupResolver(schema),
    })
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Field
                id="currentPass"
                invalid={errors.currentPass !== undefined}
                label={
                    <>
                        {t(
                            'accountManagement.changePassword.form.currentPass.label'
                        )}{' '}
                        <RequiredAsterisk />
                    </>
                }
            >
                <PasswordInput
                    {...register('currentPass', {
                        required: t('form.required').toString(),
                    })}
                    autoComplete="current-password"
                />
            </Field>
            <Field
                id="newPass"
                invalid={errors.newPass !== undefined}
                label={
                    <>
                        {t(
                            'accountManagement.changePassword.form.newPass.label'
                        )}{' '}
                        <RequiredAsterisk />
                    </>
                }
            >
                <PasswordInput
                    {...register('newPass', {
                        required: t('form.required').toString(),
                        minLength: {
                            value: 4,
                            message: 'Minimum length should be 4',
                        },
                    })}
                    autoComplete="new-password"
                />
            </Field>
            <PrimaryButton
                fontFamily="heading"
                mt={8}
                loading={isSubmitting}
                type="submit"
            >
                {t('global.save')}
            </PrimaryButton>
        </form>
    )
}
