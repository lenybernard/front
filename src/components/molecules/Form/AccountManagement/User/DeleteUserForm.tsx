import React from 'react'
import { useTranslation } from 'react-i18next'
import rehypeRaw from 'rehype-raw'
import ReactMarkdown from 'react-markdown'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTrigger,
} from '@components/ui/dialog'
import { H3 } from '@components/atoms/Heading'
import { DangerButton } from '../../../../atoms/Button'
import { EmphasisText } from '../../../../atoms/Text/EmphasisText'

export const DeleteUserForm = ({ onSubmit }: { onSubmit: () => void }) => {
    const { t } = useTranslation()

    return (
        <>
            <DialogRoot placement={'center'}>
                <DialogBackdrop />
                <DialogTrigger>
                    <DangerButton fontFamily="heading" mt={8}>
                        {t('global.delete')}
                    </DangerButton>
                </DialogTrigger>
                <DialogContent>
                    <DialogHeader>
                        <H3>
                            {t('accountManagement.deleteAccount.modal.title')}
                        </H3>
                    </DialogHeader>
                    <DialogCloseTrigger />
                    <DialogBody>
                        <EmphasisText color="red.400">
                            {t('accountManagement.deleteAccount.modal.warning')}
                        </EmphasisText>
                        <br />
                        <br />
                        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                            {t(
                                'accountManagement.deleteAccount.modal.pleaseFeedback',
                                {
                                    email: process.env.REACT_APP_CONTACT_EMAIL,
                                }
                            )}
                        </ReactMarkdown>
                        <br />
                        {t('accountManagement.deleteAccount.modal.seeYou')}
                    </DialogBody>
                    <DialogFooter>
                        <DangerButton onClick={() => onSubmit()}>
                            {t('accountManagement.deleteAccount.modal.confirm')}
                        </DangerButton>
                    </DialogFooter>
                </DialogContent>
            </DialogRoot>
        </>
    )
}
