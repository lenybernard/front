import { Base64File } from '@utils/image'

export type UserProfileFormValues = {
    firstname: string
    lastname: string
    email: string
    gender: string
    settings: {
        language: string
    }
    avatar?: Base64File | null
    removeAvatar?: boolean
    countryCode?: number
    nationalNumber?: string
}
