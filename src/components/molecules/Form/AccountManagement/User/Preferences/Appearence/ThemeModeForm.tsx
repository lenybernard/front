import { RadioCardRoot } from '@components/ui/radio-card'
import { ChoiceCard } from '@components/ui/choice-card'
import { SystemModeIcon } from '@components/atoms/Icon/SystemModeIcon'
import { ThemeMode } from '@_/graphql/api/generated'
import { LightModeIcon } from '@components/atoms/Icon/LightModeIcon'
import { DarkModeIcon } from '@components/atoms/Icon/DarkModeIcon'
import { Controller, useFormContext, useWatch } from 'react-hook-form'
import { useColorMode } from '@components/ui/color-mode'
import { Flex } from '@chakra-ui/react'
import { useEffect } from 'react'

export const ThemeModeForm = ({
    name = 'themeMode',
    width = 'full',
    onValueChange,
}: {
    name?: string
    width?: string
    onValueChange?: (themeMode: ThemeMode) => void
}) => {
    const { colorMode, setColorMode } = useColorMode()
    const { control } = useFormContext()
    const watchThemeMode = useWatch({ name })
    useEffect(() => {
        if (!watchThemeMode || colorMode === watchThemeMode) {
            return
        }
        setColorMode(watchThemeMode.toLowerCase())
    }, [setColorMode, watchThemeMode])

    return (
        <Controller
            name={name}
            control={control}
            defaultValue={colorMode}
            render={({ field }) => (
                <RadioCardRoot
                    size="lg"
                    defaultValue={field.value || colorMode}
                    width={width}
                    name={field.name}
                    value={field.value}
                    onValueChange={({ value }: any) => {
                        field.onChange(value)
                        if (onValueChange) {
                            onValueChange(value)
                        }
                    }}
                >
                    <Flex gap="4">
                        <ChoiceCard
                            icon={<SystemModeIcon />}
                            label="System"
                            value={ThemeMode.System}
                        />
                        <ChoiceCard
                            icon={<LightModeIcon />}
                            label="Light"
                            value={ThemeMode.Light}
                        />
                        <ChoiceCard
                            icon={<DarkModeIcon />}
                            label="Dark"
                            value={ThemeMode.Dark}
                        />
                    </Flex>
                </RadioCardRoot>
            )}
        />
    )
}
