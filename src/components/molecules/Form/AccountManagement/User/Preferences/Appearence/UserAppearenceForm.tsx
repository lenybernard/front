import React, { useEffect } from 'react'
import { Box, HStack, Stack } from '@chakra-ui/react'
import { FormProvider, useForm } from 'react-hook-form'
import 'react-phone-number-input/style.css'
import 'yup-phone'
import {
    ThemeMode,
    UserAppearenceForm__UserFragment,
} from '@_/graphql/api/generated'
import { UserAppearanceFormValues } from '@components/views/accountManagement/preferences/MyPreferences.types'
import { ThemeModeForm } from '@components/molecules/Form/AccountManagement/User/Preferences/Appearence/ThemeModeForm'
import { Button } from '@components/ui/button'
import { useTranslation } from 'react-i18next'
import { LanguageSwitcher } from '@components/atoms/LanguageSwitcher'
import { GenderForm } from '@components/atoms/Form/GenderForm'
import { Field } from '@components/ui/field'

export const UserAppearenceForm = ({
    onSubmit,
    user,
}: {
    onSubmit: (values: UserAppearanceFormValues) => void
    user?: UserAppearenceForm__UserFragment
}) => {
    const { t, i18n } = useTranslation()
    const methods = useForm<UserAppearanceFormValues>({
        defaultValues: {
            settings: {
                themeMode: user?.settings?.themeMode ?? ThemeMode.System,
            },
            gender: user?.gender,
        },
    })
    useEffect(() => {
        methods.setValue('gender', user?.gender ?? '')
    }, [i18n.language])
    const {
        handleSubmit,
        register,
        formState: { isSubmitting },
    } = methods

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Box>
                    <input
                        type="hidden"
                        {...register('settings.id')}
                        value={user?.settings?.id}
                    />
                </Box>
                <Stack gap={5} direction="column">
                    <ThemeModeForm
                        {...register('settings.themeMode', {
                            value: user?.settings?.themeMode,
                        })}
                    />
                    <LanguageSwitcher
                        onChange={(language) => {
                            i18n.changeLanguage(language)
                            methods.setValue('settings.language', language)
                        }}
                        orientation="vertical"
                    />
                    <Field
                        id="gender"
                        label={t(
                            'accountManagement.my.profile.form.gender.label'
                        )}
                        helperText={t(
                            'accountManagement.my.profile.form.gender.help'
                        )}
                    >
                        <GenderForm
                            required={false}
                            register={register('gender')}
                        />
                    </Field>

                    <HStack>
                        <Button
                            fontFamily="heading"
                            mt={8}
                            loading={isSubmitting}
                            type="submit"
                        >
                            {user && t('global.save')}
                        </Button>
                    </HStack>
                </Stack>
            </form>
        </FormProvider>
    )
}
