import { useEffect, useState } from 'react'
import {
    UserListAvatars_UserFragment,
    useUserListAvatarsQuery,
} from '@_/graphql/api/generated'
import { PageInfo } from '@_/types'

export const useUserList = (id: string, nbToDisplay = 8) => {
    const [afterCursor, setAfterCursor] = useState<string | undefined>(
        undefined
    )
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>(
        undefined
    )
    const [displayedUsers, setDisplayedUsers] = useState<
        (UserListAvatars_UserFragment | undefined)[]
    >([])
    const [allUsers, setAllUsers] = useState<
        (UserListAvatars_UserFragment | undefined)[]
    >([])
    const [pageInfo, setPageInfo] = useState<PageInfo | undefined>(undefined)
    const [currentIndex, setCurrentIndex] = useState(0) // Current index for displayed users
    const nbToFetch = 50

    const variables = {
        id,
        afterCursor,
        beforeCursor,
        first: nbToFetch,
    }

    const { data, loading, refetch } = useUserListAvatarsQuery({
        variables,
        fetchPolicy: 'network-only',
    })

    useEffect(() => {
        if (data?.userList?.users?.edges) {
            const newUsers = data.userList.users.edges.map((edge) => edge)
            setAllUsers(newUsers)
            setDisplayedUsers(newUsers.slice(0, nbToDisplay))
            setPageInfo(data.userList.users.pageInfo)
            setCurrentIndex(0)
        }
    }, [data, nbToDisplay])

    useEffect(() => {
        const end = currentIndex + nbToDisplay
        setDisplayedUsers(allUsers.slice(currentIndex, end))
    }, [currentIndex, allUsers, nbToDisplay])

    const loadNext = () => {
        const nextIndex = currentIndex + 1
        if (
            nextIndex + nbToDisplay > allUsers.length &&
            pageInfo?.hasNextPage
        ) {
            const middleCursor =
                allUsers[Math.floor(allUsers.length / 2)]?.cursor
            setAfterCursor(middleCursor)
            setBeforeCursor(undefined)
            refetch()
        } else {
            setCurrentIndex(nextIndex)
        }
    }

    const loadPrevious = () => {
        const previousIndex = currentIndex - 1
        if (previousIndex >= 0) {
            setCurrentIndex(previousIndex)
        } else if (pageInfo?.hasPreviousPage) {
            const middleCursor =
                allUsers[Math.floor(allUsers.length / 2)]?.cursor
            setBeforeCursor(middleCursor)
            setAfterCursor(undefined)
            refetch()
        }
    }

    const removeUser = (userId: string) => {
        const updatedAllUsers = allUsers.filter(
            (user) => user?.node?.id !== userId
        )
        setAllUsers(updatedAllUsers)
        const end = currentIndex + nbToDisplay
        setDisplayedUsers(updatedAllUsers.slice(currentIndex, end))
    }

    const addUser = (newUser: UserListAvatars_UserFragment) => {
        const updatedAllUsers = [newUser, ...allUsers]
        setAllUsers(updatedAllUsers)
        const end = currentIndex + nbToDisplay
        setDisplayedUsers(updatedAllUsers.slice(currentIndex, end))
        setPageInfo((prevPageInfo) =>
            prevPageInfo
                ? {
                      ...prevPageInfo,
                      hasNextPage: true,
                  }
                : undefined
        )
    }

    useEffect(() => {
        if (afterCursor || beforeCursor) {
            refetch()
        }
    }, [afterCursor, beforeCursor, refetch])

    return {
        displayedUsers,
        loadNext,
        loadPrevious,
        loading,
        allUsers,
        pageInfo,
        currentIndex,
        removeUser,
        addUser,
    }
}
