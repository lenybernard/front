import { AvatarWithIcon } from '@components/molecules/Avatar/AvatarWithIcon'
import { HStack, IconButton, Stack } from '@chakra-ui/react'
import { FaChevronLeft, FaChevronRight, FaTimes } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { AnimatePresence, motion } from 'framer-motion'
import {
    MyListEditViewDocument,
    RemoveUserFromUserListInput,
    useMyListEdit_RemoveFromListMutation,
    UserListAvatarsDocument,
} from '@_/graphql/api/generated'
import { useUserList } from '@components/molecules/Form/AccountManagement/UserList/useUserList'
import { SkeletonCircle } from '@components/ui/skeleton'

export const UserListAvatars = ({ id }: { id: string }) => {
    const { t } = useTranslation()
    const [removeUserFromList] = useMyListEdit_RemoveFromListMutation()
    const nbToDisplay = 8
    const {
        displayedUsers,
        loadNext,
        loadPrevious,
        loading,
        allUsers,
        pageInfo,
        currentIndex,
        removeUser,
    } = useUserList(id, nbToDisplay)
    const variables = {
        id,
        afterCursor: undefined,
        beforeCursor: undefined,
        first: 50,
    }
    const onRemove = (input: RemoveUserFromUserListInput) => {
        removeUserFromList({
            variables: {
                input,
            },
            refetchQueries: [
                {
                    query: MyListEditViewDocument,
                    variables,
                },
                {
                    query: UserListAvatarsDocument,
                    variables,
                },
            ],
            onCompleted: () => {
                removeUser(input.userId)
            },
        })
    }

    if (loading) {
        return (
            <HStack>
                {[...Array(nbToDisplay)].map((_, i) => (
                    <SkeletonCircle key={i} size={'48px'} />
                ))}
            </HStack>
        )
    }

    if (displayedUsers.length > 0) {
        return (
            <HStack gap={2}>
                <IconButton
                    aria-label={t('global.previous')}
                    onClick={loadPrevious}
                    disabled={currentIndex === 0 && !pageInfo?.hasPreviousPage}
                >
                    <FaChevronLeft />
                </IconButton>
                <AnimatePresence initial={false}>
                    {displayedUsers.map((edge) => {
                        const user = edge?.node!
                        return (
                            <motion.div
                                key={user.id}
                                initial={{ opacity: 0, x: -50 }}
                                animate={{ opacity: 1, x: 0 }}
                                exit={{ opacity: 0, x: 50 }}
                            >
                                <Stack direction="row" gap={4}>
                                    <AvatarWithIcon
                                        name={user.fullname}
                                        src={user.avatar?.contentUrl}
                                        onClick={() =>
                                            onRemove({ id, userId: user.id })
                                        }
                                        icon={<FaTimes size={'0.8em'} />}
                                        color="red.400"
                                        hoverColor="red.600"
                                    />
                                </Stack>
                            </motion.div>
                        )
                    })}
                </AnimatePresence>
                <IconButton
                    aria-label={t('global.next')}
                    onClick={loadNext}
                    disabled={
                        !pageInfo?.hasNextPage &&
                        currentIndex + displayedUsers.length >= allUsers.length
                    }
                >
                    <FaChevronRight />
                </IconButton>
            </HStack>
        )
    }
    return <></>
}
