import React, { useCallback } from 'react'
import { useFormContext } from 'react-hook-form'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { useAuth } from '@_/auth/useAuth'
import {
    AddressChangedEvent,
    AddressSearch,
} from '../../../../atoms/Form/AddressSearch'

export const CircleLocationForm = () => {
    const { setValue } = useFormContext()
    const { community } = useCircle()
    const auth = useAuth()

    const handleAddressChanged = useCallback(
        (event: AddressChangedEvent) => {
            const { coordinates, address, countryCode, city, postalCode } =
                event
            if (!coordinates?.lat || !coordinates?.lng) {
                throw new Error('coordinates are required')
            }
            setValue('address', address)
            setValue('postalCode', postalCode)
            setValue('city', city)
            setValue('country', countryCode)
            setValue('latitude', `${coordinates?.lat}`)
            setValue('longitude', `${coordinates?.lng}`)
        },
        [setValue]
    )

    return (
        <>
            <AddressSearch
                key={`circle-address-search-location`}
                onAddressChanged={handleAddressChanged}
                defaultValue={{
                    address: community?.address,
                    latitude: (
                        community?.latitude || auth?.user?.latitude
                    )?.toString(),
                    longitude: (
                        community?.longitude || auth?.user?.longitude
                    )?.toString(),
                }}
            />
        </>
    )
}
