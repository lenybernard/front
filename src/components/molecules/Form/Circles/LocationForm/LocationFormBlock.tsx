import { Button } from '@components/ui/button'
import { CircleLocationFormData } from '@components/molecules/Form/Circles/CircleForm/types'
import { FormProvider, useForm } from 'react-hook-form'
import { FaSave } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { useUpdateCircleMutation } from '@_/graphql/api/generated'
import { toast } from 'react-toastify'
import { Box, VStack } from '@chakra-ui/react'
import { useState } from 'react'
import { CircleLocationForm } from './LocationForm'

export const LocationFormBlock = () => {
    const { t } = useTranslation()
    const { community, setCommunity } = useCircle()
    const methods = useForm<CircleLocationFormData>()
    const [showMap, setShowMap] = useState(false)
    const [updateCircle] = useUpdateCircleMutation()

    const onSubmit = async (values: CircleLocationFormData) => {
        if (community) {
            updateCircle({
                variables: {
                    input: {
                        id: community.id,
                        ...values,
                    },
                },
            }).then((r) => {
                if (r.data?.updateCircle?.circle?.slug) {
                    toast.success(t('pooling.edit.toast.success'))
                    setCommunity(r.data?.updateCircle?.circle)
                    setShowMap(false)
                }

                return

                toast.error(t('circles.edit.toast.error'))
            })
        }
    }

    return (
        <FormProvider {...methods}>
            <Box asChild w="full">
                <VStack alignItems="flex-start">
                    {(showMap && (
                        <form
                            onSubmit={methods.handleSubmit(onSubmit)}
                            style={{ width: '100%' }}
                        >
                            <CircleLocationForm />
                            <Button type="submit" mt={5}>
                                <FaSave />
                                {t('global.save')}
                            </Button>
                        </form>
                    )) || (
                        <>
                            <Box fontWeight={'bold'}>{community?.address}</Box>
                            <Button
                                type={'button'}
                                mt={2}
                                variant="outline"
                                onClick={() => setShowMap(true)}
                            >
                                {t('communities.form.location.change')}
                            </Button>
                        </>
                    )}
                </VStack>
            </Box>
        </FormProvider>
    )
}
