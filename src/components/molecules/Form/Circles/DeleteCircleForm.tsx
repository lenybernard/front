import React from 'react'
import { useTranslation } from 'react-i18next'
import rehypeRaw from 'rehype-raw'
import ReactMarkdown from 'react-markdown'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTrigger,
} from '@components/ui/dialog'
import { useDeleteCircleMutation } from '@_/graphql/api/generated'
import { useNavigate } from 'react-router'
import { toast } from 'react-toastify'
import { useColorModeValue } from '@components/ui/color-mode'
import { H3 } from '@components/atoms/Heading'
import { EmphasisText } from '../../../atoms/Text/EmphasisText'
import { DangerButton } from '../../../atoms/Button'

export const DeleteCircleForm = ({
    id,
    name,
}: {
    id: string
    name: string
}) => {
    const navigate = useNavigate()
    const { t } = useTranslation()
    const [deleteCircle] = useDeleteCircleMutation()
    const onDelete = () => {
        deleteCircle({
            variables: {
                input: {
                    id,
                },
            },
        })
        toast.success(t('communities.form.toast.delete.success'))
        navigate('/')
    }

    return (
        <>
            <DialogRoot placement={'center'}>
                <DialogBackdrop />
                <DialogTrigger asChild>
                    <DangerButton fontFamily="heading">
                        {t('global.delete')}
                    </DangerButton>
                </DialogTrigger>
                <DialogContent bg={useColorModeValue('red.300', 'red.800')}>
                    <DialogHeader>
                        <H3>{t('communities.delete.modal.noRegret')}</H3>
                    </DialogHeader>
                    <DialogCloseTrigger />
                    <DialogBody>
                        <EmphasisText color="red.400">
                            {t('communities.delete.modal.warning', {
                                circleName: name,
                            })}
                        </EmphasisText>
                        <br />
                        <br />
                        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                            {t('communities.delete.modal.pleaseFeedback', {
                                email: process.env.REACT_APP_CONTACT_EMAIL,
                            })}
                        </ReactMarkdown>
                    </DialogBody>
                    <DialogFooter>
                        <DangerButton onClick={() => onDelete()}>
                            {t('communities.delete.modal.confirm')}
                        </DangerButton>
                    </DialogFooter>
                </DialogContent>
            </DialogRoot>
        </>
    )
}
