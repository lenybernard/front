import React from 'react'
import { useFormContext, UseFormRegisterReturn } from 'react-hook-form'
import { UserForm__UserAvatarFragment } from '@_/graphql/api/generated'
import { Field } from '@components/ui/field'
import { useTranslation } from 'react-i18next'
import { fileToBase64 } from '@utils/image'
import { CircleMediaFormData } from '@components/molecules/Form/Circles/CircleForm/types'
import Compressor from 'compressorjs'
import { ImageForm } from '../../../../atoms/Form/ImageForm'

type CircleMediaFormProps = {
    logo?: UserForm__UserAvatarFragment
    cover?: UserForm__UserAvatarFragment
    register?: UseFormRegisterReturn
}

export const CircleMediaForm = ({ logo, cover }: CircleMediaFormProps) => {
    const { t } = useTranslation()
    const {
        setValue,
        formState: { errors },
    } = useFormContext<CircleMediaFormData>()
    const [logoPreview, setLogoPreview] = React.useState(logo?.contentUrl)
    const [coverPreview, setCoverPreview] = React.useState(cover?.contentUrl)

    const handleLogoChange = (file: File) => {
        /* eslint-disable no-new */
        new Compressor(file, {
            quality: 0.8,
            maxWidth: 500,
            maxHeight: 500,
            success(compressedFile: File) {
                fileToBase64(compressedFile).then((logoFile) => {
                    setValue('logo', logoFile)
                    setValue('removeLogo', false)
                    setLogoPreview(logoFile.base64)
                })
            },
        })
    }

    const handleLogoDelete = () => {
        setValue('logo', null)
        setValue('removeLogo', true)
        setLogoPreview(undefined)
    }

    const handleCoverChange = (file: File) => {
        /* eslint-disable no-new */
        new Compressor(file, {
            quality: 0.8,
            maxWidth: 1024,
            maxHeight: 768,
            success(compressedFile: File) {
                fileToBase64(compressedFile).then((coverFile) => {
                    setValue('cover', coverFile)
                    setValue('removeCover', false)
                    setCoverPreview(coverFile.base64)
                })
            },
        })
    }

    const handleCoverDelete = () => {
        setValue('cover', null)
        setValue('removeCover', true)
        setCoverPreview(undefined)
    }

    return (
        <>
            <Field
                label={<>{t('communities.form.logo.label')}</>}
                errorText={errors.logo?.message}
            >
                <ImageForm
                    preview={logoPreview}
                    avatarProps={{}}
                    onFileChange={handleLogoChange}
                    onDelete={handleLogoDelete}
                />
            </Field>
            <Field
                label={<>{t('communities.form.cover.label')}</>}
                errorText={errors.cover?.message}
            >
                <ImageForm
                    preview={coverPreview}
                    avatarProps={{}}
                    onFileChange={handleCoverChange}
                    onDelete={handleCoverDelete}
                />
            </Field>
        </>
    )
}
