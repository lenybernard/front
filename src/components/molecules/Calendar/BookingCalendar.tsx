import React, { Dispatch, SetStateAction } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import listPlugin from '@fullcalendar/list'
import { EventInput } from '@fullcalendar/core'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { Box } from '@chakra-ui/react'
import { LazyQueryExecFunction } from '@apollo/client'
import {
    Exact,
    MaterialBookingCalendarQuery,
    MaterialCalendar__MaterialBookingFragment,
    UnavailableMaterialBookingMutationFn,
} from '@_/graphql/api/generated'
import './calendar.scss'

export type CalendarEventInfo = {
    event: {
        groupId: string
        start: Date | null
        end: Date | null
    }
    revert: () => void
}

type CalendarProps = {
    events: EventInput[]
    handleEventMove: (eventInfo: CalendarEventInfo) => void
    eventTitle: ({
        userId,
        userName,
        datePeriod,
        price,
        comment,
    }: Record<string, unknown>) => string
    queryBookingLazyQuery: LazyQueryExecFunction<
        MaterialBookingCalendarQuery,
        Exact<{ id: string }>
    >
    unavailable: UnavailableMaterialBookingMutationFn
    materialId: string
    setSelectedBooking: Dispatch<
        SetStateAction<MaterialCalendar__MaterialBookingFragment | undefined>
    >
}

export const BookingCalendar: React.FC<CalendarProps> = ({
    events,
    handleEventMove,
    eventTitle,
    queryBookingLazyQuery,
    unavailable,
    materialId,
    setSelectedBooking,
}) => {
    const { t, i18n } = useTranslation()
    return (
        <Box my={5}>
            <FullCalendar
                plugins={[dayGridPlugin, interactionPlugin, listPlugin]}
                initialView="dayGridMonth"
                eventClick={async ({ event }) => {
                    const { loading: loadingBooking, data: dataBooking } =
                        await queryBookingLazyQuery({
                            variables: {
                                id: event._def.groupId,
                            },
                        })

                    if (!loadingBooking && dataBooking?.materialBooking) {
                        setSelectedBooking(dataBooking.materialBooking)
                        event._def.extendedProps.modal()
                    }
                }}
                dateClick={async ({ dateStr }) => {
                    await unavailable({
                        variables: {
                            materialId,
                            startDate: dateStr,
                            endDate: moment(dateStr)
                                .add(1, 'd')
                                .format('Y/MM/DD'),
                        },
                    })
                }}
                locale={i18n.language}
                firstDay={parseInt(t('global.calendar.firstDay'), 10)}
                headerToolbar={{
                    right: 'prev,next dayGridMonth,listMonth',
                    left: 'title',
                }}
                editable
                eventResize={(eventInfo) => {
                    handleEventMove(eventInfo)
                }}
                eventDrop={(eventInfo) => {
                    handleEventMove(eventInfo)
                }}
                events={events}
                eventContent={(eventInfo) => {
                    return {
                        html: `<div style="background-color: padding: 8px">${eventTitle(
                            eventInfo.event._def.extendedProps
                        )}</div>`,
                    }
                }}
                titleFormat={{
                    year: 'numeric',
                    month: 'long',
                }}
                buttonText={{
                    today: t('global.calendar.today'),
                    month: t('global.calendar.month'),
                    list: t('global.calendar.list'),
                }}
                views={{
                    dayGridMonth: {
                        titleFormat: {
                            year: 'numeric',
                            month: 'long',
                        },
                        eventTimeFormat: {
                            hour: '2-digit',
                            minute: '2-digit',
                        },
                    },
                }}
            />
        </Box>
    )
}
