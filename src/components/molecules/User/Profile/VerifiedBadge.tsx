import { Badge } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { LuCheckCheck } from 'react-icons/lu'

export const VerifiedBadge = ({ user }: { user: { gender?: string } }) => {
    const { t } = useTranslation()

    return (
        <Badge
            size="md"
            variant="solid"
            colorPalette="yellow"
            ms="2"
            verticalAlign="middle"
        >
            <LuCheckCheck />{' '}
            {t('user.profile.verified', { context: user.gender })}
        </Badge>
    )
}
