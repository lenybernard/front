import {
    Badge,
    Flex,
    Float,
    Heading,
    HStack,
    Span,
    Stack,
    Text,
    VStack,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { VerifiedBadge } from '@components/molecules/User/Profile/VerifiedBadge'
import { Button } from '@components/ui/button'
import { LuUserPlus } from 'react-icons/lu'
import { Avatar } from '@components/ui/avatar'
import { getAge } from '@utils/getAge'
import { ProfileUserCoverFragment } from '@_/graphql/api/generated'
import { ContactUserModal } from '@components/molecules/Modal/Pooling/Material/ContactUserModal'

export const ProfileUserCover = ({
    user,
    w = '5xl',
}: {
    user: Omit<ProfileUserCoverFragment, 'firstname' | 'lastname'>
    w?: string
}) => {
    const isSocialEnabled = false
    const { t } = useTranslation()
    const age = user.birthDate ? getAge(user.birthDate) : null

    return (
        <VStack gap="6" pt="50px">
            <Flex
                gap="8"
                direction={{ base: 'column-reverse', md: 'row' }}
                w={w}
            >
                <Stack flex="1" gap="4">
                    <Stack gap="1">
                        <Heading size="2xl">
                            {user.fullname}
                            {(user.circleMemberships?.totalCount ?? 0) > 0 && (
                                <VerifiedBadge user={user} />
                            )}
                        </Heading>
                        <HStack color="fg.muted">
                            {!['', undefined].includes(user.gender) && (
                                <Span>
                                    {t('user.profile.genderPronom', {
                                        context: user.gender,
                                    })}{' '}
                                    •
                                </Span>
                            )}{' '}
                            {age && (
                                <Span>{t('user.profile.age', { age })} • </Span>
                            )}{' '}
                            {user.city && (
                                <Span>
                                    {user.city} {user.country}
                                </Span>
                            )}
                        </HStack>
                    </Stack>

                    {user.bio && (
                        <Text whiteSpace={'pre-wrap'}>{user.bio}</Text>
                    )}

                    {isSocialEnabled && (
                        <>
                            <HStack mt="2" gap="4">
                                <HStack gap="1">
                                    <Span fontWeight="semibold">0</Span>
                                    <Span color="fg.muted">followers</Span>
                                </HStack>
                                <HStack gap="1">
                                    <Span fontWeight="semibold">0</Span>
                                    <Span color="fg.muted">following</Span>
                                </HStack>
                            </HStack>
                        </>
                    )}
                    <>
                        <Flex mt="2" gap={5}>
                            <Button>
                                <LuUserPlus /> {t('social.follow.button.label')}
                                <Float placement="top-end">
                                    <Badge
                                        size="sm"
                                        variant="solid"
                                        colorPalette="green"
                                    >
                                        {t('global.soon')}
                                    </Badge>
                                </Float>
                            </Button>
                            <ContactUserModal user={user} />
                        </Flex>
                    </>
                </Stack>

                <Avatar
                    name={user.fullname}
                    borderWidth="1px"
                    borderColor="blackAlpha.50"
                    src={user.avatar?.contentUrl}
                    css={{
                        '--avatar-size': 'sizes.32',
                        '--avatar-font-size': 'fontSizes.3xl',
                    }}
                />
            </Flex>
        </VStack>
    )
}
