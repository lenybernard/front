import { Group } from '@chakra-ui/react'
import { Button, ButtonProps } from '@components/ui/button'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'

type ItemNav = ButtonProps

export const PrevNextNav = ({
    prev,
    next,
}: {
    prev?: ItemNav
    next?: ItemNav
}) => {
    const baseButtonProps: ButtonProps = {
        size: { base: 'lg', md: 'xl' },
        alignSelf: { base: 'stretch', sm: 'start' },
    }

    return (
        <Group attached>
            {(prev && (
                <Button {...baseButtonProps} {...prev}>
                    <FaChevronLeft />
                </Button>
            )) || (
                <Button {...baseButtonProps} disabled={true}>
                    <FaChevronLeft />
                </Button>
            )}
            {(next && (
                <Button {...baseButtonProps} {...next}>
                    <FaChevronRight />
                </Button>
            )) || (
                <Button {...baseButtonProps} disabled={true}>
                    <FaChevronRight />
                </Button>
            )}
        </Group>
    )
}
