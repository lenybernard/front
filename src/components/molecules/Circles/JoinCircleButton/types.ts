import { ButtonProps } from '@components/ui/button'
import { JoinCircleButton__CircleFragment } from '@_/graphql/api/generated'

export type Props = {
    circle: JoinCircleButton__CircleFragment
} & ButtonProps
