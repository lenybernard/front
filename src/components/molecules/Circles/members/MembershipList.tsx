import React, { useState } from 'react'
import { Box, SimpleGrid } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useMembershipListQuery } from '@_/graphql/api/generated'
import { CommunityMemberCard } from '@components/molecules/Cards/UserCard/CommunityMemberCard'
import { Pager } from '../../Pager/Pager'
import { Loader } from '../../../atoms/Loader/Loader'
import { H2 } from '../../../atoms/Heading'
import { EmptyList } from '../../../atoms/List/EmptyList'

export const MembershipList = ({
    circleSlug,
    statuses,
    nbToFetch,
    title,
    order = [{ createdAt: 'ASC' }],
}: {
    circleSlug: string
    title: string
    statuses: string[]
    nbToFetch?: number
    order?: [
        {
            createdAt?: 'ASC' | 'DESC'
            permission?: 'ASC' | 'DESC'
            user_firstname?: 'ASC' | 'DESC'
            user_lastname?: 'ASC' | 'DESC'
        },
    ]
}) => {
    const [afterCursor, setAfterCursor] = useState<string | undefined>()
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>()
    const { t } = useTranslation()
    const { loading, data } = useMembershipListQuery({
        variables: {
            slug: circleSlug,
            statuses,
            beforeCursor,
            afterCursor,
            nbToFetch,
            order,
        },
    })

    if (loading) {
        return <Loader />
    }

    const connection = data?.circleMemberships

    if (!connection || !connection.edges) {
        return <div>Error</div>
    }

    const { edges: memberships, totalCount, pageInfo } = connection

    return (
        <>
            <H2>
                {t(title, {
                    count: totalCount || 0,
                })}
            </H2>
            <SimpleGrid
                columns={{ base: 1, sm: 2, md: 3, xl: 4 }}
                gap={3}
                mt={6}
            >
                {memberships.map(({ node: circleMembership }) => {
                    if (!circleMembership) return <div key={'none'} />

                    return (
                        <CommunityMemberCard
                            key={circleMembership?.user.id}
                            p={5}
                            member={circleMembership}
                        />
                    )
                })}
            </SimpleGrid>
            {totalCount === 0 && (
                <EmptyList>
                    <Box>{t('communities.show.users.empty')} 😵‍💫</Box>
                </EmptyList>
            )}
            {pageInfo && (
                <Pager
                    pageInfo={pageInfo}
                    loading={loading}
                    setBeforeCursor={setBeforeCursor}
                    setAfterCursor={setAfterCursor}
                />
            )}
        </>
    )
}
