import { useApolloClient } from '@apollo/client'
import { Button } from '@components/ui/button'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import {
    ChangeMembershipStatusMembershipFragment,
    useChangeMembershipStatusButtonMutation,
} from '@_/graphql/api/generated'

type Props = {
    circleMembership: ChangeMembershipStatusMembershipFragment
    action: string
}

export const ChangeMembershipStatusButton = ({
    circleMembership,
    action,
}: Props) => {
    const { t } = useTranslation()
    const client = useApolloClient()
    const [changeStatus] = useChangeMembershipStatusButtonMutation({
        update() {
            client.resetStore()
        },
    })
    const onAction = (id: string, transition: string) => {
        const variables = { id, transition }
        changeStatus({
            variables,
        }).then((r) => {
            const membership =
                r.data?.changeStatusCircleMembership?.circleMembership
            if (membership === null) {
                toast.success(
                    t(`communities.show.users.updateStatus.toast_${action}`, {
                        context: 'error',
                    })
                )
                return
            }
            toast.success(
                t(`communities.show.users.updateStatus.toast_${action}`, {
                    context: 'success',
                    name: membership?.user.firstname,
                    email: membership?.user.email,
                })
            )
        })
    }

    return (
        <Button
            colorPalette={['accept'].includes(action) ? 'green' : 'red'}
            key={`action-${action}`}
            onClick={() => onAction(circleMembership.id, action)}
        >
            {t('communities.show.users.actions', {
                context: action,
            })}
        </Button>
    )
}
