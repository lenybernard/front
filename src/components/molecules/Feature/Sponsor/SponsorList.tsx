import { Box, Flex, Heading, Image, Link, Text } from '@chakra-ui/react'
import React, { ReactNode } from 'react'
import { EmphasisText } from '../../../atoms/Text/EmphasisText'

type Props = {
    title: string
    subtitle: string
    sponsors?: {
        name: string
        logo: {
            src: string
            alt: string
            height?: string
            width?: string
        }
        url: string
    }[]
    rightIcon?: ReactNode
    leftIcon?: ReactNode
    bgColor?: string
    color?: string
    emphasisColor?: string
}
export const SponsorList = ({
    title,
    subtitle,
    leftIcon,
    rightIcon,
    sponsors,
    bgColor = 'blue.600',
    color = 'white',
    emphasisColor = 'yellow.400',
}: Props) => {
    return (
        <Box bg={bgColor} color={color} pt="20" pb="20">
            <Box
                maxW={{ base: 'xl', md: '5xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
            >
                <Box textAlign="center">
                    <Heading size="lg" mb="4">
                        <Flex gap={5} justifyContent="center">
                            {leftIcon}
                            <EmphasisText color="pink.400">
                                {title}
                            </EmphasisText>
                            {rightIcon}
                        </Flex>
                    </Heading>
                    <Text maxW="2xl" mx="auto">
                        <EmphasisText color={emphasisColor}>
                            {subtitle}
                        </EmphasisText>
                    </Text>
                </Box>
                <Flex
                    mt="12"
                    gap={50}
                    alignItems="center"
                    justifyContent="center"
                    fontSize="2xl"
                    opacity={0.6}
                    color={'white'}
                >
                    {sponsors &&
                        sponsors.map(({ logo, url, name }) => (
                            <Link
                                key={name}
                                href={url}
                                target="_blank"
                                title={name}
                            >
                                <Image {...logo} maxH={50} />
                            </Link>
                        ))}
                </Flex>
            </Box>
        </Box>
    )
}
