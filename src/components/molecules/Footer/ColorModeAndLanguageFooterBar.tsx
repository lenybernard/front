import { Flex } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { ColorModeSwitcherButton } from '@components/atoms/Button/ColorModeSwitcherButton'
import { LanguageSwitcher } from '@components/atoms/LanguageSwitcher'

export const ColorModeAndLanguageFooterBar = () => {
    return (
        <Flex
            alignItems="center"
            justifyContent="center"
            gap="2"
            py="2"
            color={'white'}
            bg={useColorModeValue('gray.900', 'gray.950')}
        >
            <ColorModeSwitcherButton fontWeight="normal" />
            <LanguageSwitcher />
        </Flex>
    )
}
