import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { FaDiscord, FaFacebook, FaGitlab, FaLinkedin } from 'react-icons/fa'
import { StaticPagesRoutes } from '@_/routes/_hooks/routes.enum'

export interface LinkGroup {
    title: string
    links: Array<{
        label: string
        href: string
        badge?: React.ReactElement
    }>
}
export interface SocialLink {
    label: string
    icon: React.ReactElement
    href: string
}

export type FooterLink = {
    label: string
    href: string
}

export const useLinks = (): LinkGroup[] => {
    const { t } = useTranslation()
    return [
        {
            title: t('layout.footer.links.aboutUs.title'),
            links: [
                {
                    label: t('layout.footer.links.aboutUs.team'),
                    href: StaticPagesRoutes.Team,
                },
                /* {
                    label: t('layout.footer.links.aboutUs.careers.label'),
                    href: '#',
                    badge: (
                        <Box className={'light'}>
                            <Badge colorPalette="blue" fontSize="0.625rem">
                                {t(
                                    'layout.footer.links.aboutUs.careers.notHiring'
                                )}
                            </Badge>
                        </Box>
                    ),
                 }, */
                { label: t('layout.footer.links.aboutUs.press'), href: '#' },
                { label: t('layout.footer.links.aboutUs.faq'), href: '#' },
            ],
        },
        {
            title: t('layout.footer.links.product.title'),
            links: [
                {
                    label: t('layout.footer.links.product.howItWorks'),
                    href: '#',
                },
                {
                    label: t('layout.footer.links.product.pricing'),
                    href: StaticPagesRoutes.Pricing,
                },
                {
                    label: t('layout.footer.links.product.contribute'),
                    href: StaticPagesRoutes.Contribute,
                },
            ],
        },
        {
            title: t('layout.footer.links.resources.title'),
            links: [
                { label: t('layout.footer.links.resources.blog'), href: '#' },
                {
                    label: t('layout.footer.links.resources.sponsors'),
                    href: '#',
                },
                { label: t('layout.footer.links.resources.help'), href: '#' },
            ],
        },
        {
            title: t('layout.footer.links.contact.title'),
            links: [
                {
                    label: t('layout.footer.links.contact.form'),
                    href: StaticPagesRoutes.Contact,
                },
                ...(process.env.REACT_APP_DISCORD_INVITATION_LINK
                    ? [
                          {
                              label: t('layout.footer.links.contact.discord'),
                              href: process.env
                                  .REACT_APP_DISCORD_INVITATION_LINK,
                          },
                      ]
                    : []),
                ...(process.env.REACT_APP_CONTACT_EMAIL
                    ? [
                          {
                              label: process.env.REACT_APP_CONTACT_EMAIL,
                              href: `mailto:${process.env.REACT_APP_CONTACT_EMAIL}`,
                          },
                      ]
                    : []),
            ],
        },
    ]
}

export const useSocialLinks = (): SocialLink[] => {
    const { t } = useTranslation()
    return [
        ...(process.env.REACT_APP_DISCORD_INVITATION_LINK
            ? [
                  {
                      label: t('layout.footer.links.social.discord'),
                      icon: <FaDiscord />,
                      href: process.env.REACT_APP_DISCORD_INVITATION_LINK,
                  },
              ]
            : []),
        ...(process.env.REACT_APP_FACEBOOK_LINK
            ? [
                  {
                      label: t('layout.footer.links.social.facebook'),
                      icon: <FaFacebook />,
                      href: process.env.REACT_APP_FACEBOOK_LINK,
                  },
              ]
            : []),
        ...(process.env.REACT_APP_LINKEDIN_LINK
            ? [
                  {
                      label: t('layout.footer.links.social.linkedin'),
                      icon: <FaLinkedin />,
                      href: process.env.REACT_APP_LINKEDIN_LINK,
                  },
              ]
            : []),
        ...(process.env.REACT_APP_GITLAB_LINK
            ? [
                  {
                      label: t('layout.footer.links.social.linkedin'),
                      icon: <FaGitlab />,
                      href: process.env.REACT_APP_GITLAB_LINK,
                  },
              ]
            : []),
    ]
}

export const useExtraFooterLinks = (): FooterLink[] => {
    return []
}
