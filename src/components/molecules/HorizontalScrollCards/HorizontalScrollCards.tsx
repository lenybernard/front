import React from 'react'
import { Box, Flex, Image, Text } from '@chakra-ui/react'

interface CardData {
    id: number
    title: string
    description: string
    imageUrl: string
}

interface HorizontalScrollCardsProps {
    cards: CardData[]
}

export const HorizontalScrollCards: React.FC<HorizontalScrollCardsProps> = ({
    cards,
}) => {
    return (
        <Flex overflowX="scroll" p={4} gap={4}>
            {cards.map((card) => (
                <Box
                    key={card.id}
                    minWidth="300px"
                    borderWidth="1px"
                    borderRadius="lg"
                    overflow="hidden"
                >
                    <Image src={card.imageUrl} alt={`Image de ${card.title}`} />
                    <Box p={5}>
                        <Text fontSize="xl" fontWeight="bold">
                            {card.title}
                        </Text>
                        <Text mt={2}>{card.description}</Text>
                    </Box>
                </Box>
            ))}
        </Flex>
    )
}
