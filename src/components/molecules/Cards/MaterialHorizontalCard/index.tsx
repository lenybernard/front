import { Center, Flex, Heading, Image, Stack, Text } from '@chakra-ui/react'
import { FaChevronRight } from 'react-icons/fa'
import { Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { materialmainPicture } from '@utils/image'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar } from '@components/ui/avatar'
import { Button } from '@components/ui/button'
import { MaterialHorizontalCardProps } from './types'

export const MaterialHorizontalCard = ({
    material,
}: MaterialHorizontalCardProps) => {
    const { t } = useTranslation()
    const { name, brand, model, mainOwnershipUser, mainOwnershipCircle, slug } =
        material
    let ownerInfo
    if (mainOwnershipUser) {
        ownerInfo = {
            name: `${mainOwnershipUser.fullname}`,
            title: `${mainOwnershipUser.firstname}`,
            picture: mainOwnershipUser.avatar?.contentUrl,
            city: mainOwnershipUser.city,
        }
    }
    if (mainOwnershipCircle) {
        ownerInfo = {
            name: `${mainOwnershipCircle.name}`,
            title: `${mainOwnershipCircle.name}`,
            picture: mainOwnershipCircle.logo?.contentUrl,
            city: mainOwnershipCircle.city,
        }
    }

    return (
        <Center>
            <Stack
                borderWidth="1px"
                borderTopRadius="lg"
                w={{ sm: '100%', md: '540px' }}
                height={{ sm: '476px', md: '20rem' }}
                direction={{ base: 'column', md: 'row' }}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow="2xl"
                padding={4}
            >
                <Flex flex={1} bg="blue.200">
                    <Image
                        objectFit="cover"
                        boxSize="100%"
                        src={materialmainPicture(material)}
                    />
                </Flex>
                <Stack
                    flex={1}
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                    p={1}
                    pt={2}
                >
                    <Heading fontSize="2xl" fontFamily="body">
                        {name}
                    </Heading>
                    <Text
                        fontWeight={600}
                        color="gray.500"
                        textStyle="sm"
                        mb={4}
                    >
                        {brand} - {model}
                    </Text>
                    {ownerInfo && (
                        <>
                            <Flex alignItems="center" gap={3}>
                                <Avatar
                                    src={`${ownerInfo.picture}`}
                                    title={ownerInfo.title}
                                />
                                <Heading fontSize="2xl" fontFamily="body">
                                    {ownerInfo.title}
                                </Heading>
                            </Flex>
                            <Text>{/* add here material localization */}</Text>
                        </>
                    )}

                    <Stack direction="row" align="center">
                        <Link to={`/pooling/${slug}`}>
                            <Button
                                rounded="full"
                                px={6}
                                w="full"
                                mt={8}
                                colorPalette={useColorModeValue(
                                    'red',
                                    'yellow'
                                )}
                                textTransform="uppercase"
                            >
                                {t('global.see')}
                                <FaChevronRight />
                            </Button>
                        </Link>
                    </Stack>
                </Stack>
            </Stack>
        </Center>
    )
}
