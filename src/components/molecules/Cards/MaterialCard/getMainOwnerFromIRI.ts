import { client } from '@_/apollo/main/client'
import {
    ShowMaterial_MainOwnerCircleDocument,
    ShowMaterial_MainOwnerCircleQuery,
    ShowMaterial_MainOwnerCircleQueryVariables,
    ShowMaterial_MainOwnerUserDocument,
    ShowMaterial_MainOwnerUserQuery,
    ShowMaterial_MainOwnerUserQueryVariables,
} from '@_/graphql/api/generated'

export const getMainOwnerFromId = (id: string) => {
    if (id?.startsWith('/users/')) {
        return client
            .query<
                ShowMaterial_MainOwnerUserQuery,
                ShowMaterial_MainOwnerUserQueryVariables
            >({
                query: ShowMaterial_MainOwnerUserDocument,
                variables: { id },
            })
            .then((res) => {
                return res.data?.user
            })
    }
    if (id?.startsWith('/circles/')) {
        return client
            .query<
                ShowMaterial_MainOwnerCircleQuery,
                ShowMaterial_MainOwnerCircleQueryVariables
            >({
                query: ShowMaterial_MainOwnerCircleDocument,
                variables: { id },
            })
            .then((res) => {
                return res.data?.circle
            })
    }
    throw new Error('id must starts with /users/ or /cirlces/')
}
