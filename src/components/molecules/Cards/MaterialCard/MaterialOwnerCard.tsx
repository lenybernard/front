import React, { ReactNode, useEffect, useState } from 'react'
import { CommunityCard } from '@components/molecules/Cards/CommunityCard/CommunityCard'
import { UserCard } from '../UserCard/UserCard'
import { isMainOwnerCircle, isMainOwnerUser } from './types'
import { getMainOwnerFromId } from './getMainOwnerFromIRI'

type Props = {
    mainOwnerId: string
    borderRadius?: string
}

export const MaterialOwnerCard = ({ mainOwnerId, borderRadius }: Props) => {
    const [card, setCard] = useState<ReactNode>()

    useEffect(() => {
        getMainOwnerFromId(mainOwnerId).then((mainOwner) => {
            if (mainOwner && isMainOwnerUser(mainOwner)) {
                setCard(<UserCard user={mainOwner} />)
            }
            if (mainOwner && isMainOwnerCircle(mainOwner)) {
                setCard(<CommunityCard circle={mainOwner} />)
            }
        })
    }, [mainOwnerId])

    return card
}
