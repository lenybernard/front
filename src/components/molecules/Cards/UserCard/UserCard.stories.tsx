import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { UserCard } from '@components/molecules/Cards/UserCard/UserCard'
import { CirclePermissionType } from '@_/graphql/api/generated'

const meta: Meta<typeof UserCard> = {
    title: 'molecules/Card',
    component: UserCard,
    args: {
        user: {
            averageRatingScore: 3.7,
            email: 'john.doe@mail.com',
            city: 'Roma',
            slug: 'john-doe',
            firstname: 'John',
            lastname: 'Doe',
            fullname: 'John Doe',
            roles: [],
            circleMemberships: {
                totalCount: 1,
                edges: [
                    {
                        node: {
                            id: '1',
                            circle: {
                                name: 'Roma Soccer team',
                                slug: 'roma-soccer-team',
                                logo: undefined,
                            },
                            permission: CirclePermissionType.Member,
                        },
                    },
                ],
            },
            ratings: { totalCount: 42 },
            userOwnerships: { totalCount: 7 },
            avatar: undefined,
        },
    },
    decorators: [
        (Story) => (
            <Box p={100}>
                <Story />
            </Box>
        ),
    ],
}
export default meta
type Story = StoryObj<typeof UserCard>

export const UserCardDefault: Story = {}
