import { ReactNode } from 'react'
import {
    AvatarFan__UserFragment,
    UserCard__UserFragment,
} from '@_/graphql/api/generated'
import { CardRootProps } from '@chakra-ui/react'

export type Props = {
    user: UserCard__UserFragment
    otherUsers?: AvatarFan__UserFragment[]
    permission?: string
    borderRadius?: number
    actions?: ReactNode | undefined
} & CardRootProps
