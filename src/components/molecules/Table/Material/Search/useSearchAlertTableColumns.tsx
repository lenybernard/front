import React, { ReactNode } from 'react'
import { ColumnDef, createColumnHelper } from '@tanstack/react-table'
import { useTranslation } from 'react-i18next'

export type SearchAlertRow = {
    id?: string
    name?: string | null
    actions?: ReactNode
}

export const useSearchAlertTableColumns = (): Array<
    ColumnDef<SearchAlertRow>
> => {
    const { t } = useTranslation()
    const columnHelper = createColumnHelper<SearchAlertRow>()

    return [
        columnHelper.accessor('name', {
            cell: (data) => {
                return data.getValue()
            },
            header: () => (
                <span>{t('pooling.my_alerts.index.table.name')}</span>
            ),
        }),
        columnHelper.accessor('actions', {
            cell: (data) => {
                return data.getValue()
            },
            header: () => <span />,
        }),
    ] as Array<ColumnDef<SearchAlertRow>>
}
