import { Material, MaterialPricing, User } from '@_/graphql/api/generated'

export type TMaterialTable = Pick<
    Material,
    'id' | 'slug' | 'name' | 'brand' | 'model'
> & {
    images?: {
        edges?: {
            node?: {
                contentUrl?: string
            }
        }[]
    }
    owner?: Pick<
        User,
        'city' | 'slug' | 'firstname' | 'lastname' | 'fullname'
    > & {
        avatar?: { contentUrl?: string }
    }
    bestPriceForUser?: Pick<MaterialPricing, 'value'>
}
