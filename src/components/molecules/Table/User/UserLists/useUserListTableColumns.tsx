import React, { ReactNode } from 'react'
import { ColumnDef, createColumnHelper } from '@tanstack/react-table'
import { useTranslation } from 'react-i18next'
import { Flex, Text } from '@chakra-ui/react'
import { UserList_UsersConnexFragment } from '@_/graphql/api/generated'
import { UserStackedAvatars } from '@components/molecules/User/UserStackedAvatars'

export type UserListRow = {
    slug: string
    name?: string | null
    users?: UserList_UsersConnexFragment
    actions?: ReactNode
}

export const useUserListTableColumns = (): Array<ColumnDef<UserListRow>> => {
    const { t } = useTranslation()
    const columnHelper = createColumnHelper<UserListRow>()

    return [
        columnHelper.accessor('name', {
            cell: (data) => {
                return data.getValue()
            },
            header: () => <span>{t('pooling.my_lists.index.table.name')}</span>,
        }),
        columnHelper.accessor('users', {
            cell: (data) => {
                const users = data.getValue()
                const extraCount = users!.totalCount! - users!.edges!.length
                return (
                    <Flex justifyContent="center" alignItems="center">
                        <UserStackedAvatars
                            users={users?.edges?.map((edge) => edge.node!)}
                        />
                        {extraCount > 0 && (
                            <Text ml={2} fontWeight="bold" color="gray.500">
                                +{extraCount}
                            </Text>
                        )}
                    </Flex>
                )
            },
            header: () => (
                <span>{t('pooling.my_lists.index.table.users')}</span>
            ),
        }),
        columnHelper.accessor('actions', {
            cell: (data) => {
                return data.getValue()
            },
            header: () => (
                <span>{t('pooling.my_lists.index.table.actions')}</span>
            ),
        }),
    ] as Array<ColumnDef<UserListRow>>
}
