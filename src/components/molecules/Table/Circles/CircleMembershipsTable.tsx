import { Badge, Flex, Group, HStack, Table, VStack } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { generatePath, Link, useNavigate } from 'react-router'
import { FaChevronRight } from 'react-icons/fa'
import {
    CircleMembershipsTable__CircleMembershipFragment,
    GetUserCirclesDocument,
    GetUserCirclesQueryVariables,
    useDeleteCircleMutation,
} from '@_/graphql/api/generated'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { toast } from 'react-toastify'
import { Button } from '@components/ui/button'
import { useColorModeValue } from '@components/ui/color-mode'
import { Tooltip } from '@components/ui/tooltip'
import { CircleLogo } from '../../User/Circle/CircleLogo'
import { AcceptInvitationButton } from '../../../atoms/Button/circle/membership/CircleMembershipStatusActionButton'
import { ThreeDotsMenu } from '../../../atoms/Menu/ThreeDotsMenu'

export const CircleMembershipsTable = ({
    circleMemberships,
    variables,
}: {
    circleMemberships: CircleMembershipsTable__CircleMembershipFragment[]
    variables: GetUserCirclesQueryVariables
}) => {
    const { t } = useTranslation()
    const tableBgColor = useColorModeValue('gray.50', 'gray.800')
    const [deleteCircle] = useDeleteCircleMutation()
    const navigate = useNavigate()

    const onDelete = (_circle: { id: string }) => {
        deleteCircle({
            variables: {
                input: {
                    id: _circle.id,
                },
            },
            refetchQueries: [{ query: GetUserCirclesDocument, variables }],
        })
        toast.success(t('communities.form.toast.delete.success'))
        navigate(generatePath(CommunitiesRoutes.MyCommunities))
    }

    return (
        circleMemberships.length > 0 && (
            <Table.Root w="100%" my={25} borderRadius={'2xl'} striped size="sm">
                <Table.Header>
                    <Table.Row>
                        {['name'].map((item) => (
                            <Table.ColumnHeader
                                key={item}
                                p={4}
                                bgColor={tableBgColor}
                            >
                                {t(
                                    `communities.my_communities.index.table.${item}`
                                )}
                            </Table.ColumnHeader>
                        ))}
                        <Table.ColumnHeader
                            p={4}
                            bgColor={tableBgColor}
                            textAlign="right"
                        >
                            {t(
                                'communities.my_communities.index.table.actions.label'
                            )}
                        </Table.ColumnHeader>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {circleMemberships.map((node) => {
                        const actions = [
                            {
                                component: (
                                    <Link
                                        to={generatePath(
                                            CommunitiesRoutes.Show,
                                            { slug: node.circle.slug }
                                        )}
                                    >
                                        <Button
                                            variant="ghost"
                                            asChild
                                            title={t('global.see')}
                                            w="100%"
                                            justifyContent="space-between"
                                        >
                                            <HStack>
                                                {t('global.see')}
                                                <FaChevronRight />
                                            </HStack>
                                        </Button>
                                    </Link>
                                ),
                                key: `seeLink${node.circle.slug}`,
                            },
                            ...(node.circle.activeUserActions?.map(
                                (action: string) => {
                                    if (action === 'edit') {
                                        return {
                                            component: (
                                                <Link
                                                    to={generatePath(
                                                        CommunitiesRoutes.Edit,
                                                        {
                                                            slug: node.circle
                                                                .slug,
                                                        }
                                                    )}
                                                >
                                                    <Button
                                                        variant="ghost"
                                                        asChild
                                                        title={t(
                                                            'communities.my_communities.index.table.manage'
                                                        )}
                                                        w="100%"
                                                        justifyContent="space-between"
                                                    >
                                                        <HStack>
                                                            {t(
                                                                'communities.my_communities.index.table.manage'
                                                            )}
                                                            <FaChevronRight />
                                                        </HStack>
                                                    </Button>
                                                </Link>
                                            ),
                                            key: `editLink${node.circle.slug}`,
                                        }
                                    }
                                    if (action === 'delete') {
                                        return {
                                            component: (
                                                <Button
                                                    variant="ghost"
                                                    onClick={() => {
                                                        onDelete(node.circle)
                                                    }}
                                                    title={t(
                                                        'communities.my_communities.index.table.actions.delete'
                                                    )}
                                                    w="100%"
                                                    justifyContent="space-between"
                                                >
                                                    <HStack>
                                                        {t(
                                                            'communities.my_communities.index.table.actions.delete'
                                                        )}
                                                        <FaChevronRight />
                                                    </HStack>
                                                </Button>
                                            ),
                                            key: `deleteLink${node.circle.slug}`,
                                        }
                                    }
                                    return ''
                                }
                            ) || []),
                        ]

                        return (
                            <Table.Row key={`circleMembership-${node.id}`}>
                                <Table.Cell>
                                    <Flex alignItems="center" gap={3}>
                                        <CircleLogo
                                            circle={node.circle}
                                            alt={`${t(
                                                'communities.my_communities.index.table.logo'
                                            )} ${node.circle.name}`}
                                        />
                                        <VStack>
                                            <Link
                                                to={generatePath(
                                                    CommunitiesRoutes.Show,
                                                    {
                                                        slug: node.circle.slug,
                                                    }
                                                )}
                                            >
                                                {node.circle.name}
                                            </Link>
                                        </VStack>
                                        {['invited', 'pending'].includes(
                                            node.status
                                        ) && (
                                            <Tooltip
                                                label={t(
                                                    `communities.my_communities.index.table.status.${node.status}_tooltip`
                                                )}
                                            >
                                                <Badge
                                                    px="2"
                                                    variant="solid"
                                                    bgColor="yellow.400"
                                                    color="white"
                                                    rounded="full"
                                                    textTransform="capitalize"
                                                >
                                                    {t(
                                                        `communities.my_communities.index.table.status.${node.status}`,
                                                        {
                                                            context:
                                                                node.user
                                                                    .gender ||
                                                                'other',
                                                        }
                                                    )}
                                                </Badge>
                                            </Tooltip>
                                        )}
                                    </Flex>
                                </Table.Cell>
                                <Table.Cell textAlign="right">
                                    <Group>
                                        {node.statusTransitionAvailables &&
                                            node.statusTransitionAvailables.map(
                                                (action: string) =>
                                                    action ===
                                                        'acceptInvitation' && (
                                                        <AcceptInvitationButton
                                                            key={`acceptInvitationButton${node.circle.slug}`}
                                                            circleMembership={
                                                                node
                                                            }
                                                        />
                                                    )
                                            )}
                                        <ThreeDotsMenu
                                            ariaLabel="Actions"
                                            buttons={actions}
                                        />
                                    </Group>
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>
            </Table.Root>
        )
    )
}
