import { Heading } from '@components/atoms/Heading'
import { Box, Flex, LinkBox, LinkOverlay, Text } from '@chakra-ui/react'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import { MouseEvent, ReactNode, useEffect } from 'react'

const MotionLinkBox = motion(LinkBox)

export const WantedItem = ({
    title,
    description,
    image,
    link,
    onClick,
    button,
}: {
    title: string
    description: string
    link?: string
    onClick?: (e: MouseEvent) => void
    image?: string
    button?: ReactNode
}) => {
    const controls = useAnimation()
    const [ref, inView] = useInView({
        triggerOnce: true,
        threshold: 0.1,
    })

    useEffect(() => {
        if (inView) {
            controls.start('visible')
        }
    }, [controls, inView])

    // Variants for framer-motion animations
    const cardVariants = {
        hidden: { opacity: 0, y: 20 },
        visible: { opacity: 1, y: 0 },
        hover: { scale: 1.05, boxShadow: '0px 4px 15px rgba(0, 0, 0, 0.2)' },
    }

    return (
        <MotionLinkBox
            as="article"
            bg="bg.emphasized"
            shadow="base"
            rounded="md"
            overflow="hidden"
            transition={{ duration: 0.4, ease: 'easeInOut' }}
            ref={ref}
            initial="hidden"
            animate={controls}
            whileHover="hover"
            variants={cardVariants}
            onClick={onClick}
            cursor="pointer"
        >
            <Flex direction="column">
                {image && (
                    <Box
                        height="180px"
                        width="100%"
                        bgSize="cover"
                        position="center"
                        bgImage={`url('${image}')`}
                    />
                )}
                <Flex direction="column" p="6">
                    <Heading as="h3" size="lg" mb="4" lineHeight="base">
                        <LinkOverlay href={link}>{title}</LinkOverlay>
                    </Heading>
                    <Text lineClamp={2} mb="8" fontSize="md" color="fg.muted">
                        {description}
                    </Text>
                    {button}
                </Flex>
            </Flex>
        </MotionLinkBox>
    )
}
