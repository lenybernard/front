import {
    Box,
    BoxProps,
    Center,
    Flex,
    List,
    ListItemProps,
    Text,
} from '@chakra-ui/react'
import * as React from 'react'
import { ReactNode } from 'react'
import { RainbowButton } from '@components/atoms/Button'
import { useTranslation } from 'react-i18next'
import { PricingView_Plans__FeaturesFragment } from '@_/graphql/api/generated'
import { NewBadge } from '@components/atoms/Badge/NewBadge'
import { SoonBadge } from '@components/atoms/Badge/SoonBadge'
import { useColorModeValue } from '@components/ui/color-mode'
import { Button } from '@components/ui/button'
import { Link } from 'react-router'
import { LuCheck } from 'react-icons/lu'

const PricingDetail = (props: ListItemProps) => {
    const { children, ...rest } = props
    return (
        <List.Item
            display="flex"
            alignItems="center"
            fontWeight="medium"
            maxW="260px"
            gap={2}
            {...rest}
        >
            <Box color={useColorModeValue('yellow.500', 'yellow.300')}>
                <LuCheck />
            </Box>
            <Text as="span" display="inline-block">
                {children}
            </Text>
        </List.Item>
    )
}

const PopularBadge = (props: BoxProps) => (
    <Box
        whiteSpace="nowrap"
        top="-4"
        left="50%"
        transform="translateX(-50%)"
        pos="absolute"
        rounded="md"
        fontWeight="bold"
        fontSize="sm"
        px="4"
        py="1"
        textTransform="uppercase"
        bg="yellow.500"
        color="white"
        {...props}
    />
)

interface PriceDisplayProps {
    currency: string
    price: number
}

const PriceDisplay = (props: PriceDisplayProps) => {
    const { currency, price } = props
    const { t } = useTranslation()

    return (
        <Flex
            w="100%"
            align="center"
            justify="center"
            my="5"
            fontWeight="extrabold"
        >
            <Text fontSize="72px" lineHeight="1" letterSpacing="tight">
                {price}
            </Text>
            <Text fontSize="4xl" ml="2">
                {currency}
                <Text fontSize={'small'}>
                    {price !== 0 && t('pricing.perMemberPerMonth')}
                </Text>
            </Text>
        </Flex>
    )
}

const PricingWrapper = (props: BoxProps & { highlight?: boolean }) => {
    const { highlight, ...rest } = props

    const popularStyles: BoxProps = {
        borderWidth: '2px',
        borderColor: 'yellow.500',
        zIndex: 1,
        px: '8',
        pt: '12',
        pb: '10',
        top: { lg: '-8' },
    }

    const styles = highlight ? popularStyles : null

    return (
        <Box
            w="full"
            maxW="md"
            mx="auto"
            bg={useColorModeValue('white', 'gray.700')}
            px="10"
            pt="8"
            pb="8"
            rounded="lg"
            shadow="base"
            position="relative"
            {...styles}
            {...rest}
        />
    )
}

interface PricingCardProps extends BoxProps {
    features: PricingView_Plans__FeaturesFragment[]
    popular?: boolean
    name: string
    description: string
    price: number
    cta: {
        label: string
        to: string
        isDisabled?: boolean
        detail?: ReactNode
    }
    onClick?: () => void
}

export const PricingCard = (props: PricingCardProps) => {
    const {
        onClick,
        features,
        name,
        description,
        price,
        popular,
        cta,
        ...rest
    } = props
    const { t } = useTranslation()

    return (
        <PricingWrapper highlight={popular} {...rest}>
            {popular && <PopularBadge>{t('pricing.popular')}</PopularBadge>}

            <Flex direction="column" justify="center">
                <Text textAlign="center" fontSize="2xl" fontWeight="bold">
                    {name}
                </Text>
                <Text
                    textAlign="center"
                    mt="2"
                    color={useColorModeValue('gray.600', 'gray.400')}
                    maxW="16rem"
                    mx="auto"
                >
                    {description}
                </Text>
                <PriceDisplay currency="€" price={price} />
            </Flex>
            <List.Root listStylePosition="outside" mt="8" gap={4}>
                {features.map((feature) => {
                    const badge =
                        (feature.new && <NewBadge />) ||
                        (feature.workInProgress && <SoonBadge />)
                    return (
                        <PricingDetail key={feature.id}>
                            {feature.name} {badge}
                        </PricingDetail>
                    )
                })}
            </List.Root>
            {(popular && (
                <RainbowButton
                    minH="3.5rem"
                    rounded="lg"
                    fontWeight="bold"
                    mt="8"
                    w="100%"
                    onClick={onClick}
                    asChild
                    disabled={cta.isDisabled}
                >
                    <Link to={cta.to}>{cta.label}</Link>
                </RainbowButton>
            )) || (
                <Button
                    minH="3.5rem"
                    rounded="lg"
                    fontWeight="bold"
                    colorPalette={popular ? 'yellow' : 'gray'}
                    mt="8"
                    w="100%"
                    onClick={onClick}
                    asChild
                    disabled={cta.isDisabled}
                >
                    <Link to={cta.to}>{cta.label}</Link>
                </Button>
            )}
            {cta.detail && (
                <Center color={'blue.400'} fontStyle={'italic'}>
                    {cta.detail}
                </Center>
            )}
        </PricingWrapper>
    )
}
