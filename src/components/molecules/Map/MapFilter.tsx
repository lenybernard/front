import React, { ChangeEvent } from 'react'
import { Box, Input } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'
import { FaSearch } from 'react-icons/fa'
import { InputGroup } from '@components/ui/input-group'

type Props = {
    value: string
    onChange: (event: ChangeEvent<HTMLInputElement>) => void
    loading: boolean
    totalResults: number
}

export const MapFilter = ({
    value,
    onChange,
    loading,
    totalResults,
}: Props) => {
    const { t } = useTranslation()

    return (
        <Box
            p="4"
            bgGradient={`linear-gradient(180deg, rgba(${useColorModeValue(
                '255, 255, 255',
                '50, 50, 50'
            )}, 0.9) 0%, rgba(${useColorModeValue(
                '255, 255, 200',
                '50, 50, 60'
            )}, 0.9) 100%)`}
            boxShadow="2xl"
            borderRadius="xl"
            position="absolute"
            top="20%"
            left="50%"
            transform="translate(-50%, -50%)"
        >
            <InputGroup startElement={<FaSearch color="yellow.400" />}>
                <Input
                    type="text"
                    placeholder={t('communities.map.input.placeholder')}
                    value={value}
                    onChange={onChange}
                    _placeholder={{
                        color: useColorModeValue('gray.900', 'white'),
                    }}
                />
            </InputGroup>
            <Box mt="2">
                {loading
                    ? t('communities.map.filter.loading')
                    : t('communities.map.totalResults', {
                          count: totalResults,
                      })}
            </Box>
        </Box>
    )
}
