import React from 'react'
import { AspectRatio, Box, Center, Flex, Text } from '@chakra-ui/react'
import { generatePath, Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Circle } from '@_/graphql/api/generated'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { LinkButton } from '@components/ui/link-button'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { Heading } from '@components/atoms/Heading'
import { useColorModeValue } from '@components/ui/color-mode'
import { CircleLogo } from '../User/Circle/CircleLogo'

interface Props {
    selectedCircle: Circle | null
    onCloseModal: () => void
}

export const MapCircleModal = ({ selectedCircle, onCloseModal }: Props) => {
    const { t } = useTranslation()
    const coverBgColor = useColorModeValue('gray.50', 'gray.800')

    if (!selectedCircle) return null

    return (
        <DialogRoot
            lazyMount
            open={true}
            onOpenChange={onCloseModal}
            placement={'center'}
        >
            <DialogBackdrop />
            <DialogContent rounded={'2xl'}>
                <DialogHeader p={0}>
                    <AspectRatio ratio={16 / 9} w="full">
                        <Box
                            bgColor={coverBgColor}
                            bgImage={`url(${selectedCircle.cover?.contentUrl ?? '/photos/community-blured.webp'})`}
                            bgSize="cover"
                            bgPos="center"
                            roundedTop={'2xl'}
                            opacity={0.2}
                            h={'full'}
                            w={'full'}
                        />
                    </AspectRatio>
                    <Flex
                        position="absolute"
                        marginTop={-220}
                        alignItems="center"
                        w="full"
                        direction="column"
                        gap={5}
                    >
                        <CircleLogo circle={selectedCircle} size={'2xl'} />
                        <Heading size="4xl" textAlign="center">
                            {selectedCircle.name}
                        </Heading>
                    </Flex>
                </DialogHeader>
                <DialogCloseTrigger />
                <DialogBody>
                    <Flex direction="column" alignItems="center" mb="4">
                        <Box ml="4">
                            <Text fontSize="lg">{selectedCircle.city}</Text>
                            <Text>{selectedCircle.description}</Text>
                        </Box>
                    </Flex>
                    <Center>
                        <LinkButton asChild colorPalette="yellow">
                            <Link
                                to={generatePath(CommunitiesRoutes.Show, {
                                    slug: selectedCircle.slug,
                                })}
                            >
                                {t('communities.map.modal.button')}
                            </Link>
                        </LinkButton>
                    </Center>
                </DialogBody>
            </DialogContent>
        </DialogRoot>
    )
}
