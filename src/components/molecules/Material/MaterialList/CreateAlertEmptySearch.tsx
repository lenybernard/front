import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Card, Heading, Stack, Text } from '@chakra-ui/react'
import { useAuth } from '@_/auth/useAuth'
import { FaBell, FaCheckCircle } from 'react-icons/fa'
import {
    SearchAlertFragment,
    SearchMaterialsDocument,
    useCreateAlertEmptySearchMutation,
} from '@_/graphql/api/generated'
import { toast } from 'react-toastify'
import { SearchAlertCard } from '@components/molecules/Cards/SearchAlertCard/SearchAlertCard'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Link, useLocation, useNavigate } from 'react-router'
import { Button } from '@components/ui/button'
import { LinkButton } from '@components/ui/link-button'
import { useColorModeValue } from '@components/ui/color-mode'

export const CreateAlertEmptySearch = ({
    searchTerms,
    alert: existingAlert,
}: {
    searchTerms: string
    alert?: SearchAlertFragment
}) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const [create] = useCreateAlertEmptySearchMutation()
    const [alert, setAlert] = useState<SearchAlertFragment | undefined>(
        existingAlert
    )
    const navigate = useNavigate()
    const { pathname } = useLocation()

    const onSubmit = (name: string) => {
        create({
            variables: {
                name,
            },
            refetchQueries: [
                {
                    query: SearchMaterialsDocument,
                    variables: {
                        searchTerms: name,
                    },
                },
            ],
            onCompleted: ({ createMaterialSearchAlert }) => {
                toast.success(
                    t(
                        'pooling.index.search.notFound.alert.created.toast_success'
                    )
                )
                setAlert(createMaterialSearchAlert?.materialSearchAlert)
            },
        })
    }

    return (
        <Card.Root
            bg={useColorModeValue('white', 'gray.900')}
            p={50}
            w={{ base: 'full', md: '50%' }}
            m={'50px auto'}
        >
            <Card.Body>
                {(!alert && (
                    <>
                        <Heading mt="4" fontWeight="extrabold">
                            {t('pooling.index.search.notFound.title')}
                        </Heading>
                        <Heading mt="4" fontSize={'lg'} fontWeight="extrabold">
                            {t(
                                'pooling.index.search.notFound.alert.create.title'
                            )}
                        </Heading>
                        <Text fontSize={'md'} my={5}>
                            {t(
                                'pooling.index.search.notFound.alert.create.description',
                                {
                                    context: auth?.user?.gender || undefined,
                                }
                            )}
                        </Text>
                        <Button
                            colorPalette="yellow"
                            minW={240}
                            onClick={() => {
                                if (auth?.user) {
                                    onSubmit(searchTerms)
                                } else {
                                    toast(t('global.please.login'))
                                    navigate(UserRoutes.Login, {
                                        state: {
                                            from: pathname,
                                        },
                                    })
                                }
                            }}
                        >
                            <FaBell />
                            {t(
                                'pooling.index.search.notFound.alert.create.button'
                            )}
                        </Button>
                    </>
                )) || (
                    <Stack align={'center'} gap={5}>
                        <Text color={'green.400'}>
                            <FaCheckCircle size={100} />
                        </Text>
                        <Heading mt="4" fontWeight="extrabold">
                            {t(
                                'pooling.index.search.notFound.alert.created.title'
                            )}
                        </Heading>
                        <Text>
                            {t(
                                'pooling.index.search.notFound.alert.created.description',
                                {
                                    context: auth?.user?.gender || undefined,
                                }
                            )}
                        </Text>

                        <Box className="light">
                            {alert && (
                                <SearchAlertCard
                                    alert={alert}
                                    button={
                                        <LinkButton
                                            colorPalette="yellow"
                                            minW={240}
                                            asChild
                                        >
                                            <Link to={PoolingRoutes.MyAlerts}>
                                                <FaBell />
                                                {t(
                                                    'pooling.searchAlerts.item.gotToList.button'
                                                )}
                                            </Link>
                                        </LinkButton>
                                    }
                                />
                            )}
                        </Box>
                    </Stack>
                )}
            </Card.Body>
        </Card.Root>
    )
}
