import { MaterialCard__MaterialFragment } from '@_/graphql/api/generated'

export interface Props {
    materials: MaterialCard__MaterialFragment[]
    columns?:
        | number
        | { base?: number; sm?: number; md?: number; lg?: number; xl?: number }
}
