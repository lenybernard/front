import React from 'react'
import { Box, Container, Flex, Text } from '@chakra-ui/react'
import { Pager } from '@components/molecules/Pager/Pager'
import { MaterialList } from '@components/molecules/Material/MaterialList'
import { MaterialMap } from '@components/molecules/Material/MaterialMap'
import {
    MaterialPageInfo,
    SearchAlertFragment,
    SearchMaterialFragment,
} from '@_/graphql/api/generated'
import { useTranslation } from 'react-i18next'
import { CreateAlertEmptySearch } from './MaterialList/CreateAlertEmptySearch'

interface PaginatedMaterialListProps {
    materials: SearchMaterialFragment[]
    pageInfo: MaterialPageInfo
    totalCount: number
    loading: boolean
    setBeforeCursor: React.Dispatch<React.SetStateAction<string | undefined>>
    setAfterCursor: React.Dispatch<React.SetStateAction<string | undefined>>
    searchTerms: string
    alert?: SearchAlertFragment
    viewMode?: 'map' | 'card'
}

export const PaginatedMaterialList: React.FC<PaginatedMaterialListProps> = ({
    materials,
    pageInfo,
    totalCount,
    loading,
    viewMode = 'card',
    setBeforeCursor,
    setAfterCursor,
    alert,
    searchTerms,
}) => {
    const hasPager = totalCount > 16 // Assuming 16 is the number of items per page
    const { t } = useTranslation()

    return (
        <Container maxW="8xl">
            <Text>
                {totalCount > 0 &&
                    t(
                        `pooling.index.itemsFound${
                            totalCount > 1 ? '_plural' : ''
                        }`,
                        { count: totalCount }
                    )}
            </Text>
            <Box>
                {pageInfo && totalCount > 0 ? (
                    <>
                        {hasPager && (
                            <Pager
                                pageInfo={pageInfo}
                                loading={loading}
                                setBeforeCursor={setBeforeCursor}
                                setAfterCursor={setAfterCursor}
                            />
                        )}
                        <Flex>
                            <MaterialList materials={materials} />
                            {viewMode === 'map' && (
                                <MaterialMap materials={materials} />
                            )}
                        </Flex>
                        {hasPager && (
                            <Pager
                                pageInfo={pageInfo}
                                loading={loading}
                                setBeforeCursor={setBeforeCursor}
                                setAfterCursor={setAfterCursor}
                            />
                        )}
                    </>
                ) : (
                    <CreateAlertEmptySearch
                        searchTerms={searchTerms}
                        alert={alert}
                    />
                )}
            </Box>
        </Container>
    )
}
