import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Field, FieldProps } from '@components/ui/field'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'

export const LanguageSwitcher = ({
    onChange,
    ...fieldProps
}: {
    onChange?: (language: string) => void
} & FieldProps) => {
    const { t, i18n } = useTranslation()
    return (
        <Field
            label={t('global.language')}
            orientation="horizontal"
            {...fieldProps}
        >
            <NativeSelectRoot size="sm">
                <NativeSelectField
                    defaultValue={i18n.language}
                    onChange={(e) => {
                        if (onChange) {
                            onChange(e.target.value)
                        } else {
                            i18n.changeLanguage(e.target.value)
                        }
                    }}
                    aria-label={t('switch.language')}
                    title={t('switch.language')}
                >
                    <option value="fr">Français</option>
                    <option value="en">English</option>
                </NativeSelectField>
            </NativeSelectRoot>
        </Field>
    )
}
