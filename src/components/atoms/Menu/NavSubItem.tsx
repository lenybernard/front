import { Link } from 'react-router'
import { Box, Flex, Icon, MenuItem } from '@chakra-ui/react'
import React from 'react'
import { MenuContent, MenuRoot, MenuTrigger } from '@components/ui/menu'
import { Button } from '@components/ui/button'
import { NavSubItemProps } from './NavItemProps'

export const NavSubItem = ({
    icon,
    display,
    subItems,
    children,
    ...rest
}: NavSubItemProps) => {
    const handle = (
        <Flex
            align="center"
            mx="4"
            p="6"
            as="span"
            gap="4"
            role="group"
            {...rest}
        >
            <Icon
                display={{ base: 'none', lg: 'flex' }}
                fontSize="16"
                _groupHover={{
                    color: 'black',
                }}
                as={icon}
            />
            <Box as="span" h="100%">
                {children}
            </Box>
        </Flex>
    )

    return (
        <MenuRoot>
            <MenuTrigger>
                <Button
                    role="group"
                    cursor="pointer"
                    borderBottomWidth="5px"
                    borderBottomColor="transparent"
                    _hover={{
                        bg: 'yellow.400',
                        color: 'black',
                        borderBottomColor: 'yellow.800',
                    }}
                >
                    {handle}
                </Button>
            </MenuTrigger>
            <MenuContent borderTopRadius={0} zIndex={1000}>
                <>
                    {subItems.map((item) => (
                        <MenuItem w="100%" key={item.name}>
                            <Link to={item.to ?? '#'}>{item.name}</Link>
                        </MenuItem>
                    ))}
                </>
            </MenuContent>
        </MenuRoot>
    )
}
