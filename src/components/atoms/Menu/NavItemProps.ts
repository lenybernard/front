import { FlexProps } from '@chakra-ui/react'
import { IconType } from 'react-icons'
import React from 'react'

declare type BaseLinkItemProps = {
    name: string
    icon?: IconType
    disabled?: boolean
    children?: React.ReactNode
} & FlexProps

export declare type NavItemProps = {
    to: string
} & BaseLinkItemProps

export declare type NavSubItemProps = {
    subItems: NavItemProps[]
} & BaseLinkItemProps

export const isNavSubItem = (
    item: NavSubItemProps | NavItemProps
): item is NavSubItemProps => (item as NavSubItemProps).subItems !== undefined

export const isNavItem = (
    item: NavSubItemProps | NavItemProps
): item is NavItemProps => (item as NavItemProps).to !== undefined
