import Lottie, { EventListener } from 'react-lottie'
import React from 'react'
import wellDoneAnimation from '../../../lotties/welldone.json'

export const WellDone = ({
    eventListeners,
}: {
    eventListeners?: ReadonlyArray<EventListener> | undefined
}) => {
    return (
        <Lottie
            options={{
                loop: false,
                autoplay: true,
                animationData: wellDoneAnimation,
                rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice',
                },
            }}
            eventListeners={eventListeners}
            height={400 as any}
            width={400 as any}
        />
    )
}
