import { Text } from '@chakra-ui/react'
import React from 'react'
import { useTranslation } from 'react-i18next'

export const RequiredAsterisk = () => {
    const { t } = useTranslation()
    return (
        <Text
            as="span"
            bgGradient="to-r"
            gradientFrom="red.400"
            gradientTo="pink.400"
            bgClip="text"
            title={t('form.required')}
        >
            *
        </Text>
    )
}
