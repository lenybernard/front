import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import mapboxgl from 'mapbox-gl'
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css'

export interface AddressChangedEvent {
    coordinates?: {
        lat: number
        lng: number
    }
    address?: string
    city?: string
    postalCode?: string
    country?: string
    countryCode?: string
}

interface AddressSearchProps {
    onAddressChanged: (event: AddressChangedEvent) => void
    defaultValue?: {
        address?: string | null
        latitude?: string | null
        longitude?: string | null
    }
}

export const AddressSearch: React.FC<AddressSearchProps> = ({
    onAddressChanged,
    defaultValue,
}) => {
    const { t, i18n } = useTranslation()
    const mapContainerRef = useRef<HTMLDivElement>(null)
    const [map, setMap] = useState<mapboxgl.Map | null>(null)
    const markerRef = useRef<mapboxgl.Marker | null>(null)

    useEffect(() => {
        if (!process.env.REACT_APP_MAPBOX_ACCESS_TOKEN) {
            console.error('Mapbox access token is missing')
            return
        }
        mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN

        const initialLng = defaultValue?.longitude
            ? parseFloat(defaultValue.longitude)
            : 2.3522219
        const initialLat = defaultValue?.latitude
            ? parseFloat(defaultValue.latitude)
            : 48.856614

        const mapInstance = new mapboxgl.Map({
            container: mapContainerRef.current as HTMLElement,
            style: process.env.REACT_APP_MAP_DARK_URL,
            center: [initialLng, initialLat],
            zoom: 12,
        })

        setMap(mapInstance)

        // eslint-disable-next-line consistent-return
        return () => {
            mapInstance.remove()
        }
    }, [])

    useEffect(() => {
        if (map && defaultValue?.latitude && defaultValue?.longitude) {
            const newLng = parseFloat(defaultValue.longitude)
            const newLat = parseFloat(defaultValue.latitude)
            const newCoordinates: [number, number] = [newLng, newLat]

            map.jumpTo({ center: newCoordinates })

            const handleMarkerDragEnd = () => {
                if (markerRef.current) {
                    onAddressChanged({
                        coordinates: markerRef.current.getLngLat(),
                    })
                }
            }

            if (!markerRef.current) {
                markerRef.current = new mapboxgl.Marker({ draggable: true })
                    .setLngLat(newCoordinates)
                    .addTo(map)
                    .on('dragend', handleMarkerDragEnd)
            } else {
                markerRef.current.setLngLat(newCoordinates)
            }
        }
    }, [defaultValue, map, onAddressChanged])

    useEffect(() => {
        if (!map || !mapContainerRef.current) return

        const geocoder = new MapboxGeocoder({
            accessToken: process.env.REACT_APP_MAPBOX_ACCESS_TOKEN || '',
            mapboxgl,
            placeholder: t('communities.form.address.placeholder'),
            language: i18n.language,
            marker: false,
            flyTo: false,
        })

        geocoder.on('result', ({ result }) => {
            const newCoordinates = result.geometry.coordinates as [
                number,
                number,
            ]

            let city = ''
            let postalCode = ''
            let country = ''
            let countryCode = ''

            if (result.context) {
                result.context.forEach((context: any) => {
                    if (context.id.startsWith('place')) {
                        city = context.text
                    }
                    if (context.id.startsWith('postcode')) {
                        postalCode = context.text
                    }
                    if (context.id.startsWith('country')) {
                        country = context.text
                        countryCode = context.short_code?.toUpperCase() || ''
                    }
                })
            }

            onAddressChanged({
                coordinates: {
                    lng: newCoordinates[0],
                    lat: newCoordinates[1],
                },
                address: result.place_name,
                city,
                postalCode,
                country,
                countryCode,
            })

            map.jumpTo({ center: newCoordinates })

            const handleMarkerDragEnd = () => {
                if (markerRef.current) {
                    onAddressChanged({
                        coordinates: markerRef.current.getLngLat(),
                    })
                }
            }

            if (!markerRef.current) {
                markerRef.current = new mapboxgl.Marker({ draggable: true })
                    .setLngLat(newCoordinates)
                    .addTo(map)
                    .on('dragend', handleMarkerDragEnd)
            } else {
                markerRef.current.setLngLat(newCoordinates)
            }
        })

        geocoder.on('results', ({ config }) => {
            onAddressChanged({
                address: config.query,
                coordinates: markerRef.current?.getLngLat(),
            })
        })

        map.addControl(geocoder, 'top-left')

        if (defaultValue?.address) {
            const inputElement = mapContainerRef.current.querySelector('input')
            if (inputElement) {
                inputElement.value = defaultValue.address
            }
        }

        // eslint-disable-next-line consistent-return
        return () => {
            try {
                map.removeControl(geocoder)
            } catch (error) {
                console.warn('error while removing geocoder:', error)
            }
        }
    }, [map, defaultValue, onAddressChanged, t, i18n.language])

    return (
        <div ref={mapContainerRef} style={{ height: '400px', width: '100%' }} />
    )
}
