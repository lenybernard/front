import React, { useEffect, useState } from 'react'
import {
    Box,
    Float,
    IconButton,
    InputProps,
    Stack,
    useDisclosure,
} from '@chakra-ui/react'
import { UseFormRegisterReturn } from 'react-hook-form'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar, AvatarProps } from '@components/ui/avatar'
import { FaTrashAlt } from 'react-icons/fa'
import {
    FileUploadDropzone,
    FileUploadRoot,
    FileUploadRootProps,
} from '@components/ui/file-button'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { PimpImageModal } from '../../molecules/Form/AccountManagement/User/PimpImageModal'

type ImageDropzoneFormProps = {
    label?: string
    description?: string
    register?: UseFormRegisterReturn
    avatarProps?: AvatarProps
    inputProps?: InputProps
    onFileChange: (file: File, closeModal: () => void) => void
    onDelete?: () => void
    preview?: string | false
    maxFiles?: number
} & FileUploadRootProps

export const ImageDropzoneForm = ({
    label,
    description,
    avatarProps,
    onFileChange,
    onDelete,
    preview = false,
    maxFiles,
    ...fileUploadProps
}: ImageDropzoneFormProps) => {
    const { open, onOpen, onClose } = useDisclosure()
    const { t } = useTranslation()
    const [currentFile, setCurrentFile] = useState<File | string | null>(null)
    const [pendingFiles, setPendingFiles] = useState<File[]>([])
    const [uploadKey, setUploadKey] = useState(0)

    const isMulti = maxFiles && maxFiles > 1
    useEffect(() => {
        if (!currentFile && pendingFiles.length > 0) {
            setCurrentFile(pendingFiles[0])
            setPendingFiles((prev) => prev.slice(1))
            onOpen()
        }
    }, [currentFile, pendingFiles, onOpen])

    const handleFilesChange = (files: File[]) => {
        if (files && files.length > 0) {
            setPendingFiles((prev) => [...prev, ...files])
        }
    }

    const avatarBg = useColorModeValue('gray.800', 'gray.200')
    const avatarColor = useColorModeValue('white', 'gray.800')
    const dropzoneBg = useColorModeValue('gray.50', 'gray.800')

    return (
        <>
            <Stack direction={['column', 'row']} gap={6}>
                {!isMulti && preview ? (
                    <Box display="inline-block" pos="relative">
                        <Avatar
                            size="2xl"
                            cursor="pointer"
                            src={
                                typeof preview === 'string'
                                    ? preview
                                    : undefined
                            }
                            bg={avatarBg}
                            color={avatarColor}
                            onClick={() => {
                                if (preview) {
                                    setCurrentFile(preview)
                                    onOpen()
                                }
                            }}
                            shape="rounded"
                            {...avatarProps}
                        />
                        {onDelete && (
                            <Float placement="top-end">
                                <IconButton
                                    size="sm"
                                    rounded="full"
                                    colorPalette="red"
                                    color="white"
                                    aria-label="remove image"
                                    onClick={(e: React.MouseEvent) => {
                                        onDelete()
                                        e.stopPropagation()
                                    }}
                                >
                                    <FaTrashAlt />
                                </IconButton>
                            </Float>
                        )}
                    </Box>
                ) : (
                    <FileUploadRoot
                        key={uploadKey}
                        accept={['image/*']}
                        maxFiles={maxFiles}
                        onFileChange={(e: {
                            acceptedFiles: File[]
                            rejectedFiles: File[]
                        }) => {
                            if (e.rejectedFiles && e.rejectedFiles.length > 0) {
                                console.log(e)
                                toast.error(t('global.form.file.rejected'))
                            }
                            if (e.acceptedFiles && e.acceptedFiles.length > 0) {
                                handleFilesChange(e.acceptedFiles)
                            } else if (onDelete) {
                                onDelete()
                            }
                            setUploadKey((prev) => prev + 1)
                        }}
                        {...fileUploadProps}
                    >
                        <FileUploadDropzone
                            label={
                                label || t('global.form.dropzoneImage.label')
                            }
                            description={
                                description ||
                                t('global.form.dropzoneImage.description')
                            }
                            bg={dropzoneBg}
                            w={'full'}
                        />
                    </FileUploadRoot>
                )}
            </Stack>
            {currentFile && (
                <PimpImageModal
                    isOpen={open}
                    onClose={() => {
                        onClose()
                        setCurrentFile(null)
                    }}
                    rawFile={
                        currentFile instanceof File ? currentFile : undefined
                    }
                    preview={
                        typeof currentFile === 'string'
                            ? currentFile
                            : undefined
                    }
                    onUpload={(file, closeModal) => {
                        onFileChange(file, closeModal)
                        setCurrentFile(null)
                    }}
                />
            )}
        </>
    )
}
