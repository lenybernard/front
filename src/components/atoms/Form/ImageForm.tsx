import React, { useState } from 'react'
import {
    Box,
    Float,
    IconButton,
    InputProps,
    Spinner,
    Stack,
    useDisclosure,
} from '@chakra-ui/react'
import { UseFormRegisterReturn } from 'react-hook-form'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar, AvatarProps } from '@components/ui/avatar'
import { FaTrashAlt, FaUpload } from 'react-icons/fa'
import {
    FileUploadList,
    FileUploadRoot,
    FileUploadTrigger,
} from '@components/ui/file-button'
import { Button } from '@components/ui/button'
import { useTranslation } from 'react-i18next'
import { PimpImageModal } from '../../molecules/Form/AccountManagement/User/PimpImageModal'

type ImageFormProps = {
    label?: string
    register?: UseFormRegisterReturn
    avatarProps?: AvatarProps
    inputProps?: InputProps
    onFileChange: (file: File, closeModal: () => void) => void
    onDelete?: () => void
    preview?: string | false
}

export const ImageForm = ({
    label,
    avatarProps,
    onFileChange,
    onDelete,
    preview = false,
}: ImageFormProps) => {
    const { open, onOpen, onClose } = useDisclosure()
    const { t } = useTranslation()
    const [currentImage, setCurrentImage] = useState<File | string | null>(null)
    const [removing, setRemoving] = useState(false)

    const handleAvatarClick = () => {
        if (preview) {
            setCurrentImage(preview)
            onOpen()
        }
    }

    const handleFileChange = (files: File[]) => {
        if (files && files[0]) {
            setCurrentImage(files[0])
            onOpen()
        }
    }
    const avatarBg = useColorModeValue('gray.800', 'gray.200')
    const avatarColor = useColorModeValue('white', 'gray.800')

    return (
        <>
            <Stack direction={['column', 'row']} gap={6}>
                {(preview && (
                    <Box display="inline-block" pos="relative">
                        <Avatar
                            size="2xl"
                            cursor="pointer"
                            src={preview}
                            bg={avatarBg}
                            color={avatarColor}
                            onClick={handleAvatarClick}
                            shape="rounded"
                            {...avatarProps}
                        />
                        {onDelete && (
                            <Float placement="top-end">
                                {(!removing && (
                                    <IconButton
                                        size="sm"
                                        rounded="full"
                                        colorPalette="red"
                                        color="white"
                                        aria-label="remove image"
                                        onClick={(e: React.MouseEvent) => {
                                            setRemoving(true)
                                            onDelete()
                                            e.stopPropagation()
                                        }}
                                    >
                                        <FaTrashAlt />
                                    </IconButton>
                                )) || (
                                    <IconButton
                                        size="sm"
                                        rounded="full"
                                        colorPalette="pink"
                                        color="white"
                                        aria-label="loading"
                                    >
                                        <Spinner size="sm" />
                                    </IconButton>
                                )}
                            </Float>
                        )}
                    </Box>
                )) || (
                    <FileUploadRoot
                        accept={['image/*']}
                        onFileChange={(e: {
                            acceptedFiles: File[]
                            rejectedFiles: File[]
                        }) => {
                            if (e.acceptedFiles && e.acceptedFiles[0]) {
                                handleFileChange(e.acceptedFiles)
                            } else if (onDelete) {
                                onDelete()
                            }
                        }}
                    >
                        <FileUploadTrigger asChild>
                            <Button variant="outline" size="sm">
                                <FaUpload />{' '}
                                {label || t('global.form.uploadImage')}
                            </Button>
                        </FileUploadTrigger>
                        <FileUploadList clearable />
                    </FileUploadRoot>
                )}
            </Stack>
            {currentImage && (
                <PimpImageModal
                    isOpen={open}
                    onClose={() => {
                        onClose()
                        setCurrentImage(null)
                    }}
                    rawFile={
                        currentImage instanceof File ? currentImage : undefined
                    }
                    preview={
                        typeof currentImage === 'string'
                            ? currentImage
                            : undefined
                    }
                    onUpload={(file, closeModal) => {
                        onFileChange(file, closeModal)
                        setCurrentImage(null)
                    }}
                />
            )}
        </>
    )
}
