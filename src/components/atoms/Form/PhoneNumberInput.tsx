import React from 'react'
import { Box, Flex, Input } from '@chakra-ui/react'
import { withMask } from 'use-mask-input'
import { FieldError, UseFormRegisterReturn } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { getCountries, getCountryCallingCode } from 'react-phone-number-input'
import { getName, isValid, registerLocale } from 'i18n-iso-countries'
import en from 'i18n-iso-countries/langs/en.json'
import fr from 'i18n-iso-countries/langs/fr.json'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'

interface PhoneNumberProps {
    name: string
    countryCodeRegister: UseFormRegisterReturn<'countryCode'>
    nationalNumberRegister: UseFormRegisterReturn<'nationalNumber'>
    errors:
        | {
              countryCode?: FieldError | undefined
              nationalNumber?: FieldError | undefined
          }
        | undefined
    required?: boolean
}

export const PhoneNumberInput = ({
    name,
    countryCodeRegister,
    nationalNumberRegister,
    errors,
    required = false,
}: PhoneNumberProps) => {
    const { i18n } = useTranslation()
    const countries = getCountries()
    registerLocale(en)
    registerLocale(fr)

    return (
        <Flex gap={2}>
            <NativeSelectRoot width="90px">
                <NativeSelectField
                    defaultValue={getCountryCallingCode('FR')}
                    required={required}
                    {...countryCodeRegister}
                    w="6rem"
                >
                    {countries.map(
                        (code) =>
                            isValid(code) && (
                                <option
                                    key={code}
                                    value={getCountryCallingCode(code)}
                                >
                                    {getName(code, i18n.language.split('-')[0])}
                                </option>
                            )
                    )}
                </NativeSelectField>
            </NativeSelectRoot>
            <Box flexGrow={1} ml={1}>
                <Input
                    type="text"
                    required={required}
                    /* @ts-ignore */
                    ref={withMask('99 99 99 99 99')}
                    {...nationalNumberRegister}
                />
            </Box>
        </Flex>
    )
}
