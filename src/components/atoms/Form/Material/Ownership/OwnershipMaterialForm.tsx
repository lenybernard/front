import { Select } from 'chakra-react-select'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Field } from '@components/ui/field'
import { OwnershipMaterialFormProps } from '@components/atoms/Form/Material/Ownership/OwnershipMaterialForm.types'
import { ReactNode } from 'react'

type OptionType = {
    label: ReactNode
    value: string
    group?: string
}

export const OwnershipMaterialForm = ({
    inputName,
    control,
    options,
    ...otherSelectProps
}: OwnershipMaterialFormProps) => {
    const { t } = useTranslation()

    return (
        <Controller
            control={control}
            name={inputName}
            render={({
                field: { onChange, onBlur, value, name, ref },
                fieldState: { error },
            }) => (
                <Field
                    errorText={error?.message}
                    label={t(`pooling.form.${inputName}.label`)}
                >
                    {/* @ts-ignore-next-line */}
                    <Select<OptionType, false>
                        name={name}
                        isDisabled={!options}
                        ref={ref}
                        onChange={onChange}
                        onBlur={onBlur}
                        value={value}
                        options={options}
                        {...otherSelectProps}
                    />
                </Field>
            )}
        />
    )
}
