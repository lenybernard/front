import { Flex } from '@chakra-ui/react'
import React from 'react'
import { Link, useLocation } from 'react-router'
import { NavItemProps } from '../Menu/NavItemProps'

export const NavItem = ({
    icon: Icon,
    to,
    disabled,
    name,
    ...rest
}: NavItemProps) => {
    const location = useLocation()
    const activeUrl = location.pathname === to
    const activeStyle = disabled
        ? {
              color: 'gray.700',
          }
        : {
              bg: 'yellow.300',
              color: 'orange.700',
          }
    const currentStyle = activeUrl ? activeStyle : {}
    const disabledStyle = disabled ? { color: 'gray' } : {}

    return (
        <Link
            to={to && !activeUrl && !disabled ? to : '#'}
            style={{ textDecoration: 'none' }}
        >
            <Flex
                align="center"
                p="4"
                mx="4"
                my={3}
                gap={2}
                borderRadius="lg"
                role="group"
                cursor={disabled ? 'default' : 'pointer'}
                _hover={activeStyle}
                {...currentStyle}
                {...disabledStyle}
                {...rest}
            >
                {Icon && <Icon />}
                {name}
            </Flex>
        </Link>
    )
}
