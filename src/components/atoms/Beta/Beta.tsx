import { Badge } from '@chakra-ui/react'

export const Beta = () => {
    return <Badge colorPalette="yellow">BETA</Badge>
}
export const Wip = () => {
    return <Badge colorPalette="orange">🚧 WIP</Badge>
}
