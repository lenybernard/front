import { Box, BoxProps, Text } from '@chakra-ui/react'
import React from 'react'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import rehypeRaw from 'rehype-raw'
import ReactMarkdown from 'react-markdown'
import { useColorModeValue } from '@components/ui/color-mode'
import { TimeAgo } from '@components/atoms/Message/TimeAgo'
import { ChatBubbleProps } from './types'

const DefaultBubble = ({
    item,
    actions,
    alignSelf,
    ml,
    mr,
    maxW = '500px',
    dateColor = 'gray.500',
    roundedTop = '3xl',
    ...otherProps
}: { dateColor?: string } & ChatBubbleProps & BoxProps) => {
    const { t } = useTranslation()
    const { content, parameters } = item
    const message = content.replace(/\n/gi, '<br />')

    return (
        <Box {...{ alignSelf, ml, mr }} mb="1rem" display="flex" gap="0.3rem">
            <Box
                {...{
                    maxW,
                    roundedTop,
                    ...otherProps,
                }}
                px="1rem"
                py="0.2rem"
                boxShadow="xl"
            >
                <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                    {t(message, message, JSON.parse(parameters))}
                </ReactMarkdown>
                <br />
                {item.createdAt && (
                    <Text color={dateColor}>
                        <em
                            title={moment(item.createdAt).format('DD ll, h:mm')}
                        >
                            <TimeAgo datetime={item.createdAt} />
                        </em>
                    </Text>
                )}
            </Box>
            <Box as="span" alignSelf="end" fontSize="xs">
                {actions.map((action) => action)}
            </Box>
        </Box>
    )
}

export const SenderBubble = ({ item, actions }: ChatBubbleProps) => {
    return (
        <DefaultBubble
            alignSelf="end"
            mr="1rem"
            bg="gray.100"
            color="black"
            roundedBottomLeft="3xl"
            item={item}
            actions={actions}
        />
    )
}

export const ReceiverBubble = ({ item, actions }: ChatBubbleProps) => {
    return (
        <DefaultBubble
            alignSelf="start"
            ml="1rem"
            mb="1rem"
            bg="yellow.400"
            color="black"
            roundedBottom="lg"
            item={item}
            actions={actions}
        />
    )
}
export const SystemBubble = ({ item, actions }: ChatBubbleProps) => {
    return (
        <DefaultBubble
            alignSelf="start"
            w="100%"
            maxW="100%"
            border="3px solid"
            borderColor={useColorModeValue('white', 'green.200')}
            rounded={'2xl'}
            fontSize="xs"
            bgColor={useColorModeValue('white', 'green.400')}
            color="black"
            dateColor="black"
            item={item}
            actions={actions}
        />
    )
}
