import { ReactNode } from 'react'
import { ChatBubble__MessageFragment } from '@_/graphql/api/generated'

export type ChatBubbleProps = {
    item: ChatBubble__MessageFragment
    actions: ReactNode[]
}
