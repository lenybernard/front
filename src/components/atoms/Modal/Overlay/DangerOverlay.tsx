import { DialogBackdrop } from '@components/ui/dialog'

export const DangerOverlay = () => (
    <DialogBackdrop
        bg="none"
        backdropFilter="auto"
        backdropInvert="80%"
        backdropBlur="2px"
    />
)
