import React from 'react'
import { Badge } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

export const NewBadge = () => {
    const { t } = useTranslation()
    return <Badge colorPalette={'green'}>{t('global.new')}</Badge>
}
