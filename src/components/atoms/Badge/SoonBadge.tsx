import React from 'react'
import { Badge } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

export const SoonBadge = () => {
    const { t } = useTranslation()
    return <Badge colorPalette={'yellow'}>{t('global.soon')}</Badge>
}
