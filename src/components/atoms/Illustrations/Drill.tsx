import { Icon, IconProps } from '@chakra-ui/react'

export const Drill = ({ height, mt }: IconProps) => {
    return (
        <Icon width="100%" height={height} mt={mt}>
            <svg
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="247.5 247.5 105.0000000000001 105.0000000000001"
                overflow="visible"
            >
                <defs>
                    <clipPath id="drill-id1">
                        <path
                            d="M 483 29 L 709 29 L 709 211 L 483 211 Z M 483 29 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id2">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id3">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id4">
                        <path
                            d="M 483 31 L 605 31 L 605 77 L 483 77 Z M 483 31 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id5">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id6">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id7">
                        <path
                            d="M 490 57 L 613 57 L 613 104 L 490 104 Z M 490 57 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id8">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id9">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id10">
                        <path
                            d="M 497 84 L 620 84 L 620 131 L 497 131 Z M 497 84 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id11">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id12">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id13">
                        <path
                            d="M 505 111 L 627 111 L 627 157 L 505 157 Z M 505 111 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id14">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id15">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id16">
                        <path
                            d="M 512 138 L 635 138 L 635 184 L 512 184 Z M 512 138 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id17">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id18">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id19">
                        <path
                            d="M 519 164 L 642 164 L 642 211 L 519 211 Z M 519 164 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id20">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id21">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id22">
                        <path
                            d="M 0 83 L 404 83 L 404 350 L 0 350 Z M 0 83 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id23">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id24">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id25">
                        <path
                            d="M 75 261 L 413 261 L 413 622 L 75 622 Z M 75 261 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id26">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id27">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id28">
                        <path
                            d="M 188 220 L 413 220 L 413 510 L 188 510 Z M 188 220 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id29">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id30">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id31">
                        <path
                            d="M 56 56 L 411 56 L 411 319 L 56 319 Z M 56 56 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id32">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id33">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id34">
                        <path
                            d="M 359 44 L 529 44 L 529 262 L 359 262 Z M 359 44 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id35">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id36">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id37">
                        <path
                            d="M 126 164 L 318 164 L 318 264 L 126 264 Z M 126 164 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id38">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id39">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id40">
                        <path
                            d="M 359 44 L 483 44 L 483 94 L 359 94 Z M 359 44 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id41">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id42">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id43">
                        <path
                            d="M 368 78 L 492 78 L 492 128 L 368 128 Z M 368 78 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id44">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id45">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id46">
                        <path
                            d="M 378 111 L 501 111 L 501 161 L 378 161 Z M 378 111 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id47">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id48">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id49">
                        <path
                            d="M 387 145 L 510 145 L 510 195 L 387 195 Z M 387 145 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id50">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id51">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id52">
                        <path
                            d="M 396 179 L 520 179 L 520 228 L 396 228 Z M 396 179 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id53">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id54">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id55">
                        <path
                            d="M 405 212 L 529 212 L 529 262 L 405 262 Z M 405 212 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id56">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id57">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id58">
                        <path
                            d="M 709 54 L 762 54 L 762 94 L 709 94 Z M 709 54 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id59">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id60">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id61">
                        <path
                            d="M 680 33 L 726 33 L 726 136 L 680 136 Z M 680 33 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id62">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id63">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id64">
                        <path
                            d="M 177 563 L 449 563 L 449 759 L 177 759 Z M 177 563 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id65">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id66">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id67">
                        <path
                            d="M 263.433594 478.859375 L 130.324219 697.59375 L 358.738281 836.597656 L 491.847656 617.863281 Z M 263.433594 478.859375 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id68">
                        <path
                            d="M 263.433594 478.859375 L 130.324219 697.59375 L 358.003906 836.152344 L 491.117188 617.417969 Z M 263.433594 478.859375 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id69">
                        <path
                            d="M 197 628 L 449 628 L 449 759 L 197 759 Z M 197 628 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id70">
                        <path
                            d="M -189.15625 503.167969 L 265.460938 -243.875 L 812.609375 89.09375 L 357.992188 836.136719 Z M -189.15625 503.167969 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id71">
                        <path
                            d="M 265.304688 -243.617188 L -189.3125 503.425781 L 357.835938 836.398438 L 812.453125 89.355469 Z M 265.304688 -243.617188 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id72">
                        <path
                            d="M 290.925781 539.578125 L 177.347656 726.210938 L 358.738281 836.597656 L 472.3125 649.960938 Z M 290.925781 539.578125 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                    <clipPath id="drill-id73">
                        <path
                            d="M 290.925781 539.578125 L 177.347656 726.210938 L 358.003906 836.152344 L 471.582031 649.515625 Z M 290.925781 539.578125 "
                            clipRule="nonzero"
                        />
                    </clipPath>
                </defs>
                <g clipPath="url(#id1)">
                    <g clipPath="url(#id2)">
                        <g clipPath="url(#id3)">
                            <path
                                fill="#543e4e"
                                d="M 607.972656 29.414062 L 608.777344 32.375 L 680.414062 33.941406 L 708.140625 135.476562 L 647.25 173.257812 L 648.046875 176.1875 L 523.1875 210.300781 L 483.109375 63.527344 L 607.972656 29.414062 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id4)">
                    <g clipPath="url(#id5)">
                        <g clipPath="url(#id6)">
                            <path
                                fill="#948289"
                                d="M 601.398438 31.210938 L 483.109375 63.527344 L 486.695312 76.664062 L 604.988281 44.355469 L 601.398438 31.210938 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id7)">
                    <g clipPath="url(#id8)">
                        <g clipPath="url(#id9)">
                            <path
                                fill="#948289"
                                d="M 608.699219 57.9375 L 490.40625 90.253906 L 493.992188 103.394531 L 612.285156 71.078125 L 608.699219 57.9375 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id10)">
                    <g clipPath="url(#id11)">
                        <g clipPath="url(#id12)">
                            <path
                                fill="#948289"
                                d="M 615.996094 84.664062 L 497.703125 116.980469 L 501.289062 130.121094 L 619.582031 97.804688 L 615.996094 84.664062 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id13)">
                    <g clipPath="url(#id14)">
                        <g clipPath="url(#id15)">
                            <path
                                fill="#948289"
                                d="M 623.292969 111.394531 L 505 143.707031 L 508.589844 156.847656 L 626.882812 124.53125 L 623.292969 111.394531 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id16)">
                    <g clipPath="url(#id17)">
                        <g clipPath="url(#id18)">
                            <path
                                fill="#948289"
                                d="M 630.589844 138.121094 L 512.300781 170.429688 L 515.890625 183.574219 L 634.179688 151.261719 L 630.589844 138.121094 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id19)">
                    <g clipPath="url(#id20)">
                        <g clipPath="url(#id21)">
                            <path
                                fill="#948289"
                                d="M 637.890625 164.84375 L 519.597656 197.160156 L 523.183594 210.304688 L 641.476562 177.984375 L 637.890625 164.84375 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id22)">
                    <g clipPath="url(#id23)">
                        <g clipPath="url(#id24)">
                            <path
                                fill="#543e4e"
                                d="M 381.410156 269.308594 L 98.039062 346.722656 C 62.867188 356.328125 26.566406 335.605469 16.964844 300.433594 L 1.363281 243.285156 C -8.246094 208.117188 12.484375 171.8125 47.65625 162.207031 L 331.03125 84.789062 C 346.839844 80.472656 363.160156 89.789062 367.480469 105.601562 L 402.226562 232.855469 C 406.542969 248.667969 397.226562 264.992188 381.410156 269.308594 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id25)">
                    <g clipPath="url(#id26)">
                        <g clipPath="url(#id27)">
                            <path
                                fill="#543e4e"
                                d="M 410.046875 261.488281 C 418.964844 294.167969 399.710938 327.894531 367.035156 336.820312 L 350.355469 341.378906 L 355.867188 361.574219 C 365.167969 395.640625 345.09375 430.796875 311.027344 440.109375 L 347.890625 575.105469 L 177.296875 621.714844 L 118.074219 404.832031 L 114.597656 405.785156 C 98.261719 410.246094 81.398438 400.617188 76.933594 384.277344 C 72.476562 367.941406 82.101562 351.078125 98.445312 346.613281 L 410.046875 261.488281 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id28)">
                    <g clipPath="url(#id29)">
                        <g clipPath="url(#id30)">
                            <path
                                fill="#ffde59"
                                d="M 228.125 266.609375 L 261.191406 257.574219 L 398.714844 220.007812 L 410.046875 261.488281 C 418.964844 294.167969 399.710938 327.894531 367.035156 336.820312 L 288.671875 358.230469 L 324.460938 489.285156 L 252.238281 509.019531 L 188.96875 277.308594 L 228.125 266.609375 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id31)">
                    <g clipPath="url(#id32)">
                        <g clipPath="url(#id33)">
                            <path
                                fill="#ffde59"
                                d="M 110.304688 189.660156 L 105.929688 173.628906 L 196.015625 149.015625 L 183.605469 103.570312 L 354.195312 56.96875 L 370.988281 118.449219 L 398.714844 220.007812 L 410.046875 261.488281 L 200.289062 318.785156 L 188.96875 277.308594 L 138.035156 291.222656 L 123.09375 295.304688 C 95.050781 302.964844 66.109375 286.4375 58.453125 258.394531 C 50.792969 230.351562 67.320312 201.402344 95.363281 193.742188 L 110.304688 189.660156 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id34)">
                    <g clipPath="url(#id35)">
                        <g clipPath="url(#id36)">
                            <path
                                fill="#948289"
                                d="M 410.046875 261.488281 L 528.335938 229.171875 L 477.953125 44.652344 L 359.65625 76.972656 L 410.046875 261.488281 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id37)">
                    <g clipPath="url(#id38)">
                        <g clipPath="url(#id39)">
                            <path
                                fill="#543e4e"
                                d="M 317.214844 214.640625 L 139.777344 263.105469 L 126.085938 212.988281 L 303.53125 164.515625 L 317.214844 214.640625 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id40)">
                    <g clipPath="url(#id41)">
                        <g clipPath="url(#id42)">
                            <path
                                fill="#543e4e"
                                d="M 477.953125 44.652344 L 359.65625 76.972656 L 364.171875 93.496094 L 482.464844 61.179688 L 477.953125 44.652344 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id43)">
                    <g clipPath="url(#id44)">
                        <g clipPath="url(#id45)">
                            <path
                                fill="#543e4e"
                                d="M 487.125 78.253906 L 368.835938 110.566406 L 373.34375 127.09375 L 491.636719 94.78125 L 487.125 78.253906 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id46)">
                    <g clipPath="url(#id47)">
                        <g clipPath="url(#id48)">
                            <path
                                fill="#543e4e"
                                d="M 496.300781 111.851562 L 378.011719 144.167969 L 382.519531 160.691406 L 500.8125 128.378906 L 496.300781 111.851562 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id49)">
                    <g clipPath="url(#id50)">
                        <g clipPath="url(#id51)">
                            <path
                                fill="#543e4e"
                                d="M 505.476562 145.453125 L 387.1875 177.765625 L 391.699219 194.292969 L 509.992188 161.972656 L 505.476562 145.453125 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id52)">
                    <g clipPath="url(#id53)">
                        <g clipPath="url(#id54)">
                            <path
                                fill="#543e4e"
                                d="M 514.648438 179.050781 L 396.359375 211.367188 L 400.871094 227.890625 L 519.164062 195.574219 L 514.648438 179.050781 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id55)">
                    <g clipPath="url(#id56)">
                        <g clipPath="url(#id57)">
                            <path
                                fill="#543e4e"
                                d="M 523.828125 212.648438 L 405.53125 244.964844 L 410.046875 261.488281 L 528.335938 229.171875 L 523.828125 212.648438 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id58)">
                    <g clipPath="url(#id59)">
                        <g clipPath="url(#id60)">
                            <path
                                fill="#948289"
                                d="M 716.4375 93.007812 L 761.175781 80.785156 L 753.878906 54.058594 L 709.136719 66.285156 L 716.4375 93.007812 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id61)">
                    <g clipPath="url(#id62)">
                        <g clipPath="url(#id63)">
                            <path
                                fill="#c8c8ca"
                                d="M 680.414062 33.941406 L 708.140625 135.476562 L 725.164062 124.898438 L 700.453125 34.402344 L 680.414062 33.941406 "
                                fillOpacity="1"
                                fillRule="nonzero"
                            />
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id64)">
                    <g clipPath="url(#id65)">
                        <g clipPath="url(#id66)">
                            <g clipPath="url(#id67)">
                                <g clipPath="url(#id68)">
                                    <path
                                        fill="#ffde59"
                                        d="M 424.402344 709.570312 L 248.820312 757.539062 C 231.125 762.375 212.859375 751.945312 208.027344 734.25 L 177.296875 621.714844 L 384.921875 564.992188 C 402.621094 560.15625 420.878906 570.582031 425.714844 588.28125 L 447.695312 668.777344 C 452.527344 686.472656 442.097656 704.742188 424.402344 709.570312 "
                                        fillOpacity="1"
                                        fillRule="nonzero"
                                    />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
                <g clipPath="url(#id69)">
                    <g clipPath="url(#id70)">
                        <g clipPath="url(#id71)">
                            <g clipPath="url(#id72)">
                                <g clipPath="url(#id73)">
                                    <path
                                        fill="#543e4e"
                                        d="M 424.402344 709.570312 L 248.820312 757.539062 C 231.125 762.375 212.859375 751.945312 208.027344 734.25 L 197.035156 694.003906 L 436.703125 628.527344 L 447.695312 668.777344 C 452.527344 686.472656 442.097656 704.742188 424.402344 709.570312 "
                                        fillOpacity="1"
                                        fillRule="nonzero"
                                    />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </Icon>
    )
}
