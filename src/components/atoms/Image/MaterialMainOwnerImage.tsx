import { getMainOwnerFromId } from '@components/molecules/Cards/MaterialCard/getMainOwnerFromIRI'
import {
    isMainOwnerCircle,
    isMainOwnerUser,
} from '@components/molecules/Cards/MaterialCard/types'
import { IRI } from '@components/molecules/Form/Pooling/Material/types'
import { CircleLogo } from '@components/molecules/User/Circle/CircleLogo'
import { Box } from '@chakra-ui/react'
import { ReactNode, useEffect, useState } from 'react'
import { Avatar } from '@components/ui/avatar'

export const MaterialMainOwnerImage = ({
    ownerIRI,
}: {
    ownerIRI: IRI<'users' | 'circles', string>
}) => {
    const [card, setCard] = useState<ReactNode>()

    useEffect(() => {
        getMainOwnerFromId(ownerIRI).then((mainOwner) => {
            if (mainOwner && isMainOwnerUser(mainOwner)) {
                setCard(
                    <Avatar
                        size="xl"
                        src={`${mainOwner?.avatar?.contentUrl}`}
                        name={`${mainOwner?.firstname} ${mainOwner?.lastname}`}
                        mb={4}
                        pos="relative"
                    />
                )
            }
            if (mainOwner && isMainOwnerCircle(mainOwner)) {
                setCard(<CircleLogo circle={mainOwner} />)
            }
        })
    }, [ownerIRI])

    return <Box>{card && card}</Box>
}
