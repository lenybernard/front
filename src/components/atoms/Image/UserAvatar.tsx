import { IRI } from '@components/molecules/Form/Pooling/Material/types'
import { useUserImageQuery } from '@_/graphql/api/generated'
import { Avatar, AvatarProps } from '@components/ui/avatar'

export const UserAvatar = ({
    userId,
    ...avatarProps
}: { userId: IRI<'users', string> } & AvatarProps) => {
    const { data } = useUserImageQuery({
        variables: {
            id: userId,
        },
    })

    return (
        (data?.user && (
            <Avatar
                src={data.user.avatar?.contentUrl}
                title={`${data.user.firstname} ${data.user.lastname}`}
                {...avatarProps}
            />
        )) || <Avatar />
    )
}
