import React, {
    forwardRef,
    useImperativeHandle,
    useState,
    useEffect,
    useRef,
    useCallback,
} from 'react'

interface BlurSelectionProps {
    imageSrc: string
}

export const BlurSelection = forwardRef<HTMLCanvasElement, BlurSelectionProps>(
    ({ imageSrc }, ref) => {
        const canvasRef = useRef<HTMLCanvasElement>(null)
        useImperativeHandle(ref, () => canvasRef.current as HTMLCanvasElement)
        const [isSelecting, setIsSelecting] = useState(false)
        const [isMoving, setIsMoving] = useState(false)
        const selectionRef = useRef<{
            x: number
            y: number
            width: number
            height: number
        }>({ x: 50, y: 50, width: 100, height: 100 })
        const startRef = useRef<{ x: number; y: number }>({ x: 0, y: 0 })
        const imageRef = useRef(new Image())
        const drawCanva = useCallback(() => {
            if (!canvasRef.current) {
                return
            }

            const ctx = canvasRef.current.getContext('2d')
            if (ctx) {
                ctx.clearRect(
                    0,
                    0,
                    canvasRef.current.width,
                    canvasRef.current.height
                )
                ctx.drawImage(
                    imageRef.current,
                    0,
                    0,
                    canvasRef.current.width,
                    canvasRef.current.height
                )

                ctx.save()
                ctx.beginPath()
                ctx.rect(
                    selectionRef.current.x,
                    selectionRef.current.y,
                    selectionRef.current.width,
                    selectionRef.current.height
                )
                ctx.clip()
                ctx.filter = 'blur(15px)'
                ctx.drawImage(
                    imageRef.current,
                    0,
                    0,
                    canvasRef.current.width,
                    canvasRef.current.height
                )

                ctx.fillStyle = 'rgba(255, 255, 0, 0.2)'
                ctx.fillRect(
                    selectionRef.current.x,
                    selectionRef.current.y,
                    selectionRef.current.width,
                    selectionRef.current.height
                )
                ctx.restore()
            }
        }, [])
        useEffect(() => {
            const canvas = canvasRef.current
            const ctx = canvas?.getContext('2d')
            const image = imageRef.current
            image.crossOrigin = 'anonymous'
            image.src = imageSrc
            image.onload = () => {
                if (canvas && ctx) {
                    const scale = 400 / image.width
                    canvas.width = 400
                    canvas.height = image.height * scale
                    ctx.drawImage(image, 0, 0, canvas.width, canvas.height)
                    drawCanva()
                }
            }
        }, [drawCanva, imageSrc])

        const handleMouseDown = useCallback((e: React.MouseEvent) => {
            const rect = canvasRef.current?.getBoundingClientRect()
            if (rect) {
                const x = e.clientX - rect.left
                const y = e.clientY - rect.top

                // Vérifier si le clic est à l'intérieur de la zone de sélection
                const isInsideSelection =
                    x > selectionRef.current.x &&
                    x < selectionRef.current.x + selectionRef.current.width &&
                    y > selectionRef.current.y &&
                    y < selectionRef.current.y + selectionRef.current.height

                if (isInsideSelection) {
                    setIsMoving(true) // Commencer le déplacement
                    setIsSelecting(false) // Assurez-vous de ne pas créer une nouvelle sélection
                } else {
                    setIsSelecting(true) // Commencer une nouvelle sélection
                    setIsMoving(false)
                }
                startRef.current = { x, y }
            }
        }, [])

        const handleMouseMove = useCallback(
            (e: React.MouseEvent) => {
                if (!canvasRef.current) return

                const rect = canvasRef.current.getBoundingClientRect()
                const currentX = e.clientX - rect.left
                const currentY = e.clientY - rect.top

                if (isSelecting) {
                    const width = currentX - startRef.current.x
                    const height = currentY - startRef.current.y
                    selectionRef.current = {
                        x: startRef.current.x,
                        y: startRef.current.y,
                        width,
                        height,
                    }
                } else if (isMoving) {
                    const dx = currentX - startRef.current.x
                    const dy = currentY - startRef.current.y

                    selectionRef.current = {
                        x: selectionRef.current.x + dx,
                        y: selectionRef.current.y + dy,
                        width: selectionRef.current.width,
                        height: selectionRef.current.height,
                    }

                    startRef.current = { x: currentX, y: currentY }
                }

                drawCanva()
            },
            [isSelecting, isMoving, drawCanva]
        )

        const handleMouseUp = useCallback(() => {
            setIsSelecting(false)
            setIsMoving(false)
        }, [])

        return (
            <canvas
                ref={canvasRef}
                style={{ cursor: 'grab' }}
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseUp}
            />
        )
    }
)

BlurSelection.displayName = 'BlurSelection'
