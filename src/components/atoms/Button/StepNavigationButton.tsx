import React, { ReactNode } from 'react'
import { useTranslation } from 'react-i18next'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'
import { StepsNextTrigger, StepsPrevTrigger } from '@components/ui/steps'
import { Button, ButtonProps } from '@components/ui/button'

type StepButtonProps = {
    title?: string
    icon?: ReactNode
} & ButtonProps

export const StepPreviousButton = ({
    title,
    icon,
    ...buttonProps
}: StepButtonProps) => {
    const { t } = useTranslation()
    return (
        <StepsPrevTrigger asChild>
            <Button variant="ghost" size="sm" {...buttonProps}>
                {icon || <FaChevronLeft />}
                {title || t('global.previous')}
            </Button>
        </StepsPrevTrigger>
    )
}

export const StepNextButton = ({
    title,
    icon,
    ...buttonProps
}: StepButtonProps) => {
    const { t } = useTranslation()
    return (
        <StepsNextTrigger asChild>
            <Button variant="ghost" size="sm" {...buttonProps}>
                {icon || <FaChevronRight />}
                {title || t('global.next')}
            </Button>
        </StepsNextTrigger>
    )
}
