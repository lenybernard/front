import { useTranslation } from 'react-i18next'
import { FaMapMarker, FaThLarge } from 'react-icons/fa'
import { ToggleViewMode } from '@components/atoms/Button/ToggleViewMode'
import { Tooltip } from '@components/ui/tooltip'

export const ToggleViewCardAndMapMode = (props: {
    viewMode: 'map' | 'card'
    onClick: (mode: 'map' | 'card') => void
}) => {
    const { t } = useTranslation()
    return (
        <ToggleViewMode<'map' | 'card'>
            {...props}
            availableModes={[
                {
                    value: 'map',
                    label: (
                        <Tooltip
                            content={t(
                                'pooling.materials.index.actions.viewAsMap'
                            )}
                        >
                            <FaMapMarker />
                        </Tooltip>
                    ),
                },
                {
                    value: 'card',
                    label: (
                        <Tooltip
                            content={t(
                                'pooling.materials.index.actions.viewAsCards'
                            )}
                        >
                            <FaThLarge />
                        </Tooltip>
                    ),
                },
            ]}
        />
    )
}
