import React from 'react'
import { useApolloClient } from '@apollo/client'
import { Group, Spinner } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import {
    useChangeStatusMutation,
    useMaterialBookingQuery,
} from '@_/graphql/api/generated'
import { Button } from '@components/ui/button'
import { Loader } from '../../../../Loader/Loader'
import { Props } from './types'

export const BookingStatusActionButton = ({ id }: Props) => {
    const { t } = useTranslation()
    const client = useApolloClient()
    const { loading, data } = useMaterialBookingQuery({
        variables: {
            id,
        },
    })
    const [changeStatus] = useChangeStatusMutation({
        update() {
            client.resetStore()
        },
    })
    const onAction = (action: string) => {
        const variables = { id, transition: action }
        changeStatus({
            variables,
        }).then((r) => {
            if (r.data?.changeStatusMaterialBooking?.materialBooking === null) {
                toast.error(
                    t('pooling.booking.updateStatus.toast', {
                        context: 'error',
                        action: t('pooling.booking.action', {
                            context: action,
                        }).toLowerCase(),
                    })
                )
                return
            }
            toast.success(
                t('pooling.booking.updateStatus.toast', {
                    context: 'success',
                })
            )
        })
    }

    if (loading) {
        return <Loader height={40} width={40} />
    }
    const booking = data?.materialBooking

    return (
        (booking?.statusTransitionAvailables && (
            <Group alignSelf="center">
                {booking?.statusTransitionAvailables.map((action: string) => (
                    <Button
                        variant={
                            ['confirm', 'apply'].includes(action)
                                ? 'solid'
                                : 'outline'
                        }
                        colorPalette={
                            ['confirm', 'apply'].includes(action)
                                ? 'green'
                                : 'red'
                        }
                        key={`action-${action}`}
                        onClick={() => booking && onAction(action)}
                    >
                        {t('pooling.booking.action', { context: action })}
                    </Button>
                ))}
            </Group>
        )) || <Spinner />
    )
}
