import * as React from 'react'
import { IconButton, IconButtonProps } from '@chakra-ui/react'
import { useColorMode, useColorModeValue } from '@components/ui/color-mode'
import { FaMoon, FaSun } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'

type ColorModeSwitcherProps = Omit<IconButtonProps, 'aria-label'>

export const ColorModeSwitcher: React.FC<ColorModeSwitcherProps> = (props) => {
    const { toggleColorMode } = useColorMode()
    const text = useColorModeValue('dark', 'light')
    const SwitchIcon = useColorModeValue(FaMoon, FaSun)
    const { t } = useTranslation()

    return (
        <IconButton
            title={t(`switch.mode.${text}`)}
            size="md"
            fontSize="lg"
            variant="ghost"
            color="current"
            marginLeft="2"
            onClick={toggleColorMode}
            aria-label={t(`switch.mode.${text}`)}
            {...props}
        >
            <SwitchIcon />
        </IconButton>
    )
}
