import type { ButtonProps as ChakraButtonProps } from '@chakra-ui/react'
import { Button, Flex } from '@chakra-ui/react'
import { motion, MotionProps } from 'framer-motion'
import { ReactNode, useState } from 'react'

type MotionButtonProps = ChakraButtonProps & MotionProps

const MotionButton = motion<ChakraButtonProps>(Button)

const WidthsBySize = {
    '2xs': { initial: '50px', expanded: '120px' },
    xs: { initial: '50px', expanded: '120px' },
    sm: { initial: '60px', expanded: '120px' },
    md: { initial: '70px', expanded: '120px' },
    lg: { initial: '80px', expanded: '120px' },
    xl: { initial: '90px', expanded: '150px' },
    '2xl': { initial: '90px', expanded: '180px' },
}

export const ExpandingOnFocusButton = ({
    label,
    icon,
    size = 'md',
    customSize,
    ...buttonProps
}: {
    label: ReactNode
    icon: ReactNode
    customSize?: { initial: string; expanded: string }
} & MotionButtonProps) => {
    const [isExpanded, setIsExpanded] = useState(false)
    const [isFocused, setIsFocused] = useState(false)
    const { initial: initialWidth, expanded: expandedWidth } =
        customSize ||
        WidthsBySize[size as keyof typeof WidthsBySize] ||
        WidthsBySize.md

    return (
        <Flex alignItems="center">
            <MotionButton
                display="flex"
                alignItems="center"
                justifyContent="center"
                size={size}
                initial={{
                    width: initialWidth,
                }}
                animate={{
                    width:
                        isExpanded || isFocused ? expandedWidth : initialWidth,
                }}
                whileHover={{ width: expandedWidth }}
                whileFocus={{ width: expandedWidth }}
                _focus={{ boxShadow: 'none' }}
                onMouseEnter={() => setIsExpanded(true)}
                onMouseLeave={() => {
                    if (isFocused) {
                        return
                    }
                    setIsExpanded(false)
                }}
                onFocus={() => {
                    setIsExpanded(true)
                    setIsFocused(true)
                }}
                onBlur={() => {
                    setIsExpanded(false)
                    setIsFocused(false)
                }}
                {...buttonProps}
            >
                <Flex alignItems="center" overflow="hidden">
                    {isExpanded && (
                        <motion.span
                            initial={{ opacity: 0, x: -10 }}
                            animate={{ opacity: 1, x: 0 }}
                            exit={{ opacity: 0, x: -10 }}
                            transition={{ duration: 0.3 }}
                            style={{
                                whiteSpace: 'nowrap',
                                marginRight: '8px',
                            }}
                        >
                            {label}
                        </motion.span>
                    )}
                    <motion.div
                        initial={{ scale: 1 }}
                        animate={{ scale: 1 }}
                        transition={{ duration: 0.3 }}
                    >
                        {icon}
                    </motion.div>
                </Flex>
            </MotionButton>
        </Flex>
    )
}
