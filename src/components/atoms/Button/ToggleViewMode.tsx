import { ReactNode } from 'react'
import { SegmentedControl } from '@components/ui/segmented-control'

type ViewMode<T> = {
    value: T
    label: ReactNode
}

export const ToggleViewMode = <T extends string>({
    viewMode,
    onClick,
    availableModes,
}: {
    viewMode: T
    onClick: (mode: T) => void
    availableModes: [ViewMode<T>, ViewMode<T>]
}) => {
    const firstItem = availableModes[0]
    const secondItem = availableModes[1]

    return (
        <SegmentedControl
            hideBelow="md"
            onValueChange={(e: { value: string }) => {
                onClick(
                    viewMode === firstItem.value
                        ? secondItem.value
                        : firstItem.value
                )
            }}
            defaultValue={viewMode}
            items={availableModes}
        />
    )
}
