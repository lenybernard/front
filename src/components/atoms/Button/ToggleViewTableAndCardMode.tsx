import { useTranslation } from 'react-i18next'
import { ToggleViewMode } from '@components/atoms/Button/ToggleViewMode'
import { FaRegAddressCard, FaTable } from 'react-icons/fa'
import { Tooltip } from '@components/ui/tooltip'
import { ViewMode } from '@_/hooks/useViewMode'

type TableCardViewMode = ViewMode.Card | ViewMode.Table

export const ToggleViewTableAndCardMode = (props: {
    viewMode: TableCardViewMode
    onClick: (mode: TableCardViewMode) => void
}) => {
    const { t } = useTranslation()

    return (
        <ToggleViewMode<TableCardViewMode>
            {...props}
            availableModes={[
                {
                    value: ViewMode.Table,
                    label: (
                        <Tooltip
                            content={t(
                                'pooling.my_materials.index.actions.viewAsTable'
                            )}
                        >
                            <FaTable />
                        </Tooltip>
                    ),
                },
                {
                    value: ViewMode.Card,
                    label: (
                        <Tooltip
                            content={t(
                                'pooling.my_materials.index.actions.viewAsCard'
                            )}
                        >
                            <FaRegAddressCard />
                        </Tooltip>
                    ),
                },
            ]}
        />
    )
}
