import React from 'react'
import { Heading as ChakraHeading, HeadingProps } from '@chakra-ui/react'

export type Props = HeadingProps

export const Heading = ({ children, ...otherProps }: Props) => {
    return <ChakraHeading {...otherProps}>{children}</ChakraHeading>
}

export const H1 = ({
    color,
    fontSize = {
        base: '4xl',
        sm: '4xl',
        md: '6xl',
    },
    lineHeight = '1.2',
    children,
    ...props
}: Props) => {
    return (
        <Heading as="h1" lineHeight={lineHeight} fontSize={fontSize} {...props}>
            {children}
        </Heading>
    )
}
export const H2 = ({
    fontSize = {
        base: 'lg',
        sm: 'xl',
        md: '2xl',
    },
    lineHeight = '1.1',
    children,
    ...props
}: Props) => {
    return (
        <Heading as="h2" lineHeight={lineHeight} fontSize={fontSize} {...props}>
            {children}
        </Heading>
    )
}
export const H3 = ({
    fontSize = {
        base: 'md',
        sm: 'lg',
        md: 'xl',
    },
    lineHeight = '1',
    children,
    ...props
}: Props) => {
    return (
        <Heading as="h3" lineHeight={lineHeight} fontSize={fontSize} {...props}>
            {children}
        </Heading>
    )
}
