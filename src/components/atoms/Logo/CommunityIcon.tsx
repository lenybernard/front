import * as React from 'react'
import svg from './community.svg'

export const CommunityIcon = ({
    width,
    height,
}: {
    width: number
    height: number
}) => {
    return <img src={svg} alt="Community" width={width} height={height} />
}
