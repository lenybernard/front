import { Center, type CenterProps } from '@chakra-ui/react'

export const ImagePlaceholder = ({
    image,
    ...centerProps
}: { image: string } & CenterProps) => (
    <Center
        w="full"
        h="full"
        bg="bg.muted"
        color="fg.subtle"
        {...centerProps}
        bgImage={`url(${image})`}
        bgSize="cover"
        bgPos="center"
    />
)
