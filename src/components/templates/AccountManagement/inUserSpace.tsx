import React, { ComponentType } from 'react'
import { useAuth } from '@_/auth/useAuth'
import { AuthUser__UserFragment } from '@_/graphql/api/generated'
import { Stack } from '@chakra-ui/react'
import { ProfileSidebar } from '@components/molecules/User/Profile/ProfileSidebar'
import { Loader } from '../../atoms/Loader/Loader'

export type WithUserProps = {
    user: AuthUser__UserFragment
}

/** @ts-ignore-next-line */
export const inUserSpace = (WrappedComponent: ComponentType<WithUserProps>) => {
    // eslint-disable-next-line func-names,react/display-name
    return function ({ ...props }) {
        const auth = useAuth()
        if (auth.user) {
            return (
                <Stack
                    gap="8"
                    direction={{ base: 'column-reverse', md: 'row' }}
                    flex="1"
                    justify="start"
                >
                    <ProfileSidebar />
                    <WrappedComponent {...props} user={auth.user} />
                </Stack>
            )
        }

        return <Loader />
    }
}
