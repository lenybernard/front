import React, { ComponentType } from 'react'
import { useAuth } from '@_/auth/useAuth'
import { AuthUser__UserFragment } from '@_/graphql/api/generated'
import { Loader } from '../../atoms/Loader/Loader'

export type WithAuthProps = {
    user: AuthUser__UserFragment
}

/** @ts-ignore-next-line */
export const withUser = (WrappedComponent: ComponentType<WithUserProps>) => {
    // eslint-disable-next-line func-names,react/display-name
    return function ({ ...props }) {
        const auth = useAuth()
        if (auth.user) {
            return <WrappedComponent {...props} user={auth.user} />
        }

        return <Loader />
    }
}
