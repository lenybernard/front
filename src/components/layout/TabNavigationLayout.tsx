import React, { ReactNode } from 'react'
import { HStack, Spacer, Tabs, TabsRootProps } from '@chakra-ui/react'
import { useNavigate } from 'react-router'

export type TabItem = {
    key: string
    name: string | React.ReactElement
    tag?: string
    icon?: React.ReactElement
    disabled?: boolean
    path?: string
} & TabsRootProps

export const TabNavigationLayout = ({
    tabs,
    activeTab,
    children,
    renderTabs,
    actions = [],
    variant = 'enclosed',
}: {
    tabs: TabItem[]
    children: React.ReactNode
    activeTab?: string
    actions?: { key: string; component: ReactNode }[]
    variant?: TabsRootProps['variant']
    renderTabs?: (tabsContent: ReactNode) => ReactNode
}) => {
    const navigate = useNavigate()
    const TabsContent = (
        <Tabs.Root
            variant={variant}
            w={'full'}
            defaultValue={activeTab}
            onValueChange={({ value }: { value: string }) => {
                const relatedTab = tabs.find((tab) => tab.key === value)
                if (!relatedTab?.path || relatedTab?.disabled) {
                    return
                }
                navigate(relatedTab.path)
            }}
        >
            <Tabs.List w={'full'} alignItems="center">
                {tabs.map((tab) => (
                    <Tabs.Trigger
                        key={tab.key}
                        value={tab.key}
                        disabled={tab.disabled}
                    >
                        {tab.icon} {tab.name}
                    </Tabs.Trigger>
                ))}
                <Spacer />
                <HStack pos="relative" gap="4">
                    {actions.map(
                        (action) =>
                            action && (
                                <div key={action.key}>{action.component}</div>
                            )
                    )}
                </HStack>
            </Tabs.List>
        </Tabs.Root>
    )

    return (
        <div className="flex flex-col w-full h-full">
            {renderTabs ? renderTabs(TabsContent) : TabsContent}
            {children}
        </div>
    )
}
