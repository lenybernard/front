import React, { useEffect } from 'react'
import { Box, Flex } from '@chakra-ui/react'
import { Crisp } from 'crisp-sdk-web'
import { Theme, ToastContainer } from 'react-toastify'
import { Outlet } from 'react-router'
import { Helmet } from 'react-helmet'
import { SearchProvider } from '@_/contexts/Circles/SearchContext'
import { useColorMode, useColorModeValue } from '@components/ui/color-mode'
import { useMainMenu } from '@components/layout/Layout'
import { Footer } from '../molecules/Footer/Footer'
import { HeadBar } from '../molecules/Menu/HeadBar'
import { AdminNavBar } from '../molecules/Menu/Admin/AdminNavBar'

export const AdminLayout = () => {
    useEffect(() => {
        if (process.env.REACT_APP_CRISP_WEBSITE_ID) {
            Crisp.configure(process.env.REACT_APP_CRISP_WEBSITE_ID)
        }
    }, [])
    const { colorMode } = useColorMode()
    const bg = useColorModeValue('gray.100', 'gray.700')
    const favicon = '/favicon.svg'

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Cockpit</title>
                <link rel="icon" type="image/svg+xml" href={favicon} />

                <link
                    rel="apple-touch-icon"
                    sizes="180x180"
                    href="/favicon/apple-touch-icon.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="32x32"
                    href="/favicon/favicon-32x32.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="16x16"
                    href="/favicon/favicon-16x16.png"
                />
                <link rel="manifest" href="/favicon/site.webmanifest" />
                <link
                    rel="mask-icon"
                    href="/favicon/safari-pinned-tab.svg"
                    color="#5bbad5"
                />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffde59" />
            </Helmet>

            <SearchProvider>
                <Flex direction="column" flex="1" bg={bg}>
                    <HeadBar links={useMainMenu()} />
                    <AdminNavBar />
                    <Flex as="main" role="main" direction="column" flex="1">
                        <Box minH="lg">
                            <Outlet />
                        </Box>
                    </Flex>
                    <ToastContainer
                        theme={colorMode as Theme}
                        draggablePercent={60}
                        role="alert"
                        newestOnTop
                        position="bottom-right"
                    />
                    <Footer />
                </Flex>
            </SearchProvider>
        </>
    )
}
