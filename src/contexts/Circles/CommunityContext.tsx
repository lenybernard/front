import React, {
    createContext,
    ReactNode,
    useContext,
    useMemo,
    useState,
} from 'react'
import { CircleForm__CircleFragment } from '@_/graphql/api/generated'

type CommunityContextProps = {
    community: CircleForm__CircleFragment | null | undefined
    setCommunity: React.Dispatch<
        React.SetStateAction<CircleForm__CircleFragment | null | undefined>
    >
}

const CommunityContext = createContext<CommunityContextProps | undefined>(
    undefined
)

type Props = {
    children: ReactNode
}

export const CommunityProvider: React.FC<Props> = ({ children }) => {
    const [community, setCommunity] = useState<
        CircleForm__CircleFragment | null | undefined
    >(null)

    return useMemo(
        () => (
            <CommunityContext.Provider value={{ community, setCommunity }}>
                {children}
            </CommunityContext.Provider>
        ),
        [community, setCommunity, children]
    )
}

export const useCircle = (): CommunityContextProps => {
    const context = useContext(CommunityContext)
    if (!context) {
        throw new Error('useCircle must be used within an CircleProvider')
    }
    return context
}
