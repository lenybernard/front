import React, {
    createContext,
    Dispatch,
    ReactNode,
    SetStateAction,
    useContext,
    useState,
} from 'react'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { useNavigate } from 'react-router'

type SearchContextProps = {
    searchTerms: string
    setSearchTerms: Dispatch<SetStateAction<string>>
    tempSearchTerms: string
    setTempSearchTerms: Dispatch<SetStateAction<string>>
    handleSubmit: () => void
    resetSearchTerms: () => void
}

const SearchContext = createContext<SearchContextProps | undefined>(undefined)

type Props = {
    children: ReactNode
}

export const SearchProvider: React.FC<Props> = ({ children }) => {
    const [searchTerms, setSearchTerms] = useState<string>('')
    const [tempSearchTerms, setTempSearchTerms] = useState<string>('')
    const navigate = useNavigate()

    const handleSubmit = () => {
        setSearchTerms(tempSearchTerms)
        navigate(`${PoolingRoutes.Search}?q=${tempSearchTerms}`)
    }

    const resetSearchTerms = () => {
        setSearchTerms('')
        setTempSearchTerms('')
    }

    return (
        <SearchContext.Provider
            value={{
                searchTerms,
                setSearchTerms,
                tempSearchTerms,
                setTempSearchTerms,
                handleSubmit,
                resetSearchTerms,
            }}
        >
            {children}
        </SearchContext.Provider>
    )
}

export const useSearch = (): SearchContextProps => {
    const context = useContext(SearchContext)
    if (!context) {
        throw new Error('useSearch must be used within an SearchProvider')
    }
    return context
}
