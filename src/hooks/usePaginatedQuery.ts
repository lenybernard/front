import { OperationVariables, QueryResult } from '@apollo/client'

interface PaginatedQueryOptions {
    path: string[]
}

export type PaginatedQueryResult<T> = {
    nodes: T[]
    pageInfo: {
        hasNextPage: boolean
        hasPreviousPage: boolean
        endCursor: string
        startCursor: string
    } | null
    totalCount: number
    loading: boolean
    error?: unknown
}

export const usePaginatedQuery = <TData, TVariables extends OperationVariables>(
    queryHook: (options: { variables?: TVariables }) => QueryResult,
    { path }: PaginatedQueryOptions,
    variables?: TVariables
): PaginatedQueryResult<TData> => {
    const { loading, error, data } = queryHook({ variables })

    let nodes: TData[] = []
    let pageInfo = null
    let totalCount: number = 0

    if (data) {
        let currentData: {
            edges: { node: TData }[]
            pageInfo: {
                hasNextPage: boolean
                hasPreviousPage: boolean
                startCursor: string
                endCursor: string
            }
            totalCount: number
        } = data
        path.forEach((p) => {
            // @ts-ignore
            currentData = currentData[p]
        })

        nodes =
            currentData.edges
                ?.map((edge: { node: TData }) => edge.node)
                .filter(Boolean) || []
        pageInfo = currentData.pageInfo || null
        totalCount = currentData.totalCount
    }

    return { loading, error, nodes, pageInfo, totalCount }
}
