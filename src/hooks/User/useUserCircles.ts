import {
    CircleMembership,
    CircleMembershipPageInfo,
    GetUserCirclesQueryVariables,
    useGetUserCirclesQuery,
} from '@_/graphql/api/generated'

export const useUserCircles = (variables: GetUserCirclesQueryVariables) => {
    const { loading, error, data } = useGetUserCirclesQuery({ variables })

    let circleMemberships: CircleMembership[] = []
    let pageInfo: CircleMembershipPageInfo | null = null

    if (data?.user?.circleMemberships) {
        circleMemberships =
            data.user.circleMemberships.edges
                ?.map((edge) => edge?.node)
                .filter(
                    (node): node is CircleMembership => node !== undefined
                ) || []
        pageInfo = data.user.circleMemberships.pageInfo || null
    }

    return { loading, error, circleMemberships, pageInfo }
}
