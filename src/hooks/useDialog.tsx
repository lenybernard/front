import { useToggle } from 'react-use'

export function useDialog(initialOpen = false) {
    const [isOpen, toggle] = useToggle(initialOpen)

    const openDialog = () => toggle(true)
    const closeDialog = () => toggle(false)
    const toggleDialog = () => toggle()

    return {
        isOpen,
        openDialog,
        closeDialog,
        toggleDialog,
    }
}
