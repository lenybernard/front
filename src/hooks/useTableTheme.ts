import { useTheme as useTableLibraryTheme } from '@table-library/react-table-library/theme'
import {
    DEFAULT_OPTIONS,
    getTheme,
} from '@table-library/react-table-library/chakra-ui'
import { system } from 'src/theme'

export const useTableTheme = () => {
    const chakraTableTheme = getTheme(DEFAULT_OPTIONS)

    return useTableLibraryTheme([
        chakraTableTheme,
        {
            Table: `
        border-radius: 15px;
      `,
            HeaderRow: `
        color: ${system.token.var('colors.blue.600')};
        background-color: ${system.token.var('colors.gray.50')};
        @media (prefers-color-scheme: dark) {
          color: ${system.token.var('colors.blue.200')};
          background-color: ${system.token.var('colors.gray.800')};
        }
      `,
            Row: `
        color: ${system.token.var('colors.blue.600')};
        &:nth-of-type(odd) {
          background-color: ${system.token.var('colors.gray.50')};
        }
        &:nth-of-type(even) {
          background-color: ${system.token.var('colors.gray.100')};
        }
        @media (prefers-color-scheme: dark) {
          color: ${system.token.var('colors.blue.200')};
          &:nth-of-type(odd) {
            background-color: ${system.token.var('colors.gray.800')};
          }
          &:nth-of-type(even) {
            background-color: ${system.token.var('colors.gray.900')};
          }
        }
      `,
        },
    ])
}
