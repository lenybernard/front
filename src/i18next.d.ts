import 'i18next'

// @see https://github.com/i18next/react-i18next/issues/1587#issuecomment-1328676628
declare module 'i18next' {
    interface CustomTypeOptions {
        returnNull: false
    }
}
