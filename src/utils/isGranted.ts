export const isGranted = async (
    userId: string,
    attribute: string,
    resource?: string
) => {
    let url = `${process.env.REACT_APP_API_URL}/users/isGranted/${userId}?attribute=${attribute}`
    if (resource) {
        url += `&resource=${resource}`
    }

    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json',
            },
        })

        if (!response.ok) {
            throw new Error(`Error: ${response.status}`)
        }

        const data = await response.json()

        return data.isGranted
    } catch {
        return false
    }
}
