import { TFunction } from 'i18next'
import React, { ReactNode } from 'react'
import { Box } from '@chakra-ui/react'
import { PriceText__UserFragment } from '@_/graphql/api/generated'
import { randomTrans } from './random'
import { moneyFormat } from './money'

export const priceText = (
    price: number,
    user: PriceText__UserFragment | null,
    t: TFunction
): ReactNode => {
    if (price === 0) {
        return (
            <Box>
                {randomTrans(t, 'pooling.show.pricing.free', 9, {
                    currencySymbol: user?.settings?.currencySymbol ?? '€',
                })}
            </Box>
        )
    }

    return (
        <Box>
            {t('pooling.show.pricing.price', {
                amount: moneyFormat(
                    price ?? 0,
                    user?.settings?.language ?? 'fr'
                ),
                currencySymbol: user?.settings?.currencySymbol,
            })}
        </Box>
    )
}

export const litePriceText = (
    price: number,
    user: PriceText__UserFragment | null,
    t: TFunction
): string =>
    price === 0
        ? t('pooling.show.pricing.free.10', {
              currencySymbol: user?.settings?.currencySymbol ?? '€',
          })
        : t('pooling.show.pricing.price', {
              amount: moneyFormat(price ?? 0, user?.settings?.language ?? 'fr'),
              currencySymbol: user?.settings?.currencySymbol,
          })
