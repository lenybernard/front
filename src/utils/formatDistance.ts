type AllowedUnit = 'm' | 'km' | 'ft' | 'mi'
type MeasureSystem = 'metric' | 'imperial'

interface FormatOptions {
    unit?: AllowedUnit
    system?: MeasureSystem
    precision?: number
}

export const formatDistance = (
    distance: number,
    options?: FormatOptions
): {
    count: number
    unit: AllowedUnit
} => {
    const { unit = 'km', system = 'metric', precision = 2 } = options || {}

    let formattedDistance: number = 0
    let selectedUnit: AllowedUnit = 'km'

    if (system === 'imperial') {
        if (!unit) {
            if (distance >= 1609.34) {
                selectedUnit = 'mi' // 1 mile = 1609.34 meters
                formattedDistance = parseFloat((distance / 1609.34).toFixed(0))
            } else {
                selectedUnit = 'ft' // 1 foot = 0.3048 meters
                formattedDistance = parseFloat(
                    (distance / 0.3048).toFixed(precision)
                )
            }
        } else if (unit === 'mi') {
            formattedDistance = parseFloat((distance / 1609.34).toFixed(0))
            selectedUnit = 'mi'
        } else if (unit === 'ft') {
            formattedDistance = parseFloat(
                (distance / 0.3048).toFixed(precision)
            )
            selectedUnit = 'ft'
        }
    } else if (!unit) {
        if (distance >= 1000) {
            selectedUnit = 'km'
            formattedDistance = parseFloat((distance / 1000).toFixed(0))
        } else {
            selectedUnit = 'm'
            formattedDistance = parseFloat(distance.toFixed(precision))
        }
    } else if (unit === 'km') {
        formattedDistance = parseFloat((distance / 1000).toFixed(0))
        selectedUnit = 'km'
    } else if (unit === 'm') {
        formattedDistance = parseFloat(distance.toFixed(precision))
        selectedUnit = 'm'
    }

    return {
        count: formattedDistance,
        unit: selectedUnit,
    }
}
