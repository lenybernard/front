import { defineTokens } from '@chakra-ui/react'

export const colors = defineTokens.colors({
    gray: {
        50: { value: '#fafafa' },
        100: { value: '#EDF2F7' },
        200: { value: '#E2E8F0' },
        300: { value: '#CBD5E0' },
        400: { value: '#A0AEC0' },
        500: { value: '#718096' },
        600: { value: '#4A5568' },
        700: { value: '#2D3748' },
        800: { value: '#1A202C' },
        900: { value: '#171923' },
        950: { value: '#111111' },
    },
})
