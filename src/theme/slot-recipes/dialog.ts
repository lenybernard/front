import { defineSlotRecipe } from '@chakra-ui/react'

export const dialogSlotRecipe = defineSlotRecipe({
    slots: ['title'],
    base: {
        title: {
            textStyle: 'xl',
        },
    },
})
