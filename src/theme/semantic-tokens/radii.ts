import { defineSemanticTokens } from '@chakra-ui/react'

export const radii = defineSemanticTokens.radii({
    l2: {
        value: '{radii.xl}',
    },
    l3: {
        value: '{radii.4xl}',
    },
})
