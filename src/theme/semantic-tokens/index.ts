import { colors } from './colors'
import { radii } from './radii'

export const semanticTokens = {
    colors,
    radii,
}
