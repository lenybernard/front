import { defineSemanticTokens } from '@chakra-ui/react'

export const colors = defineSemanticTokens.colors({
    bg: {
        emphasized: {
            value: {
                _light: 'var(--chakra-colors-gray-300)',
                _dark: 'var(--chakra-colors-gray-700)',
            },
        },
        panel: {
            value: {
                _light: 'var(--chakra-colors-gray-50)',
                _dark: 'var(--chakra-colors-gray-800)',
            },
        },
    },
    border: {
        DEFAULT: {
            value: {
                _light: 'var(--chakra-colors-gray-300)',
                _dark: 'var(--chakra-colors-gray-600)',
            },
        },
    },
})
