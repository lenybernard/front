import { TeamView } from '@components/views/TeamView'
import { ContributeView } from '@components/views/ContributeView'
import { ContactView } from '@components/views/ContactView'
import { DonateView } from '@components/views/DonateView'
import { RouteObject } from 'react-router'
import { Layout } from '@components/layout/Layout'
import { HomeView } from '@components/views/HomeView'
import { StaticPagesRoutes } from '@_/routes/_hooks/routes.enum'
import { PricingView } from '@components/views/PricingView'

export const useStaticPagesRouteConfig = (): RouteObject[] => {
    return [
        {
            path: StaticPagesRoutes.Home,
            element: <Layout />,
            children: [
                {
                    index: true,
                    element: <HomeView />,
                },
                {
                    path: StaticPagesRoutes.Contact,
                    element: <ContactView />,
                },
                {
                    path: StaticPagesRoutes.Team,
                    element: <TeamView />,
                },
                {
                    path: StaticPagesRoutes.Contribute,
                    element: <ContributeView />,
                },
                {
                    path: StaticPagesRoutes.Donate,
                    element: <DonateView />,
                },
                {
                    path: StaticPagesRoutes.Pricing,
                    element: <PricingView />,
                },
            ],
        },
    ]
}
