import { MyProfileView } from '@components/views/accountManagement/profile/MyProfileView'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { MyListsView } from '@components/views/pooling/my/lists/MyListsView'
import { MyListEditView } from '@components/views/pooling/my/lists/MyListEditView'
import { RequireAuth } from '@_/auth/RequireAuth'
import { MyMaterialsView } from '@components/views/pooling/my/material/MyMaterialsView'
import { MyMaterialEditView } from '@components/views/pooling/edit/MyMaterialEditView'
import { MyBookingCalendarView } from '@components/views/pooling/my/material/calendar/MyBookingCalendarView'
import { MyBookingsRequestsView } from '@components/views/pooling/my/booking/MyBookingsRequestsView'
import { MyBookingsAsOwnerView } from '@components/views/pooling/my/booking/MyBookingsAsOwnerView'
import { MyBookingShowView } from '@components/views/pooling/my/booking/MyBookingShowView'
import { MyAlertsView } from '@components/views/pooling/my/alerts/MyAlertsView'
import { Layout } from '@components/layout/Layout'
import { MyPreferencesView } from '@components/views/accountManagement/preferences/MyPreferencesView'

export const useUserSpaceRouteConfig = () => {
    return [
        {
            path: '/',
            element: <Layout />,
            children: [
                {
                    path: UserRoutes.MyProfile,
                    element: (
                        <RequireAuth>
                            <MyProfileView />
                        </RequireAuth>
                    ),
                },
                {
                    path: UserRoutes.MyPreferences,
                    element: (
                        <RequireAuth>
                            <MyPreferencesView />
                        </RequireAuth>
                    ),
                },
                {
                    path: UserRoutes.MyLists,
                    element: (
                        <RequireAuth>
                            <MyListsView />
                        </RequireAuth>
                    ),
                },
                {
                    path: UserRoutes.EditMyList,
                    element: (
                        <RequireAuth>
                            <MyListEditView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.MyMaterials,
                    element: (
                        <RequireAuth>
                            <MyMaterialsView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.EditMyMaterial,
                    element: (
                        <RequireAuth>
                            <MyMaterialEditView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.EditMyMaterialStep,
                    element: (
                        <RequireAuth>
                            <MyMaterialEditView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.Calendar,
                    element: (
                        <RequireAuth>
                            <MyBookingCalendarView />
                        </RequireAuth>
                    ),
                },

                {
                    path: PoolingRoutes.MyBookingRequests,
                    element: (
                        <RequireAuth>
                            <MyBookingsRequestsView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.MyBookingAsOwner,
                    element: (
                        <RequireAuth>
                            <MyBookingsAsOwnerView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.MyBookingShow,
                    element: (
                        <RequireAuth>
                            <MyBookingShowView />
                        </RequireAuth>
                    ),
                },
                {
                    path: PoolingRoutes.MyAlerts,
                    children: [
                        {
                            index: true,
                            element: (
                                <RequireAuth>
                                    <MyAlertsView />
                                </RequireAuth>
                            ),
                        },
                    ],
                },
            ],
        },
    ]
}
