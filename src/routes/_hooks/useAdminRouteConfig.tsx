import { AdminLayout } from '@components/layout/AdminLayout'
import { Admin } from '@components/views/admin'
import { AdminInvitationsView } from '@components/views/admin/invitations/AdminInvitationsView'
import { AdminUsersView } from '@components/views/admin/users/AdminUsersView'
import { AdminCirclesView } from '@components/views/admin/circles/AdminCirclesView'
import { RouteObject } from 'react-router'
import { AdminRoutes } from '@_/routes/_hooks/routes.enum'

export const useAdminRouteConfig = (): RouteObject[] => {
    return [
        {
            path: AdminRoutes.Home,
            element: <AdminLayout />,
            children: [
                {
                    index: true,
                    element: <Admin />,
                },
                {
                    path: AdminRoutes.Invitations,
                    element: <AdminInvitationsView />,
                },
                {
                    path: AdminRoutes.Users,
                    element: <AdminUsersView />,
                },
                {
                    path: AdminRoutes.Circles,
                    element: <AdminCirclesView />,
                },
            ],
        },
    ]
}
