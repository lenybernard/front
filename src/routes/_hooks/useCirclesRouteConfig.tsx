import { RequireAuth } from '@_/auth/RequireAuth'
import { CirclesMapView } from '@components/views/circles/CirclesMapView'
import { CirclesShowView } from '@components/views/circles/CirclesShowView'
import { CommunityEditView } from '@components/views/circles/my/CommunityEditView'
import { MyCommunitiesView } from '@components/views/circles/my/MyCommunitiesView'
import { RouteObject } from 'react-router'
import { Layout } from '@components/layout/Layout'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'

export const useCirclesRouteConfig = (): RouteObject[] => {
    return [
        {
            path: '/',
            element: <Layout />,
            children: [
                {
                    path: CommunitiesRoutes.Map,
                    element: <CirclesMapView />,
                },
                {
                    path: CommunitiesRoutes.Show,
                    element: <CirclesShowView />,
                },
                {
                    path: CommunitiesRoutes.Edit,
                    element: (
                        <RequireAuth>
                            <CommunityEditView />
                        </RequireAuth>
                    ),
                },
                {
                    path: CommunitiesRoutes.EditStep,
                    element: (
                        <RequireAuth>
                            <CommunityEditView />
                        </RequireAuth>
                    ),
                },
                {
                    path: CommunitiesRoutes.MyCommunities,
                    element: (
                        <RequireAuth>
                            <MyCommunitiesView />
                        </RequireAuth>
                    ),
                },
            ],
        },
    ]
}
