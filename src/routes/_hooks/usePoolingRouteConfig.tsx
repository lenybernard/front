import { MaterialShowView } from '@components/views/pooling/show/MaterialShowView'
import { RequireAuth } from '@_/auth/RequireAuth'
import { SearchView } from '@components/views/pooling/Search/SearchView'
import { MaterialNewView } from '@components/views/pooling/MaterialNewView'
import { RouteObject } from 'react-router'
import { Layout } from '@components/layout/Layout'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'

export const usePoolingRouteConfig = (): RouteObject[] => {
    return [
        {
            path: '/',
            element: <Layout />,
            children: [
                {
                    path: PoolingRoutes.Search,
                    element: <SearchView />,
                },
                {
                    path: PoolingRoutes.Show,
                    element: <MaterialShowView />,
                },
                {
                    path: PoolingRoutes.New,
                    element: (
                        <RequireAuth>
                            <MaterialNewView />
                        </RequireAuth>
                    ),
                },
            ],
        },
    ]
}
