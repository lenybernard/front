import * as Sentry from '@sentry/react'
import * as React from 'react'
import { useEffect } from 'react'
import { createRoot } from 'react-dom/client'
import { ApolloProvider } from '@apollo/client'
import { I18nextProvider } from 'react-i18next'
import { CookiesProvider } from 'react-cookie'
import 'react-toastify/dist/ReactToastify.css'
import './style/index.scss'
import { App } from '@_/App'
import {
    BrowserRouter,
    createRoutesFromChildren,
    matchRoutes,
    useLocation,
    useNavigationType,
} from 'react-router'
import { ErrorPage } from '@components/views/ErrorPage'
import { ScrollToTop } from '@utils/ScrollToTop'
import { Provider as ChakraProvider } from '@components/ui/provider'
import { AuthProvider } from './auth/AuthProvider'
import i18n from './translations'
import { client } from './apollo/main/client'
import { reportWebVitals } from './reportWebVitals'
import * as serviceWorker from './serviceWorker'

Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    integrations: [
        Sentry.reactRouterV7BrowserTracingIntegration({
            useEffect,
            useLocation,
            useNavigationType,
            createRoutesFromChildren,
            matchRoutes,
        }),
        Sentry.replayIntegration(),
    ],
    environment: process.env.NODE_ENV ?? 'dev',

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    tracesSampleRate: 1.0,

    // Capture Replay for 10% of all sessions,
    // plus for 100% of sessions with an error
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
})

const rootElement = document.getElementById('root')
if (!rootElement) throw new Error('Failed to find the root element')
const root = createRoot(rootElement)

root.render(
    <React.StrictMode>
        <CookiesProvider />
        <ChakraProvider>
            <Sentry.ErrorBoundary fallback={ErrorPage}>
                <ApolloProvider client={client}>
                    <AuthProvider>
                        <I18nextProvider i18n={i18n}>
                            <BrowserRouter>
                                <ScrollToTop />
                                <App />
                            </BrowserRouter>
                        </I18nextProvider>
                    </AuthProvider>
                </ApolloProvider>
            </Sentry.ErrorBoundary>
        </ChakraProvider>
    </React.StrictMode>
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
