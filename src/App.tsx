import * as Sentry from '@sentry/react'
import { useRoutes } from 'react-router'
import { useRouteConfig } from '@_/routes/_hooks/useRouteConfig'
import { useTranslation } from 'react-i18next'
import { useAuth } from '@_/auth/useAuth'

export const App = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    Sentry.addIntegration(
        Sentry.feedbackIntegration({
            colorScheme: auth.user?.settings?.themeMode ?? 'dark',
            showBranding: false,
            showName: true,
            showEmail: true,
            useSentryUser: {
                name: 'fullname',
                email: 'email',
            },
            triggerLabel: t('feedback.triggerLabel'),
            triggerAriaLabel: t('feedback.triggerAriaLabel'),
            formTitle: t('feedback.formTitle'),
            submitButtonLabel: t('feedback.submitButtonLabel'),
            cancelButtonLabel: t('feedback.cancelButtonLabel'),
            confirmButtonLabel: t('feedback.confirmButtonLabel'),
            addScreenshotButtonLabel: t('feedback.addScreenshotButtonLabel'),
            removeScreenshotButtonLabel: t(
                'feedback.removeScreenshotButtonLabel'
            ),
            nameLabel: t('feedback.nameLabel'),
            namePlaceholder: t('feedback.namePlaceholder'),
            emailLabel: t('feedback.emailLabel'),
            emailPlaceholder: t('feedback.emailPlaceholder'),
            isRequiredLabel: t('feedback.isRequiredLabel'),
            messageLabel: t('feedback.messageLabel'),
            messagePlaceholder: t('feedback.messagePlaceholder'),
            successMessageText: t('feedback.successMessageText'),
        })
    )

    return <div>{useRoutes(useRouteConfig())}</div>
}
